package ru.musicapply.collectionplaylistsservice.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

@Component
public class JwtParser {

    public Jwt<Header, Claims> parseJwt(String jwt) {
        String[] split = jwt.split("\\.");
        String untrustedJwt = split[0] + "." + split[1] + ".";

        return Jwts.parser()
                   .parseClaimsJwt(untrustedJwt);
    }

}
