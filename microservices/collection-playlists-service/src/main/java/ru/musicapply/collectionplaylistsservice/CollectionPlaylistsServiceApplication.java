package ru.musicapply.collectionplaylistsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import ru.musicapply.collectionplaylistsservice.configuration.IntegrationProperties;


@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		IntegrationProperties.class
})
public class CollectionPlaylistsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollectionPlaylistsServiceApplication.class, args);
	}

}
