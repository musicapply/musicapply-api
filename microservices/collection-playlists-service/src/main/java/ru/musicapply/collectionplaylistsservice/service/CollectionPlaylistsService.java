package ru.musicapply.collectionplaylistsservice.service;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.playlists.dto.AddTrackInPlaylistRequest;
import ru.musicapply.api.collection.playlists.dto.PlaylistFullResponseDto;
import ru.musicapply.api.collection.playlists.dto.RemoveTrackRequest;
import ru.musicapply.api.collection.playlists.dto.TrackRecordDto;
import ru.musicapply.collectionplaylistsservice.domain.PlaylistRecord;
import ru.musicapply.collectionplaylistsservice.domain.PlaylistTrackRecord;
import ru.musicapply.collectionplaylistsservice.integration.CollectionPlaylistsServiceIntegration;
import ru.musicapply.collectionplaylistsservice.repository.PlaylistRecordRepository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class CollectionPlaylistsService {

    private final PlaylistRecordRepository repository;
    private final CollectionPlaylistsServiceIntegration integration;


    public Mono<Void> createPlaylist(String playlistName, String userId) {
        return Mono.just(playlistName)
                   .map(name -> PlaylistRecord.builder()
                                              .playlistId(UUID.randomUUID().toString())
                                              .name(name)
                                              .userId(userId)
                                              .creationDate(LocalDateTime.now())
                                              .build()
                   )
                   .flatMap(repository::save)
                   .then();
    }

    public Mono<Void> addTrackInPlaylist(AddTrackInPlaylistRequest request, String userId) {
        return repository.findByUserIdAndPlaylistIdOrderByTracksCreationDateAsc(userId, request.getPlaylistId())
                         .flatMap(playlistRecord -> integration.getTrackByIdFull(request.getTrackId())
                                                   .flatMap(track -> {
                                                       PlaylistTrackRecord trackRecord = PlaylistTrackRecord.builder()
                                                                                                            .id(ObjectId.get())
                                                                                                            .creationDate(LocalDateTime.now())
                                                                                                            .trackId(track.getTrackId())
                                                                                                            .build();


                                                       playlistRecord.addTrackInPlaylist(trackRecord);
                                                       return repository.save(playlistRecord);

                                                   })
        )


                         .then();
    }

    public Mono<PlaylistFullResponseDto> getPlaylistFullById(String playlistId, String userId) {
        return repository.findByUserIdAndPlaylistIdOrderByTracksCreationDateAsc(userId, playlistId)
                         .flatMap(playlistRecord -> {
                             PlaylistFullResponseDto playlistDto = PlaylistFullResponseDto.builder()
                                                                                          .playlistId(playlistRecord.getPlaylistId())
                                                                                          .userId(playlistRecord.getUserId())
                                                                                          .name(playlistRecord.getName())
                                                                                          .creationDate(playlistRecord.getCreationDate())
                                                                                          .build();

                             return Flux.fromStream(playlistRecord.getTracks()
                                                                  .stream()
                                                                  .sorted(Collections.reverseOrder(
                                                                          Comparator.comparing(
                                                                                  PlaylistTrackRecord::getCreationDate
                                                                          )
                                                                  ))
                                                                  .sequential()
                                        )
                                        .flatMapSequential(trackRecord -> integration.getTrackByIdFull(trackRecord.getTrackId())
                                                                                     .flatMap(trackDto -> {
                                                                                         TrackRecordDto trackRecordDto = TrackRecordDto.builder()
                                                                                                                                       .id(trackRecord.getId().toString())
                                                                                                                                       .creationDate(trackRecord.getCreationDate())
                                                                                                                                       .track(trackDto)
                                                                                                                                       .build();
                                                                                         playlistDto.getTracks().add(trackRecordDto);

                                                                                         return Mono.just(trackDto);
                                                                                     })
                                        )
                                        .then(Mono.just(playlistDto)
                                                  .map(pD -> {
                                                      pD.getTracks()
                                                        .sort(Comparator.comparing(TrackRecordDto::getCreationDate).reversed());

                                                      return pD;
                                                  })
                                        );
                         });
    }

    public Flux<PlaylistFullResponseDto> getAllPlaylistsByUserId(String userId) {
        return repository.findByUserIdOrderByCreationDateDesc(userId)
                         .flatMapSequential(playlistRecord -> this.getPlaylistFullById(playlistRecord.getPlaylistId(), userId));
    }

    public Mono<Void> removeTrackFromPlaylist(RemoveTrackRequest request, String userId) {
        return repository.findByUserIdAndPlaylistIdOrderByTracksCreationDateAsc(userId, request.getPlaylistId())
                         .flatMap(playlist -> {
                             playlist.removeTrackFromPlaylist(request.getTrackRecordId());
                             return repository.save(playlist);
                         })
                         .then();
    }

    public Mono<Void> removePlaylistFromLibrary(String playlistId, String userId) {
        return repository.findByUserIdAndPlaylistIdOrderByTracksCreationDateAsc(userId, playlistId)
                         .flatMap(repository::delete);
    }
}
