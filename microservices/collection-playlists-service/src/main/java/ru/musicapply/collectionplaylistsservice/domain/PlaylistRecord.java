package ru.musicapply.collectionplaylistsservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "playlists")
public class PlaylistRecord {

    @Id
    private ObjectId id;
    private String userId;
    private String playlistId;
    private String name;
    private LocalDateTime creationDate;

    @Field("tracks")
    @Builder.Default
    private List<PlaylistTrackRecord> tracks = new ArrayList<>();

    public void addTrackInPlaylist(PlaylistTrackRecord track) {
        this.tracks.add(track);
    }

    public void removeTrackFromPlaylist(String trackId) {
        this.tracks.removeIf(record -> record.getId().equals(new ObjectId(trackId)));
    }


}
