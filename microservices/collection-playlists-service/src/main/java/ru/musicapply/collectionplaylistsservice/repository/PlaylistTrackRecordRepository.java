package ru.musicapply.collectionplaylistsservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import ru.musicapply.collectionplaylistsservice.domain.PlaylistTrackRecord;

@Repository
public interface PlaylistTrackRecordRepository extends ReactiveMongoRepository<PlaylistTrackRecord, ObjectId> {
}
