package ru.musicapply.collectionplaylistsservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.playlists.api.CollectionPlaylistsServiceApi;
import ru.musicapply.api.collection.playlists.dto.AddTrackInPlaylistRequest;
import ru.musicapply.api.collection.playlists.dto.PlaylistFullResponseDto;
import ru.musicapply.api.collection.playlists.dto.RemoveTrackRequest;
import ru.musicapply.collectionplaylistsservice.security.UserPrincipal;
import ru.musicapply.collectionplaylistsservice.service.CollectionPlaylistsService;

@RestController
@RequestMapping("/collection/playlists")
@RequiredArgsConstructor
public class PlaylistsController implements CollectionPlaylistsServiceApi {

    private final CollectionPlaylistsService service;


    @Override
    public Mono<Void> createPlaylist(String playlistName) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.createPlaylist(playlistName, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> addTrackInPlaylist(AddTrackInPlaylistRequest request) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.addTrackInPlaylist(request, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<PlaylistFullResponseDto> getPlaylistByPlaylistId(String playlistId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.getPlaylistFullById(playlistId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Flux<PlaylistFullResponseDto> getAllPlaylistsInLibrary() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMapMany(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.getAllPlaylistsByUserId(userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> removeTrack(RemoveTrackRequest request) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.removeTrackFromPlaylist(request, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> removePlaylist(String playlistId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.removePlaylistFromLibrary(playlistId, userPrincipal.getUserId());
                                            });
    }
}
