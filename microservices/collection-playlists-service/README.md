#Collection (playlists) microservice

### Environment variables  
  * DB_HOST (mongodb database host address)  
  * DB_PORT (mongodb database port)  
  * DB_DATABASE (mongodb database name)  
  * DB_USERNAME (mongodb database username)  
  * DB_PASSWORD (mongodb database password)  

### Run in IDE

1. Add Environment variables in `Run Configuration`
2. Start database

### Run in `docker-compose`

1. Build  
  `./gradlew :microservices:collection-playlists-service:build  `  
2. Build image  
  `docker build -f src/main/docker/Dockerfile -t musicapply/collection-playlists-service:1 .`  
3. Run  
  `docker-compose up -d collection-playlists-service`