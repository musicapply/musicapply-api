#Artist (Composite) microservice

### Environment variables  
  * KAFKA_BROKER_HOST (apache kafka broker host address)  
  * KAFKA_BROKER_PORT (apache kafka broker port)  

### Run in IDE

1. Add Environment variables in `Run Configuration`

### Run in `docker-compose`

1. Build  
  `./gradlew :microservices:artist-composite-service:build  `  
2. Build image  
  `docker build -f src/main/docker/Dockerfile -t musicapply/artist-composite-service:1 .`  
3. Run  
  `docker-compose up -d artist-composite-service`