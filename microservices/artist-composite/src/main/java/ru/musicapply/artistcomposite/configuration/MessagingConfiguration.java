package ru.musicapply.artistcomposite.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import ru.musicapply.api.event.album.CreateAlbumEvent;
import ru.musicapply.api.event.artist.CreateArtistEvent;
import ru.musicapply.api.event.track.CreateTrackEvent;

import java.util.function.Supplier;

@Configuration
public class MessagingConfiguration {

    @Bean
    public Sinks.Many<CreateArtistEvent> createArtistSink() {
        return Sinks.many().unicast().onBackpressureBuffer();
    }

    @Bean
    public Supplier<Flux<CreateArtistEvent>> createArtistSupplier(Sinks.Many<CreateArtistEvent> sink){
        return sink::asFlux;
    }

    @Bean
    public Sinks.Many<CreateAlbumEvent> createAlbumSink() {
        return Sinks.many().unicast().onBackpressureBuffer();
    }

    @Bean
    public Supplier<Flux<CreateAlbumEvent>> createAlbumSupplier(Sinks.Many<CreateAlbumEvent> sink){
        return sink::asFlux;
    }

    @Bean
    public Sinks.Many<CreateTrackEvent> createTrackSink() {
        return Sinks.many().unicast().onBackpressureBuffer();
    }

    @Bean
    public Supplier<Flux<CreateTrackEvent>> createTrackSupplier(Sinks.Many<CreateTrackEvent> sink){
        return sink::asFlux;
    }

}
