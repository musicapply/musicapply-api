package ru.musicapply.artistcomposite.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "integration")
@Data
public class IntegrationProperties {

    private String userService;
    private String customerService;
    private String paymentService;
    private String productService;
    private String transactionService;
    private String artistService;
    private String albumService;
    private String trackService;
    private String authServer;
    private String fileStorage;
    private String artistCompositeService;
    private String collectionFavoriteService;
    private String collectionAlbumsService;
    private String collectionArtistsService;
    private String collectionPlaylistsService;

}
