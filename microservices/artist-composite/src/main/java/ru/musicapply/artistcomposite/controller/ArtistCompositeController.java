package ru.musicapply.artistcomposite.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.musicapply.api.artistcompositeservice.ArtistCompositeApi;
import ru.musicapply.api.artistcompositeservice.dto.CreateAlbumRequest;
import ru.musicapply.api.artistcompositeservice.dto.CreateArtistRequest;
import ru.musicapply.api.artistcompositeservice.dto.CreateTrackRequest;
import ru.musicapply.artistcomposite.security.UserPrincipal;
import ru.musicapply.artistcomposite.service.ArtistCompositeService;

@RestController
@RequestMapping("/artist-composite")
@RequiredArgsConstructor
public class ArtistCompositeController implements ArtistCompositeApi {

    private final ArtistCompositeService service;

    @Override
    public Mono<Void> createArtist(CreateArtistRequest request, Mono<FilePart> filePartMono) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.createArtist(userPrincipal.getUserId(), request, filePartMono);
                                            });
    }

    @Override
    public Mono<Void> createAlbum(CreateAlbumRequest request, Mono<FilePart> filePartMono) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.createAlbum(userPrincipal.getUserId(), request, filePartMono);
                                            });
    }

    @Override
    public Mono<Void> createTrack(CreateTrackRequest request, Mono<FilePart> filePartMono) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.createTrack(userPrincipal.getUserId(), request, filePartMono);
                                            });
    }
}
