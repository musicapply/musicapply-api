package ru.musicapply.artistcomposite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import ru.musicapply.artistcomposite.configuration.IntegrationProperties;

@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		IntegrationProperties.class
})
public class ArtistCompositeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtistCompositeApplication.class, args);
	}

}
