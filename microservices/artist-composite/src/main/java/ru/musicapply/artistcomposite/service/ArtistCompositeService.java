package ru.musicapply.artistcomposite.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Schedulers;
import ru.musicapply.api.artistcompositeservice.dto.CreateAlbumRequest;
import ru.musicapply.api.artistcompositeservice.dto.CreateArtistRequest;
import ru.musicapply.api.artistcompositeservice.dto.CreateTrackRequest;
import ru.musicapply.api.event.album.CreateAlbumEvent;
import ru.musicapply.api.event.artist.CreateArtistEvent;
import ru.musicapply.api.event.track.CreateTrackEvent;
import ru.musicapply.artistcomposite.intergation.ArtistCompositeServiceIntegration;

import java.nio.file.Files;
import java.nio.file.Path;


@Service
@RequiredArgsConstructor
public class ArtistCompositeService {

    private final Sinks.Many<CreateArtistEvent> createArtistSink;
    private final Sinks.Many<CreateAlbumEvent> createAlbumSink;
    private final Sinks.Many<CreateTrackEvent> createTrackSink;
    private final ArtistCompositeServiceIntegration integration;

    public Mono<Void> createArtist(String userId, CreateArtistRequest request, Mono<FilePart> filePartMono) {
        return filePartMono.publishOn(Schedulers.boundedElastic())
                           .flatMap(fp -> {

                               try {
                                   Path tempFile = Files.createTempFile("data", "data");
                                   fp.transferTo(tempFile).block();
                                   byte[] bytes = Files.readAllBytes(tempFile);

                                   CreateArtistEvent event = new CreateArtistEvent();
                                   event.setUserId(userId);
                                   event.setName(request.getName());
                                   event.setBio(request.getBio());
                                   event.setImage(bytes);

                                   Files.delete(tempFile);

                                   createArtistSink.tryEmitNext(event);

                               } catch (Exception ex) {
                                   ex.printStackTrace();
                               }

                               return Mono.just(fp);
                           })
                           .then();
    }

    public Mono<Void> createAlbum(String userId, CreateAlbumRequest request, Mono<FilePart> filePartMono) {
        return integration.findByUserId(userId)
                          .publishOn(Schedulers.boundedElastic())
                          .flatMap(artistDto -> {
                              try {
                                  Path tempFile = Files.createTempFile("data", "data");
                                  filePartMono.block().transferTo(tempFile).block();
                                  byte[] bytes = Files.readAllBytes(tempFile);

                                  CreateAlbumEvent event = new CreateAlbumEvent();
                                  event.setArtistId(artistDto.getArtistId());
                                  event.setName(request.getName());
                                  event.setDescription(request.getDescription());
                                  event.setReleaseDate(request.getReleaseDate());
                                  event.setImage(bytes);

                                  createAlbumSink.tryEmitNext(event);

                              } catch (Exception ex) {
                                  ex.printStackTrace();
                              }

                              return Mono.empty();
                          })
                          .then();
    }

    //wrong behavior
    public Mono<Void> createTrack(String userId, CreateTrackRequest request, Mono<FilePart> filePartMono) {
        return integration.findByUserId(userId)
                          .publishOn(Schedulers.boundedElastic())
                          .flatMap(artistDto -> {
                              try {
                                  Path tempFile = Files.createTempFile("data", "data");
                                  filePartMono.block().transferTo(tempFile).block();
                                  byte[] bytes = Files.readAllBytes(tempFile);

                                  CreateTrackEvent event = new CreateTrackEvent();
                                  event.setAlbumId(request.getAlbumId());
                                  event.setName(request.getName());
                                  event.setTrack(bytes);

                                  createTrackSink.tryEmitNext(event);

                              } catch (Exception ex) {
                                  ex.printStackTrace();
                              }

                              return Mono.empty();
                          })
                          .then();
    }
}
