package ru.musicapply.artistcomposite.intergation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import ru.musicapply.api.artistservice.internal.api.InternalArtistApi;
import ru.musicapply.api.artistservice.internal.dto.InternalArtistResponseDto;
import ru.musicapply.artistcomposite.configuration.IntegrationProperties;

import java.net.URI;

@Component
@Slf4j
public class ArtistCompositeServiceIntegration implements InternalArtistApi {

    private final String ARTIST_SERVICE_URL;

    private final WebClient webClient;

    @Autowired
    public ArtistCompositeServiceIntegration(
            WebClient.Builder webClientBuilder,
            IntegrationProperties integrationProperties
    ) {
        this.webClient = WebClient.builder().build();
        this.ARTIST_SERVICE_URL = "http://" + integrationProperties.getArtistService();
    }

    @Override
    public Mono<InternalArtistResponseDto> findByUserId(String userId) {

        URI url = UriComponentsBuilder.fromUriString(ARTIST_SERVICE_URL
                + "/internal/artist/findByUserId/{userId}")
                                      .build(userId);

        log.debug("Will call the findByUserId API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(InternalArtistResponseDto.class);
    }

}
