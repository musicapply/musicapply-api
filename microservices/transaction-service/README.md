#Transaction microservice

### Environment variables  
  * POSTGRES_DB_HOST (postgres host address)
  * POSTGRES_DB_PORT (postgres port)
  * POSTGRES_DB_DATABASE_NAME (database name)
  * POSTGRES_DB_USERNAME (postgres username)
  * POSTGRES_DB_PASSWORD (postgres password)
  * KAFKA_BROKER_HOST (apache kafka broker host address)
  * KAFKA_BROKER_PORT (apache kafka broker port)  

### Run in IDE

1. Add Environment variables in `Run Configuration`
2. Start database and Kafka broker

### Run in `docker-compose`

1. Build  
  `./gradlew :microservices:transaction-service:build  `  
2. Build image  
  `docker build -f src/main/docker/Dockerfile -t musicapply/transaction-service:1 .`  
3. Run  
  `docker-compose up -d transaction-service`