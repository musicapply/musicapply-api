package ru.musicapply.transactionservice.mapper;

import org.mapstruct.Mapper;
import ru.musicapply.api.event.NewTransactionEvent;
import ru.musicapply.api.transactionservice.dto.TransactionResponseDto;
import ru.musicapply.transactionservice.domain.TransactionEntity;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE
)
public interface TransactionMapper {

    TransactionEntity toEntity(NewTransactionEvent event);
    TransactionResponseDto toResponse(TransactionEntity entity);

}
