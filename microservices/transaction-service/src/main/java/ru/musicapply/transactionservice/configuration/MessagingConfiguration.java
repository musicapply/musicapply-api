package ru.musicapply.transactionservice.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.event.EventType;
import ru.musicapply.api.event.NewTransactionEvent;
import ru.musicapply.api.event.UpdateSubscriptionInfoEvent;
import ru.musicapply.transactionservice.service.TransactionService;

import java.util.function.Function;


@Configuration
@RequiredArgsConstructor
public class MessagingConfiguration {

    private final TransactionService service;

    @Bean
    public Function<Flux<NewTransactionEvent>, Flux<UpdateSubscriptionInfoEvent>> newTransactionProcessor() {
        return flux -> flux.filter(e -> e.getType() == EventType.NEW_TRANSACTION_EVENT)
                           .flatMap(service::handleNewTransactionEvent);
    }

}
