package ru.musicapply.transactionservice.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import ru.musicapply.transactionservice.domain.TransactionEntity;

@Repository
public interface TransactionRepository extends R2dbcRepository<TransactionEntity, Integer> {

    Flux<TransactionEntity> findAllByUserId(String userId);

}
