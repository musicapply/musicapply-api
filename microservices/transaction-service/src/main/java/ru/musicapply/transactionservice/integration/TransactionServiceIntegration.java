package ru.musicapply.transactionservice.integration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.productservice.api.ProductServiceApi;
import ru.musicapply.api.productservice.dto.CreateProductRequest;
import ru.musicapply.api.productservice.dto.CreateProductResponse;
import ru.musicapply.api.productservice.dto.ProductResponseDto;
import ru.musicapply.api.productservice.internal.dto.InternalProductResponseDto;
import ru.musicapply.transactionservice.configuration.IntegrationProperties;

import java.net.URI;

@Component
@Slf4j
public class TransactionServiceIntegration implements ProductServiceApi {

    private final String PRODUCT_SERVICE_URL;

    private final WebClient webClient;

    @Autowired
    public TransactionServiceIntegration(
            WebClient.Builder webClientBuilder,
            IntegrationProperties integrationProperties
    ) {
        this.webClient = webClientBuilder.build();
        this.PRODUCT_SERVICE_URL = "http://" + integrationProperties.getProductService();
    }



    @Override
    public Mono<CreateProductResponse> createProduct(CreateProductRequest request) {
        return Mono.error(NotImplementedException::new);
    }

    @Override
    public Mono<ProductResponseDto> getProductById(String productId) {

        URI url = UriComponentsBuilder.fromUriString(PRODUCT_SERVICE_URL
                + "/product/{productId}")
                                      .build(productId);

        log.debug("Will call the getProductById API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(ProductResponseDto.class);
    }

    @Override
    public Flux<ProductResponseDto> getAllProducts() {
        return Flux.error(NotImplementedException::new);
    }
}
