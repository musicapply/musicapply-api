package ru.musicapply.transactionservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.event.NewTransactionEvent;
import ru.musicapply.api.event.UpdateSubscriptionInfoEvent;
import ru.musicapply.api.transactionservice.dto.TransactionResponseDto;
import ru.musicapply.transactionservice.integration.TransactionServiceIntegration;
import ru.musicapply.transactionservice.mapper.TransactionMapper;
import ru.musicapply.transactionservice.repository.TransactionRepository;
import ru.musicapply.transactionservice.security.UserPrincipal;


@Service
@RequiredArgsConstructor
public class TransactionService {

    private final TransactionMapper mapper;
    private final TransactionRepository repository;
    private final TransactionalOperator r2dbcTransactionalOperator;
    private final TransactionServiceIntegration integration;

    public Mono<UpdateSubscriptionInfoEvent> handleNewTransactionEvent(NewTransactionEvent event) {
        return Mono.just(event)
                   .map(mapper::toEntity)
                   .flatMap(repository::save)
                   .as(r2dbcTransactionalOperator::transactional)
                   .then(Mono.just(new UpdateSubscriptionInfoEvent(event.getUserId(), event.getNextPayment())));
    }

    public Flux<TransactionResponseDto> findAllTransactionsByUserId(String userId) {
        return repository.findAllByUserId(userId)
                         .map(mapper::toResponse)
                         .flatMap(dto -> integration.getProductById(dto.getProductId())
                                                    .map(product -> {
                                                        dto.setProduct(product);
                                                        return dto;
                                                    })
                         )
                ;
    }

}
