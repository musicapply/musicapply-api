package ru.musicapply.transactionservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import ru.musicapply.api.transactionservice.api.TransactionApi;
import ru.musicapply.api.transactionservice.dto.TransactionResponseDto;
import ru.musicapply.transactionservice.security.UserPrincipal;
import ru.musicapply.transactionservice.service.TransactionService;

@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
public class TransactionController implements TransactionApi {

    private final TransactionService transactionService;

    @Override
    public Flux<TransactionResponseDto> getAllTransactions() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMapMany(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return transactionService.findAllTransactionsByUserId(userPrincipal.getUserId());
                                            });
    }
}
