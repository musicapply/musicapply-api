package ru.musicapply.transactionservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("transaction")
public class TransactionEntity {

    @Id
    private Integer id;

    @Column("user_id")
    private String userId;

    @Column("amount_paid")
    private Long amountPaid;

    @Column("currency")
    private String currency;

    @Column("payment_date")
    private LocalDateTime paymentDate;

    @Column("product_id")
    private String productId;

    @Column("payment_method_id")
    private String paymentMethodId;

}
