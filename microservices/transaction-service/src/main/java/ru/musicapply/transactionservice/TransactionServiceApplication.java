package ru.musicapply.transactionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import ru.musicapply.transactionservice.configuration.IntegrationProperties;


@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		IntegrationProperties.class
})
public class TransactionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionServiceApplication.class, args);
	}

}
