CREATE TABLE transaction(
    id                      SERIAL              NOT NULL,
    user_id                 VARCHAR(36)         NOT NULL,
    amount_paid             INT8                NOT NULL,
    currency                VARCHAR             NOT NULL,
    payment_date            TIMESTAMP           NOT NULL,
    product_id              VARCHAR(36)         NOT NULL,
    payment_method_id       VARCHAR(36)         NOT NULL
);

ALTER TABLE IF EXISTS transaction
    ADD CONSTRAINT PK_transaction_id
        PRIMARY KEY (id);