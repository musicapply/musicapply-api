package ru.musicapply.transactionservice.mapper;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import ru.musicapply.api.event.NewTransactionEvent;
import ru.musicapply.transactionservice.domain.TransactionEntity;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionMapperTest {

    private final TransactionMapper mapper = Mappers.getMapper(TransactionMapper.class);

    @Test
    public void contextLoad() {
        assertThat(mapper).isNotNull();
    }

    @Test
    public void testMapNewTransactionEventToTransactionEntity() {
        String userId = UUID.randomUUID().toString();
        Long amountPaid = 24900L;
        String currency = "rub";
        LocalDateTime paymentDate = LocalDateTime.now();
        String productId = UUID.randomUUID().toString();
        String paymentMethodId = UUID.randomUUID().toString();


        NewTransactionEvent event = new NewTransactionEvent();
        event.setUserId(userId);
        event.setAmountPaid(amountPaid);
        event.setCurrency(currency);
        event.setPaymentDate(paymentDate);
        event.setProductId(productId);
        event.setPaymentMethodId(paymentMethodId);

        TransactionEntity entity = mapper.toEntity(event);

        assertAll(
                () -> assertEquals(userId, entity.getUserId()),
                () -> assertEquals(amountPaid, entity.getAmountPaid()),
                () -> assertEquals(currency, entity.getCurrency()),
                () -> assertEquals(paymentDate, entity.getPaymentDate()),
                () -> assertEquals(productId, entity.getProductId()),
                () -> assertEquals(paymentMethodId, entity.getPaymentMethodId()),
                () -> assertThat(entity.getId()).isNull()
        );

    }


}
