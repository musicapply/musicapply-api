CREATE TABLE usr (
    id                  SERIAL          NOT NULL,
    user_id             VARCHAR(36)     NOT NULL,
    email               VARCHAR(100)    NOT NULL,
    username            VARCHAR(20)     NOT NULL,
    avatar_url          VARCHAR(100)    NOT NULL
);

ALTER TABLE IF EXISTS usr
    ADD CONSTRAINT PK_usr__id
        PRIMARY KEY (id);

ALTER TABLE IF EXISTS usr
    ADD CONSTRAINT U_usr__email
        UNIQUE (email);

ALTER TABLE IF EXISTS usr
    ADD CONSTRAINT U_usr__username
        UNIQUE (username);

ALTER TABLE IF EXISTS usr
    ADD CONSTRAINT  U_usr__id_email_username
        UNIQUE (user_id, email, username);

