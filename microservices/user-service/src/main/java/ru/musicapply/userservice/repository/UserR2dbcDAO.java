package ru.musicapply.userservice.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.musicapply.userservice.projection.UserEntityProjection;

@Repository
public interface UserR2dbcDAO extends R2dbcRepository<UserEntityProjection, String> {

    Mono<UserEntityProjection> findByUserId(String userId);

}
