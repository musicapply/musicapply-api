package ru.musicapply.userservice.event;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class UserUpdatedEmailEvent {

    String userId;
    String email;

}
