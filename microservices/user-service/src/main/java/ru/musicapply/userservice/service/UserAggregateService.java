package ru.musicapply.userservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import ru.musicapply.userservice.aggregates.UserAggregate;
import ru.musicapply.userservice.commands.RegisterUserCommand;
import ru.musicapply.userservice.commands.UpdateUserEmailCommand;
import ru.musicapply.userservice.event.UserCreatedEvent;
import ru.musicapply.userservice.event.UserUpdatedEmailEvent;
import ru.musicapply.userservice.repository.EventEntity;
import ru.musicapply.userservice.repository.EventStore;
import ru.musicapply.util.exception.ResourceNotFoundException;


@Component
@RequiredArgsConstructor
public class UserAggregateService {

    private final EventStore writeRepository;

    public Mono<UserCreatedEvent> handleRegisterUserCommand(RegisterUserCommand command) {
        return Mono.just(UserAggregate.builder()
                                      .build()
                   )
                   .flatMap(aggregate -> {
                       UserCreatedEvent event = aggregate.handleRegisterUserCommand(command);
                       Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                                    .streamId(command.getUserId())
                                                                                    .data(event)
                                                                                    .build()
                       );

                       return Mono.zip(mono, Mono.just(event));
                   })
                   .map(Tuple2::getT2);
    }

    public Mono<UserUpdatedEmailEvent> handleUpdateUserEmailCommand(UpdateUserEmailCommand command) {
        return recreateUserState(command.getUserId()).flatMap(aggregate -> {
            UserUpdatedEmailEvent event = aggregate.handleUpdateUserEmailCommand(command);
            Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                         .streamId(command.getUserId())
                                                                         .data(event)
                                                                         .build()
            );
            return Mono.zip(mono, Mono.just(event));
        }).map(Tuple2::getT2);
    }

    private Mono<UserAggregate> recreateUserState(String userId) {
        UserAggregate userAggregate = UserAggregate.builder().build();
        return writeRepository.getEvents(userId)
                              .map(event -> {
                                  Object sourceEvent = event.getData();
                                  if (sourceEvent instanceof UserCreatedEvent) {
                                      userAggregate.apply((UserCreatedEvent) sourceEvent);
                                  }
                                  if (sourceEvent instanceof UserUpdatedEmailEvent) {
                                      userAggregate.apply((UserUpdatedEmailEvent) sourceEvent);
                                  }
                                  return event;
                              })
                              .switchIfEmpty(Mono.error(new ResourceNotFoundException("User", "userId", userId)))
                              .then(Mono.just(userAggregate));
    }

}
