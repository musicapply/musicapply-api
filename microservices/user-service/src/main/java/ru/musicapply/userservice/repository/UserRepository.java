package ru.musicapply.userservice.repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.userservice.projection.UserEntityProjection;

public interface UserRepository {

    Mono<UserEntityProjection> save(UserEntityProjection user);
    Mono<UserEntityProjection> findById(String id);
    Mono<UserEntityProjection> findByUserId(String id);
    Flux<UserEntityProjection> findAll();

}
