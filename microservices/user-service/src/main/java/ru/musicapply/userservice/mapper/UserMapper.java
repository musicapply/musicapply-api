package ru.musicapply.userservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.musicapply.api.userservice.dto.UserInfoDto;
import ru.musicapply.userservice.projection.UserEntityProjection;


@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE
)
public interface UserMapper {

    @Mapping(target = "id", source = "userId")
    UserInfoDto toResponse(UserEntityProjection projection);

}
