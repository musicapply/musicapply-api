package ru.musicapply.userservice.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.userservice.projection.UserEntityProjection;

@Component
@RequiredArgsConstructor
public class UserRepositoryR2dbc implements UserRepository {

    private final UserR2dbcDAO userDAO;

    @Override
    public Mono<UserEntityProjection> save(UserEntityProjection user) {
        return userDAO.save(user);
    }

    @Override
    public Mono<UserEntityProjection> findById(String id) {
        return userDAO.findById(id);
    }

    @Override
    public Mono<UserEntityProjection> findByUserId(String id) {
        return userDAO.findByUserId(id);
    }

    @Override
    public Flux<UserEntityProjection> findAll() {
        return userDAO.findAll();
    }
}
