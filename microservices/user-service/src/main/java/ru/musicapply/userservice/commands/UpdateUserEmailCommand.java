package ru.musicapply.userservice.commands;

import lombok.Value;

@Value
public class UpdateUserEmailCommand {

    String userId;
    String email;

}
