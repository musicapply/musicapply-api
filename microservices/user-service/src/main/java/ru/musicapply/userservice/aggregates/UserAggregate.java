package ru.musicapply.userservice.aggregates;

import ru.musicapply.microarchitecture.Aggregate;
import ru.musicapply.microarchitecture.AggregateBuilder;
import ru.musicapply.sharedmodel.UserId;
import ru.musicapply.userservice.commands.RegisterUserCommand;
import ru.musicapply.userservice.commands.UpdateUserEmailCommand;
import ru.musicapply.userservice.event.UserCreatedEvent;
import ru.musicapply.userservice.event.UserUpdatedEmailEvent;


@Aggregate
public class UserAggregate {

    private UserRootEntity userRootEntity;

    private UserAggregate(UserAggregateBuilder builder) {
        this.userRootEntity = new UserRootEntity(
                new UserId(builder.userId),
                builder.email,
                builder.username,
                builder.avatarUrl
        );
    }

    public UserCreatedEvent handleRegisterUserCommand(RegisterUserCommand command) {
        // validation

        return UserCreatedEvent.builder()
                               .userId(command.getUserId())
                               .email(command.getEmail())
                               .username(command.getUsername())
                               .build();
    }

    public UserUpdatedEmailEvent handleUpdateUserEmailCommand(UpdateUserEmailCommand command) {
        // validation

        return UserUpdatedEmailEvent.builder()
                                    .userId(command.getUserId())
                                    .email(command.getEmail())
                                    .build();
    }

    public void apply(UserCreatedEvent event) {
        userRootEntity.username = event.getUsername();
        userRootEntity.email = event.getEmail();
        userRootEntity.userId = new UserId(event.getUserId());
    }

    public void apply(UserUpdatedEmailEvent event) {
        userRootEntity.email = event.getEmail();
    }

    public String getEmail() {
        return userRootEntity.email;
    }

    UserRootEntity getUserRootEntity() {
        return userRootEntity;
    }

    public UserId getUserId() {
        return userRootEntity.userId;
    }

    public static UserAggregate.UserAggregateBuilder builder() {
        return new UserAggregate.UserAggregateBuilder();
    }

    @AggregateBuilder
    public static class UserAggregateBuilder {

        private String userId;
        private String email;
        private String username;
        private String avatarUrl;


        private UserAggregateBuilder() {
        }

        public UserAggregateBuilder forUser(String userId) {
            this.userId = userId;
            return this;
        }

        public UserAggregateBuilder email(String email) {
            this.email = email;
            return this;
        }

        public UserAggregateBuilder username(String username) {
            this.username = username;
            return this;
        }

        public UserAggregateBuilder avatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
            return this;
        }

        public UserAggregate build() {
            return new UserAggregate(this);
        }

    }

}
