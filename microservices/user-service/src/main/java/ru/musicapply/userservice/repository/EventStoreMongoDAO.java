package ru.musicapply.userservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;


@Repository
public interface EventStoreMongoDAO extends ReactiveMongoRepository<EventEntity, ObjectId> {

    Flux<EventEntity> findAllByStreamId(String streamId);

}
