package ru.musicapply.userservice.projection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("usr")
public class UserEntityProjection {

    @Id
    private Integer id;

    @Column("user_id")
    private String userId;
    private String email;
    private String username;

    @Column("avatar_url")
    private String avatarUrl;

}
