package ru.musicapply.userservice.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "authserver")
@Data
public class KeycloakProperties {

    private String username;
    private String password;

}
