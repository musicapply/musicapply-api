package ru.musicapply.userservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.stereotype.Component;
import ru.musicapply.userservice.configuration.IntegrationProperties;
import ru.musicapply.userservice.configuration.KeycloakProperties;

import java.util.Collections;
import java.util.List;


@Component
@RequiredArgsConstructor
@Slf4j
public class KeycloakService {

    private final KeycloakProperties keycloakProperties;
    private final IntegrationProperties integrationProperties;

    public void assignRoleToUser(String userRole, String userId) {
        String authServerUrl = String.format("https://%s/auth",  integrationProperties.getAuthServer());
        log.debug("(Assign role) Will connect to auth server on: {}", authServerUrl);

        try (Keycloak keycloak = Keycloak.getInstance(authServerUrl,
                "master",
                keycloakProperties.getUsername(),
                keycloakProperties.getPassword(),
                "admin-cli"
        )) {

            UserResource userResource = keycloak.realm("musicapply")
                                                .users()
                                                .get(userId);

            List<RoleRepresentation> roleRepresentationList = userResource.roles()
                                                                          .realmLevel()
                                                                          .listAvailable();

            for (RoleRepresentation roleRepresentation : roleRepresentationList)
            {
                if (roleRepresentation.getName().equals(userRole))
                {
                    userResource.roles()
                                .realmLevel()
                                .add(Collections.singletonList(roleRepresentation));
                    break;
                }
            }
        }

    }

    public void removeUserRole(String userRole, String userId) {
        String authServerUrl = String.format("https://%s/auth",  integrationProperties.getAuthServer());
        log.debug("(Revoke role) Will connect to auth server on: {}", authServerUrl);

        try (Keycloak keycloak = Keycloak.getInstance(authServerUrl,
                "master",
                keycloakProperties.getUsername(),
                keycloakProperties.getPassword(),
                "admin-cli"
        )) {
            UserResource userResource = keycloak.realm("musicapply")
                                                .users()
                                                .get(userId);

            List<RoleRepresentation> roleRepresentationList = userResource.roles()
                                                                          .realmLevel()
                                                                          .listAll();

            for (RoleRepresentation roleRepresentation : roleRepresentationList)
            {
                if (roleRepresentation.getName().equals(userRole))
                {
                    userResource.roles()
                                .realmLevel()
                                .remove(Collections.singletonList(roleRepresentation));
                    break;
                }
            }
        }
    }

}
