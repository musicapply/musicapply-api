package ru.musicapply.userservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.musicapply.userservice.commands.RegisterUserCommand;
import ru.musicapply.userservice.commands.UpdateUserEmailCommand;
import ru.musicapply.userservice.service.UserService;


@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "app", name = "profile", havingValue = "write", matchIfMissing = true)
public class UserWriteController {

    private final UserService userService;

    @PostMapping("/register-user")
    public Mono<Void> registerUser(@RequestBody RegisterUserCommand command) {
        return userService.registerUser(command);
    }

    @PostMapping("/update-email")
    public Mono<Void> updateEmail(@RequestBody UpdateUserEmailCommand command) {
        return userService.updateUserEmail(command);
    }

}
