package ru.musicapply.userservice.service.messaging;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Sinks;
import ru.musicapply.api.event.CreateCustomerEvent;
import ru.musicapply.userservice.event.UserCreatedEvent;


@Component
@RequiredArgsConstructor
public class CustomerPublisher {

    private final Sinks.Many<CreateCustomerEvent> customerSink;


    public void raiseCustomerEvent(UserCreatedEvent userCreatedEvent) {
        CreateCustomerEvent event = new CreateCustomerEvent(userCreatedEvent.getUserId());
        this.customerSink.tryEmitNext(event);
    }

}
