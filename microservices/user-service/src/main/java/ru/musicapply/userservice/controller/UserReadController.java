package ru.musicapply.userservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.musicapply.api.userservice.api.UserApi;
import ru.musicapply.api.userservice.dto.UserInfoDto;
import ru.musicapply.userservice.security.UserPrincipal;
import ru.musicapply.userservice.service.UserService;


@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "app", name = "profile", havingValue = "read", matchIfMissing = true)
public class UserReadController implements UserApi {

    private final UserService userService;


    @Override
    public Mono<UserInfoDto> getUser() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return userService.findUserByUserId(userPrincipal.getUserId());
                                     });
    }
}
