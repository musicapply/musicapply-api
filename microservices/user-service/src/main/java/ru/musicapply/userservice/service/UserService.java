package ru.musicapply.userservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Mono;
import ru.musicapply.api.userservice.dto.UserInfoDto;
import ru.musicapply.userservice.commands.RegisterUserCommand;
import ru.musicapply.userservice.commands.UpdateUserEmailCommand;
import ru.musicapply.userservice.mapper.UserMapper;
import ru.musicapply.userservice.projection.UserEntityProjection;
import ru.musicapply.userservice.repository.UserRepository;
import ru.musicapply.userservice.service.messaging.CustomerPublisher;
import ru.musicapply.util.exception.ResourceNotFoundException;


@Service
@RequiredArgsConstructor
public class UserService {

    private final UserAggregateService userAggregateService;
    private final UserAggregateProjector projector;
    private final UserRepository userRepository;
    private final CustomerPublisher publisher;
    private final UserMapper mapper;

    private final TransactionalOperator reactiveMongoTransactionalOperator;
    private final TransactionalOperator r2dbcTransactionalOperator;

    public Mono<Void> registerUser(RegisterUserCommand command) {
        return userAggregateService.handleRegisterUserCommand(command)
                                   .flatMap(event -> Mono.when(
                                           projector.projectUserCreatedEvent(event),
                                           Mono.fromRunnable(() -> publisher.raiseCustomerEvent(event))
                                           )
                                   )
                                   .as(reactiveMongoTransactionalOperator::transactional)
                                   .as(r2dbcTransactionalOperator::transactional)
                                   .then(Mono.empty());
    }

    public Mono<Void> updateUserEmail(UpdateUserEmailCommand command) {
        return userAggregateService.handleUpdateUserEmailCommand(command)
                                   .flatMap(projector::projectUserUpdatedEmailEvent)
                                   .then(Mono.empty());
    }


    public Mono<UserInfoDto> findUserByUserId(String userId) {
        return userRepository.findByUserId(userId)
                             .map(mapper::toResponse)
                             .switchIfEmpty(Mono.error(new ResourceNotFoundException("User", "userId", userId)));
    }
}
