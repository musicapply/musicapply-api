package ru.musicapply.userservice.aggregates;

import ru.musicapply.sharedmodel.UserId;


class UserRootEntity {

    UserId userId;
    String email;
    String username;
    String avatarUrl;

    UserRootEntity(
            UserId userId,
            String email,
            String username,
            String avatarUrl
    ) {
        this.userId = userId;
        this.email = email;
        this.username = username;
        this.avatarUrl = avatarUrl;
    }

}
