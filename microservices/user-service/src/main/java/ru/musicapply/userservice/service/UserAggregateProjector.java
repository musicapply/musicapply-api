package ru.musicapply.userservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.musicapply.userservice.event.UserCreatedEvent;
import ru.musicapply.userservice.event.UserUpdatedEmailEvent;
import ru.musicapply.userservice.projection.UserEntityProjection;
import ru.musicapply.userservice.repository.UserRepository;
import ru.musicapply.util.exception.ResourceNotFoundException;


@Component
@RequiredArgsConstructor
public class UserAggregateProjector {

    private final UserRepository userRepository;

    public Mono<Void> projectUserCreatedEvent(UserCreatedEvent event) {
        return Mono.just(event)
                   .map(e -> UserEntityProjection.builder()
                                                 .userId(e.getUserId())
                                                 .username(e.getUsername())
                                                 .email(e.getEmail())
                                                 .avatarUrl("default.webp")
                                                 .build()
                   )
                   .flatMap(userRepository::save)
                   .then(Mono.empty());
    }

    public Mono<Void> projectUserUpdatedEmailEvent(UserUpdatedEmailEvent event) {
        return userRepository.findByUserId(event.getUserId())
                             .map(user -> {
                                 user.setEmail(event.getEmail());
                                 return user;
                             })
                             .flatMap(userRepository::save)
                             .switchIfEmpty(Mono.error(new ResourceNotFoundException("User", "userId", event.getUserId())))
                             .then(Mono.empty());
    }

}
