package ru.musicapply.userservice.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Schedulers;
import ru.musicapply.api.event.CreateCustomerEvent;
import ru.musicapply.api.event.EventType;
import ru.musicapply.api.event.RevokeUserRoleEvent;
import ru.musicapply.api.event.UpdateUserRoleEvent;
import ru.musicapply.userservice.service.KeycloakService;

import java.util.function.Consumer;
import java.util.function.Supplier;


@Configuration
@RequiredArgsConstructor
public class MessagingConfiguration {

    private final KeycloakService keycloakService;

    @Bean
    public Sinks.Many<CreateCustomerEvent> customerSink() {
        return Sinks.many().unicast().onBackpressureBuffer();
    }

    @Bean
    public Supplier<Flux<CreateCustomerEvent>> customerSupplier(Sinks.Many<CreateCustomerEvent> sink){
        return sink::asFlux;
    }

    @Bean
    public Consumer<Flux<UpdateUserRoleEvent>> updateUserRoleConsumer() {
        return flux -> flux.filter(e -> e.getType() == EventType.UPDATE_USER_ROLE)
                           .publishOn(Schedulers.boundedElastic())
                           .map(e -> {
                               this.keycloakService.assignRoleToUser(e.getRole(), e.getUserId());
                               return e;
                           })
                           .subscribeOn(Schedulers.single())
                           .subscribe();
    }

    @Bean
    public Consumer<Flux<RevokeUserRoleEvent>> revokeUserRoleConsumer() {
        return flux -> flux.filter(e -> e.getType() == EventType.REVOKE_USER_ROLE)
                           .publishOn(Schedulers.boundedElastic())
                           .map(e -> {
                               this.keycloakService.removeUserRole(e.getRole(), e.getUserId());
                               return e;
                           })
                           .subscribeOn(Schedulers.single())
                           .subscribe();
    }

}
