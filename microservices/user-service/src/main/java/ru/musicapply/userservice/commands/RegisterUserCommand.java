package ru.musicapply.userservice.commands;

import lombok.Value;

@Value
public class RegisterUserCommand {

    String userId;
    String email;
    String username;

}
