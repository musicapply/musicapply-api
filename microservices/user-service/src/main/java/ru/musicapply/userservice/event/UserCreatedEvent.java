package ru.musicapply.userservice.event;

import lombok.Builder;
import lombok.Value;


@Value
@Builder
public class UserCreatedEvent {

    String userId;
    String username;
    String email;

}
