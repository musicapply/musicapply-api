package ru.musicapply.userservice.repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface EventStore {

    Mono<EventEntity> addEvent(EventEntity event);
    Flux<EventEntity> getEvents(String userId);

}
