#User microservice

### Environment variables  
  * READ_DB_HOST (postgres host address)
  * READ_DB_PORT (postgres port)
  * READ_DB_DATABASE_NAME (database name)
  * READ_DB_USERNAME (postgres username)
  * READ_DB_PASSWORD (postgres password)
  * WRITE_DB_HOST (mongodb host address)
  * WRITE_DB_PORT (mongodb port)
  * WRITE_DB_DATABASE (database name)
  * WRITE_DB_USERNAME (mongodb username)
  * WRITE_DB_PASSWORD (mongodb password)
  * KAFKA_BROKER_HOST (apache kafka broker host address)
  * KAFKA_BROKER_PORT (apache kafka broker port)
  * KEYCLOAK_ADMIN_USERNAME (keycloak admin username)
  * KEYCLOAK_ADMIN_PASSWORD (keycloak admin password)

### Run in IDE

1. Add Environment variables in `Run Configuration`
2. Start read/write databases and Kafka broker

### Run in `docker-compose`

1. Build  
  `./gradlew :microservices:user-service:build  `  
2. Build image  
  `docker build -f src/main/docker/Dockerfile -t musicapply/user-service:1 .`  
3. Run  
  `docker-compose up -d user-service`