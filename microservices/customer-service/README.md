#Customer microservice

### Environment variables  
  * KAFKA_BROKER_HOST (apache kafka broker host address)
  * KAFKA_BROKER_PORT (apache kafka broker port)
  * READ_DB_HOST (postgres database host address)
  * READ_DB_PORT (postgres database port)
  * READ_DB_DATABASE_NAME (postgres database name)
  * READ_DB_USERNAME (postgres database username)
  * READ_DB_PASSWORD (postgres database password)  
  * WRITE_DB_HOST (mongodb database host address)  
  * WRITE_DB_PORT (mongodb database port)  
  * WRITE_DB_DATABASE (mongodb database name)  
  * WRITE_DB_USERNAME (mongodb database username)  
  * WRITE_DB_PASSWORD (mongodb database password)    

### Run in IDE

1. Add Environment variables in `Run Configuration`
2. Start database and Kafka broker

### Run in `docker-compose`

1. Build  
  `./gradlew :microservices:customer-service:build  `  
2. Build image  
  `docker build -f src/main/docker/Dockerfile -t musicapply/customer-service:1 .`  
3. Run  
  `docker-compose up -d customer-service`