CREATE TABLE customer(
    id                          SERIAL          NOT NULL,
    user_id                     VARCHAR(36)     NOT NULL,
    subscription_id             VARCHAR(36),
    stripe_subscription_id      VARCHAR(36),
    start                       TIMESTAMP,
    next_payment                TIMESTAMP,
    product_id                  VARCHAR(36),
    subscription_status         VARCHAR(30)
);

ALTER TABLE IF EXISTS customer
    ADD CONSTRAINT PK_customer__id
        PRIMARY KEY (id);

ALTER TABLE IF EXISTS customer
    ADD CONSTRAINT U_customer__user_id
        UNIQUE (user_id);

ALTER TABLE IF EXISTS customer
    ADD CONSTRAINT  U_customer__id_user_id
        UNIQUE (id, user_id);