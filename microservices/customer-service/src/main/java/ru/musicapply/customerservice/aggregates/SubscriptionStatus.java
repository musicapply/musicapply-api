package ru.musicapply.customerservice.aggregates;

enum SubscriptionStatus {

    ACTIVE,
    PENDING,
    PAYMENT_ERROR,
    DISABLED

}
