package ru.musicapply.customerservice.projection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("customer")
public class CustomerAggregateProjection {

    @Id
    private Integer id;

    @Column("user_id")
    private String userId;

    @Column("subscription_id")
    private String subscriptionId;

    @Column("stripe_subscription_id")
    private String stripeSubscriptionId;

    @Column("start")
    private LocalDateTime start;

    @Column("next_payment")
    private LocalDateTime nextPayment;

    @Column("product_id")
    private String productId;

    @Column("subscription_status")
    private String subscriptionStatus;

}
