package ru.musicapply.customerservice.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class EventStoreMongo implements EventStore {

    private final EventStoreMongoDAO eventStoreDAO;

    @Override
    public Mono<EventEntity> addEvent(EventEntity event) {
        return eventStoreDAO.save(event);
    }

    @Override
    public Flux<EventEntity> getEvents(String userId) {
        return eventStoreDAO.findAllByStreamId(userId);
    }

}
