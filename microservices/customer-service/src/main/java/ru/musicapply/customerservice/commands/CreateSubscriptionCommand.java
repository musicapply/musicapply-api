package ru.musicapply.customerservice.commands;

import lombok.Value;

import java.time.LocalDateTime;


@Value
public class CreateSubscriptionCommand {

    String subscriptionId;
    String userId;
    String productId;
    String stripeSubscriptionId;
    LocalDateTime startSubscriptionDate;
    LocalDateTime nextPaymentDate;

}
