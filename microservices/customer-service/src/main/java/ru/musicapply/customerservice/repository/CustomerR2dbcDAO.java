package ru.musicapply.customerservice.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.musicapply.customerservice.projection.CustomerAggregateProjection;

@Repository
public interface CustomerR2dbcDAO extends R2dbcRepository<CustomerAggregateProjection, Integer> {

    Mono<CustomerAggregateProjection> findByUserId(String userId);

}
