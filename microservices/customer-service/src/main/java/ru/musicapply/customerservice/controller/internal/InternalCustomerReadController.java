package ru.musicapply.customerservice.controller.internal;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.musicapply.api.customerservice.internal.api.InternalCustomerApi;
import ru.musicapply.api.customerservice.internal.dto.InternalCustomerResponseDto;
import ru.musicapply.customerservice.service.CustomerService;


@RestController
@RequestMapping("/internal/customer")
@RequiredArgsConstructor
public class InternalCustomerReadController implements InternalCustomerApi {

    private final CustomerService customerService;

    @Override
    public Mono<InternalCustomerResponseDto> internalGetCustomerByUserId(String userId) {
        return customerService.internalFindCustomerByUserId(userId);
    }

}
