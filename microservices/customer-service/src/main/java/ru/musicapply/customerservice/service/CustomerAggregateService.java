package ru.musicapply.customerservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import ru.musicapply.customerservice.aggregates.CustomerAggregate;
import ru.musicapply.customerservice.commands.CancelSubscriptionCommand;
import ru.musicapply.customerservice.commands.CreateCustomerCommand;
import ru.musicapply.customerservice.commands.CreateSubscriptionCommand;
import ru.musicapply.customerservice.commands.UpdateCustomerSubscriptionPaymentDateCommand;
import ru.musicapply.customerservice.domainevents.CustomerCreatedEvent;
import ru.musicapply.customerservice.domainevents.CustomerSubscriptionPaymentDateUpdatedEvent;
import ru.musicapply.customerservice.domainevents.SubscriptionCancelledEvent;
import ru.musicapply.customerservice.domainevents.SubscriptionCreatedEvent;
import ru.musicapply.customerservice.repository.EventEntity;
import ru.musicapply.customerservice.repository.EventStore;
import ru.musicapply.util.exception.ResourceNotFoundException;


@Component
@RequiredArgsConstructor
public class CustomerAggregateService {

    private final EventStore writeRepository;

    public Mono<CustomerCreatedEvent> handleCreateCustomerCommand(CreateCustomerCommand command) {
        return Mono.just(CustomerAggregate.builder().build())
                   .flatMap(aggregate -> {
                       CustomerCreatedEvent event = aggregate.handleCreateCustomerCommand(command);
                       Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                                    .streamId(command.getUserId())
                                                                                    .data(event)
                                                                                    .build()
                       );

                       return Mono.zip(mono, Mono.just(event));
                   })
                   .map(Tuple2::getT2);
    }

    public Mono<SubscriptionCreatedEvent> handleCreateSubscriptionCommand(CreateSubscriptionCommand command) {
        return recreateCustomerState(command.getUserId()).flatMap(aggregate -> {
                    SubscriptionCreatedEvent event = aggregate.handleCreateSubscriptionCommand(command);
                    Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                                 .streamId(command.getUserId())
                                                                                 .data(event)
                                                                                 .build()
                    );

                    return Mono.zip(mono, Mono.just(event));
                })
                .map(Tuple2::getT2);
    }

    public Mono<CustomerSubscriptionPaymentDateUpdatedEvent> handleUpdateCustomerSubscriptionPaymentDateCommand(UpdateCustomerSubscriptionPaymentDateCommand command) {
        return recreateCustomerState(command.getUserId()).flatMap(aggregate -> {
            CustomerSubscriptionPaymentDateUpdatedEvent event =
                    aggregate.handleUpdateCustomerSubscriptionPaymentDateCommand(command);
            Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                         .streamId(command.getUserId())
                                                                         .data(event)
                                                                         .build()
            );

            return Mono.zip(mono, Mono.just(event));
        })
        .map(Tuple2::getT2);
    }

    public Mono<SubscriptionCancelledEvent> handleCancelSubscriptionCommand(CancelSubscriptionCommand command) {
        return recreateCustomerState(command.getUserId()).flatMap(aggregate -> {
            SubscriptionCancelledEvent event = aggregate.handleCancelSubscriptionCommand(command);
            Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                         .streamId(command.getUserId())
                                                                         .data(event)
                                                                         .build()
            );

            return Mono.zip(mono, Mono.just(event));
        })
        .map(Tuple2::getT2);
    }

    private Mono<CustomerAggregate> recreateCustomerState(String userId) {
        CustomerAggregate customerAggregate = CustomerAggregate.builder().build();
        return writeRepository.getEvents(userId)
                              .map(event -> {
                                  Object sourceEvent = event.getData();
                                  if (sourceEvent instanceof CustomerCreatedEvent) {
                                      customerAggregate.apply((CustomerCreatedEvent) sourceEvent);
                                  }
                                  if (sourceEvent instanceof SubscriptionCreatedEvent) {
                                      customerAggregate.apply((SubscriptionCreatedEvent) sourceEvent);
                                  }
                                  if (sourceEvent instanceof CustomerSubscriptionPaymentDateUpdatedEvent) {
                                      customerAggregate.apply((CustomerSubscriptionPaymentDateUpdatedEvent) sourceEvent);
                                  }
                                  if (sourceEvent instanceof SubscriptionCancelledEvent) {
                                      customerAggregate.apply((SubscriptionCancelledEvent) sourceEvent);
                                  }
                                  return event;
                              })
                              .switchIfEmpty(Mono.error(new ResourceNotFoundException("Customer", "userId", userId)))
                              .then(Mono.just(customerAggregate));
    }

}
