package ru.musicapply.customerservice.domainevents;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;


@Value
@Builder
public class SubscriptionCreatedEvent {

    String subscriptionId;
    String userId;
    String productId;
    String stripeSubscriptionId;
    LocalDateTime startSubscriptionDate;
    LocalDateTime nextPaymentDate;

}
