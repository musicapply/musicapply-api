package ru.musicapply.customerservice.commands;

import lombok.Value;

@Value
public class CreateCustomerCommand {

    String userId;

}
