package ru.musicapply.customerservice.domainevents;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class CustomerCreatedEvent {

    String userId;
    String subscriptionStatus;

}
