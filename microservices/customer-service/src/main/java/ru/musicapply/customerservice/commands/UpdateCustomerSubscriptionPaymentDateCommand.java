package ru.musicapply.customerservice.commands;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class UpdateCustomerSubscriptionPaymentDateCommand {

    String userId;
    LocalDateTime nextPaymentDate;

}
