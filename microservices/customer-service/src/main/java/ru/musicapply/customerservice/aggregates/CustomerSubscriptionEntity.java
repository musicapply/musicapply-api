package ru.musicapply.customerservice.aggregates;

import ru.musicapply.sharedmodel.ProductId;
import ru.musicapply.sharedmodel.SubscriptionId;

import java.time.LocalDateTime;


class CustomerSubscriptionEntity {

    SubscriptionId id;
    String stripeSubscriptionId;
    LocalDateTime start;
    LocalDateTime nextPayment;
    ProductId productId;
    SubscriptionStatus status;

    CustomerSubscriptionEntity(
        SubscriptionId subscriptionId,
        String stripeSubscriptionId,
        LocalDateTime start,
        LocalDateTime nextPayment,
        ProductId productId,
        SubscriptionStatus status
    ) {
        this.id = subscriptionId;
        this.stripeSubscriptionId = stripeSubscriptionId;
        this.start = start;
        this.nextPayment = nextPayment;
        this.productId = productId;
        this.status = status;
    }

}
