package ru.musicapply.customerservice.aggregates;

import ru.musicapply.customerservice.commands.CancelSubscriptionCommand;
import ru.musicapply.customerservice.commands.CreateCustomerCommand;
import ru.musicapply.customerservice.commands.CreateSubscriptionCommand;
import ru.musicapply.customerservice.commands.UpdateCustomerSubscriptionPaymentDateCommand;
import ru.musicapply.customerservice.domainevents.CustomerCreatedEvent;
import ru.musicapply.customerservice.domainevents.CustomerSubscriptionPaymentDateUpdatedEvent;
import ru.musicapply.customerservice.domainevents.SubscriptionCancelledEvent;
import ru.musicapply.customerservice.domainevents.SubscriptionCreatedEvent;
import ru.musicapply.microarchitecture.Aggregate;
import ru.musicapply.microarchitecture.AggregateBuilder;
import ru.musicapply.sharedmodel.ProductId;
import ru.musicapply.sharedmodel.SubscriptionId;
import ru.musicapply.sharedmodel.UserId;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;


@Aggregate
public class CustomerAggregate {

    private UserId userId;
    private CustomerSubscriptionEntity subscription;

    private CustomerAggregate(CustomerAggregateBuilder builder) {
        this.userId = new UserId(builder.userId);
        this.subscription = new CustomerSubscriptionEntity(
                new SubscriptionId(builder.subscriptionId),
                builder.stripeSubscriptionId,
                builder.start,
                builder.nextPayment,
                new ProductId(builder.productId),
                builder.status
        );
    }

    public CustomerCreatedEvent handleCreateCustomerCommand(CreateCustomerCommand command) {
        return CustomerCreatedEvent.builder()
                                   .userId(command.getUserId())
                                   .subscriptionStatus(SubscriptionStatus.DISABLED.toString())
                                   .build();
    }

    public SubscriptionCreatedEvent handleCreateSubscriptionCommand(CreateSubscriptionCommand command) {
        return SubscriptionCreatedEvent.builder()
                                       .subscriptionId(command.getSubscriptionId())
                                       .userId(command.getUserId())
                                       .productId(command.getProductId())
                                       .stripeSubscriptionId(command.getStripeSubscriptionId())
                                       .startSubscriptionDate(command.getStartSubscriptionDate())
                                       .nextPaymentDate(command.getNextPaymentDate())
                                       .build();
    }

    public CustomerSubscriptionPaymentDateUpdatedEvent handleUpdateCustomerSubscriptionPaymentDateCommand(UpdateCustomerSubscriptionPaymentDateCommand command) {
        return CustomerSubscriptionPaymentDateUpdatedEvent.builder()
                                                          .userId(command.getUserId())
                                                          .nextPaymentDate(command.getNextPaymentDate())
                                                          .build();
    }

    public SubscriptionCancelledEvent handleCancelSubscriptionCommand(CancelSubscriptionCommand command) {
        return SubscriptionCancelledEvent.builder()
                                         .userId(command.getUserId())
                                         .build();
    }

    public void apply(CustomerCreatedEvent event) {
        this.userId = new UserId(event.getUserId());
        this.subscription.status = SubscriptionStatus.valueOf(event.getSubscriptionStatus());
    }

    public void apply(SubscriptionCreatedEvent event) {
        this.subscription.status = SubscriptionStatus.ACTIVE;
        this.subscription.id = new SubscriptionId(event.getSubscriptionId());
        this.subscription.productId = new ProductId(event.getProductId());
        this.subscription.stripeSubscriptionId = event.getStripeSubscriptionId();
        this.subscription.start = event.getStartSubscriptionDate();
        this.subscription.nextPayment = event.getNextPaymentDate();
    }

    public void apply(CustomerSubscriptionPaymentDateUpdatedEvent event) {
        this.subscription.nextPayment = event.getNextPaymentDate();
    }

    public void apply(SubscriptionCancelledEvent event) {
        this.subscription = new CustomerSubscriptionEntity(
                null,
                null,
                null,
                null,
                null,
                SubscriptionStatus.DISABLED
        );
    }

    public static CustomerAggregate.CustomerAggregateBuilder builder() {
        return new CustomerAggregate.CustomerAggregateBuilder();
    }

    @AggregateBuilder
    public static class CustomerAggregateBuilder {

        private String userId;
        private String subscriptionId;
        private String stripeSubscriptionId;
        private LocalDateTime start;
        private LocalDateTime nextPayment;
        private String productId;
        private SubscriptionStatus status;

        private CustomerAggregateBuilder() {
            this.status = SubscriptionStatus.DISABLED;
        }

        public CustomerAggregateBuilder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public CustomerAggregateBuilder subscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public CustomerAggregateBuilder stripeSubscriptionId(String stripeSubscriptionId) {
            this.stripeSubscriptionId = stripeSubscriptionId;
            return this;
        }

        public CustomerAggregateBuilder start(LocalDateTime start) {
            this.start = start;
            return this;
        }

        public CustomerAggregateBuilder nextPayment(LocalDateTime nextPayment) {
            this.nextPayment = nextPayment;
            return this;
        }

        public CustomerAggregateBuilder status(String subscriptionStatus) {
            this.status = SubscriptionStatus.valueOf(subscriptionStatus);
            return this;
        }

        public CustomerAggregateBuilder productId(String productId) {
            this.productId = productId;
            return this;
        }

        public CustomerAggregate build() {
            return new CustomerAggregate(this);
        }

    }

}
