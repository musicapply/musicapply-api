package ru.musicapply.customerservice.configuration;

import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.ReactiveMongoDatabaseFactory;
import org.springframework.data.mongodb.ReactiveMongoTransactionManager;
import org.springframework.r2dbc.connection.R2dbcTransactionManager;
import org.springframework.transaction.reactive.TransactionalOperator;

@Configuration
public class TransactionConfiguration {

    @Bean
    @Qualifier("mongoTransactionManager")
    public ReactiveMongoTransactionManager mongoTransactionManager(ReactiveMongoDatabaseFactory factory) {
        return new ReactiveMongoTransactionManager(factory);
    }

    @Bean
    @Qualifier("reactiveMongoTransactionalOperator")
    public TransactionalOperator reactiveMongoTransactionalOperator(ReactiveMongoTransactionManager txm) {
        return TransactionalOperator.create(txm);
    }

    @Bean
    @Qualifier("r2dbcTransactionManager")
    public R2dbcTransactionManager r2dbcTransactionManager(ConnectionFactory factory) {
        return new R2dbcTransactionManager(factory);
    }

    @Bean
    @Qualifier("r2dbcTransactionalOperator")
    public TransactionalOperator r2dbcTransactionalOperator(R2dbcTransactionManager txm) {
        return TransactionalOperator.create(txm);
    }

}
