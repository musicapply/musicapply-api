package ru.musicapply.customerservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.musicapply.api.customerservice.dto.CustomerResponseDto;
import ru.musicapply.api.customerservice.internal.dto.InternalCustomerResponseDto;
import ru.musicapply.customerservice.projection.CustomerAggregateProjection;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE
)
public interface CustomerMapper {

    @Mapping(target = "status", source = "subscriptionStatus")
    CustomerResponseDto toResponse(CustomerAggregateProjection projection);

    InternalCustomerResponseDto toInternalResponse(CustomerAggregateProjection projection);

}
