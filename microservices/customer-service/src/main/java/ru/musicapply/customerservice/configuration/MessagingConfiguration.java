package ru.musicapply.customerservice.configuration;

import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.event.*;
import ru.musicapply.customerservice.commands.CancelSubscriptionCommand;
import ru.musicapply.customerservice.commands.CreateCustomerCommand;
import ru.musicapply.customerservice.commands.CreateSubscriptionCommand;
import ru.musicapply.customerservice.commands.UpdateCustomerSubscriptionPaymentDateCommand;
import ru.musicapply.customerservice.service.CustomerService;

import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;


@Configuration
@RequiredArgsConstructor
public class MessagingConfiguration {

    private final CustomerService customerService;

    @Bean
    public Function<Flux<CancelSubscriptionEvent>, Flux<RevokeUserRoleEvent>> cancelSubscriptionProcessor() {
        return flux -> flux.filter(e -> e.getType() == EventType.CANCEL_SUBSCRIPTION)
                           .flatMap(this::processCancelSubscription);
    }

    private Mono<RevokeUserRoleEvent> processCancelSubscription(CancelSubscriptionEvent cancelSubscriptionEvent) {
        CancelSubscriptionCommand command = new CancelSubscriptionCommand(cancelSubscriptionEvent.getUserId());
        return customerService.cancelSubscription(command)
                              .then(Mono.just(new RevokeUserRoleEvent(command.getUserId(), "premium_user")));
    }


    @Bean
    public Function<Flux<CreateCustomerEvent>, Flux<CreatePaymentProfileEvent>> customerProcessor() {
        return flux -> flux.filter(e -> e.getType() == EventType.CREATE_CUSTOMER)
                           .flatMap(this::processCustomer);
    }

    @Bean
    public Function<Flux<CreateCustomerSubscriptionEvent>, Flux<UpdateUserRoleEvent>> createSubscriptionProcessor() {
        return flux -> flux.filter(e -> e.getType() == EventType.CREATE_CUSTOMER_SUBSCRIPTION)
                           .flatMap(this::processSubscriptionCreated);
    }

    @Bean
    public Consumer<Flux<UpdateSubscriptionInfoEvent>> updateSubscriptionInfoConsumer() {
        return flux -> flux.filter(e -> e.getType() == EventType.UPDATE_SUBSCRIPTION_INFO)
                           .flatMap(
                                   ev -> customerService.updateCustomerSubscriptionInfo(
                                           new UpdateCustomerSubscriptionPaymentDateCommand(
                                                   ev.getUserId(),
                                                   ev.getNextPaymentDate()
                                           )
                                   )
                           )
                           .subscribe();
    }

    private Mono<CreatePaymentProfileEvent> processCustomer(CreateCustomerEvent event) {
        return customerService.createCustomer(new CreateCustomerCommand(event.getUserId()))
                              .then(Mono.just(new CreatePaymentProfileEvent(event.getUserId())));
    }

    private Mono<UpdateUserRoleEvent> processSubscriptionCreated(CreateCustomerSubscriptionEvent event) {
            return customerService.createSubscription(
                    new CreateSubscriptionCommand(
                            UUID.randomUUID().toString(),
                            event.getUserId(),
                            event.getProductId(),
                            event.getStripeSubscriptionId(),
                            event.getStartSubscriptionDate(),
                            event.getNextPaymentDate()
                    )
            )
                                  .then(Mono.just(
                                          new UpdateUserRoleEvent(
                                                  event.getUserId(),
                                                  "premium_user"
                                          )
                                  ));
    }

}
