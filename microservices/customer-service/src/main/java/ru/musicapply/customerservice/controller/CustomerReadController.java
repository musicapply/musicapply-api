package ru.musicapply.customerservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.musicapply.api.customerservice.api.CustomerApi;
import ru.musicapply.api.customerservice.dto.CustomerResponseDto;
import ru.musicapply.customerservice.security.UserPrincipal;
import ru.musicapply.customerservice.service.CustomerService;


@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "app", name = "profile", havingValue = "read", matchIfMissing = true)
public class CustomerReadController implements CustomerApi {

    private final CustomerService customerService;


    @Override
    public Mono<CustomerResponseDto> getCustomerByUserId(String userId) {
        return customerService.findCustomerByUserId(userId);
    }

    @Override
    public Mono<CustomerResponseDto> me() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return customerService.findCustomerByUserId(userPrincipal.getUserId());
                                            });
    }

}
