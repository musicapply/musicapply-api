package ru.musicapply.customerservice.commands;

import lombok.Value;

@Value
public class CancelSubscriptionCommand {

    String userId;

}
