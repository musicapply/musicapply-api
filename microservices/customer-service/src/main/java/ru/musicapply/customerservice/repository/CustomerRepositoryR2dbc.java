package ru.musicapply.customerservice.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.customerservice.projection.CustomerAggregateProjection;


@Component
@RequiredArgsConstructor
public class CustomerRepositoryR2dbc implements CustomerRepository {

    private final CustomerR2dbcDAO customerDAO;

    @Override
    public Mono<CustomerAggregateProjection> save(CustomerAggregateProjection customer) {
        return customerDAO.save(customer);
    }

    @Override
    public Mono<CustomerAggregateProjection> findById(Integer id) {
        return customerDAO.findById(id);
    }

    @Override
    public Mono<CustomerAggregateProjection> findByUserId(String userId) {
        return customerDAO.findByUserId(userId);
    }

    @Override
    public Mono<CustomerAggregateProjection> update(CustomerAggregateProjection customer) {
        return customerDAO.findByUserId(customer.getUserId())
                          .map(pr -> {
                              pr.setSubscriptionId(customer.getSubscriptionId());
                              pr.setStripeSubscriptionId(customer.getStripeSubscriptionId());
                              pr.setStart(customer.getStart());
                              pr.setNextPayment(customer.getNextPayment());
                              pr.setProductId(customer.getProductId());
                              pr.setSubscriptionStatus(customer.getSubscriptionStatus());

                              return pr;
                          })
                          .flatMap(customerDAO::save);
    }

    @Override
    public Flux<CustomerAggregateProjection> findAll() {
        return customerDAO.findAll();
    }

}
