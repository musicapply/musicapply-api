package ru.musicapply.customerservice.domainevents;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@Builder
public class CustomerSubscriptionPaymentDateUpdatedEvent {

    String userId;
    LocalDateTime nextPaymentDate;

}
