package ru.musicapply.customerservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.musicapply.customerservice.domainevents.CustomerCreatedEvent;
import ru.musicapply.customerservice.domainevents.CustomerSubscriptionPaymentDateUpdatedEvent;
import ru.musicapply.customerservice.domainevents.SubscriptionCancelledEvent;
import ru.musicapply.customerservice.domainevents.SubscriptionCreatedEvent;
import ru.musicapply.customerservice.projection.CustomerAggregateProjection;
import ru.musicapply.customerservice.repository.CustomerRepository;

import java.time.LocalDateTime;


@Component
@RequiredArgsConstructor
public class CustomerAggregateProjector {

    private final CustomerRepository customerRepository;

    public Mono<Void> projectCustomerCreatedEvent(CustomerCreatedEvent event) {
        return Mono.just(event)
                   .map(e -> CustomerAggregateProjection.builder()
                                                        .userId(event.getUserId())
                                                        .subscriptionStatus(event.getSubscriptionStatus())
                                                        .build()
                   )
                   .flatMap(customerRepository::save)
                   .then(Mono.empty());
    }

    public Mono<Void> projectSubscriptionCreatedEvent(SubscriptionCreatedEvent event) {
        return customerRepository.findByUserId(event.getUserId())
                                 .map(pr -> {
                                     pr.setSubscriptionId(event.getSubscriptionId());
                                     pr.setProductId(event.getProductId());
                                     pr.setStripeSubscriptionId(event.getStripeSubscriptionId());
                                     pr.setStart(event.getStartSubscriptionDate());
                                     pr.setNextPayment(event.getNextPaymentDate());
                                     pr.setSubscriptionStatus("ACTIVE");

                                     return pr;
                                 })
                                 .flatMap(customerRepository::update)
                                 .then(Mono.empty());
    }

    public Mono<Void> projectCustomerSubscriptionPaymentDateUpdatedEvent(CustomerSubscriptionPaymentDateUpdatedEvent event) {
        return customerRepository.findByUserId(event.getUserId())
                                 .map(pr -> {
                                     pr.setNextPayment(event.getNextPaymentDate());
                                     return pr;
                                 })
                                 .flatMap(customerRepository::update)
                                 .then(Mono.empty());
    }

    public Mono<Void> projectSubscriptionCancelledEvent(SubscriptionCancelledEvent event) {
        return customerRepository.findByUserId(event.getUserId())
                                 .map(pr -> {
                                     pr.setSubscriptionId(null);
                                     pr.setStripeSubscriptionId(null);
                                     pr.setStart(null);
                                     pr.setNextPayment(null);
                                     pr.setProductId(null);
                                     pr.setSubscriptionStatus("DISABLED");

                                     return pr;
                                 })
                                 .flatMap(customerRepository::update)
                                 .then(Mono.empty());
    }

}
