package ru.musicapply.customerservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Mono;
import ru.musicapply.api.customerservice.dto.CustomerResponseDto;
import ru.musicapply.api.customerservice.internal.dto.InternalCustomerResponseDto;
import ru.musicapply.customerservice.commands.CancelSubscriptionCommand;
import ru.musicapply.customerservice.commands.CreateCustomerCommand;
import ru.musicapply.customerservice.commands.CreateSubscriptionCommand;
import ru.musicapply.customerservice.commands.UpdateCustomerSubscriptionPaymentDateCommand;
import ru.musicapply.customerservice.mapper.CustomerMapper;
import ru.musicapply.customerservice.projection.CustomerAggregateProjection;
import ru.musicapply.customerservice.repository.CustomerRepository;
import ru.musicapply.util.exception.ResourceNotFoundException;


@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerAggregateService customerAggregateService;
    private final CustomerAggregateProjector projector;
    private final CustomerRepository customerRepository;
    private final CustomerMapper mapper;

    private final TransactionalOperator reactiveMongoTransactionalOperator;
    private final TransactionalOperator r2dbcTransactionalOperator;


    public Mono<Void> createCustomer(CreateCustomerCommand command) {
        return customerAggregateService.handleCreateCustomerCommand(command)
                                       .flatMap(event -> Mono.when(
                                               projector.projectCustomerCreatedEvent(event),
                                               Mono.fromRunnable(() -> {})
                                               )
                                       )
                                       .as(reactiveMongoTransactionalOperator::transactional)
                                       .as(r2dbcTransactionalOperator::transactional)
                                       .then(Mono.empty());
    }

    public Mono<Void> createSubscription(CreateSubscriptionCommand command) {
        return customerAggregateService.handleCreateSubscriptionCommand(command)
                                       .flatMap(projector::projectSubscriptionCreatedEvent)
                                       .as(reactiveMongoTransactionalOperator::transactional)
                                       .as(r2dbcTransactionalOperator::transactional)
                                       .then(Mono.empty());
    }

    public Mono<CustomerResponseDto> findCustomerByUserId(String userId) {
        return customerRepository.findByUserId(userId)
                                 .map(mapper::toResponse)
                                 .switchIfEmpty(Mono.error(new ResourceNotFoundException("Customer", "userId", userId)));
    }

    public Mono<InternalCustomerResponseDto> internalFindCustomerByUserId(String userId) {
        return customerRepository.findByUserId(userId)
                                 .map(mapper::toInternalResponse)
                                 .switchIfEmpty(Mono.error(new ResourceNotFoundException("Customer", "userId", userId)));
    }

    public Mono<Void> updateCustomerSubscriptionInfo(UpdateCustomerSubscriptionPaymentDateCommand command) {
        return customerAggregateService.handleUpdateCustomerSubscriptionPaymentDateCommand(command)
                                       .flatMap(projector::projectCustomerSubscriptionPaymentDateUpdatedEvent)
                                       .as(reactiveMongoTransactionalOperator::transactional)
                                       .as(r2dbcTransactionalOperator::transactional)
                                       .then(Mono.empty());
    }

    public Mono<Void> cancelSubscription(CancelSubscriptionCommand command) {
        return customerAggregateService.handleCancelSubscriptionCommand(command)
                                       .flatMap(projector::projectSubscriptionCancelledEvent)
                                       .as(reactiveMongoTransactionalOperator::transactional)
                                       .as(r2dbcTransactionalOperator::transactional)
                                       .then(Mono.empty());
    }

}
