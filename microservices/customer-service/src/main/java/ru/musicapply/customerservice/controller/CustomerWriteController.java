package ru.musicapply.customerservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.musicapply.api.customerservice.api.CustomerWriteApi;
import ru.musicapply.customerservice.security.UserPrincipal;
import ru.musicapply.customerservice.service.CustomerService;


@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "app", name = "profile", havingValue = "write", matchIfMissing = true)
public class CustomerWriteController implements CustomerWriteApi {

    private final CustomerService customerService;
}
