package ru.musicapply.customerservice.repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.customerservice.projection.CustomerAggregateProjection;


public interface CustomerRepository {

    Mono<CustomerAggregateProjection> save(CustomerAggregateProjection customer);
    Mono<CustomerAggregateProjection> findById(Integer id);
    Mono<CustomerAggregateProjection> findByUserId(String userId);
    Mono<CustomerAggregateProjection> update(CustomerAggregateProjection customer);
    Flux<CustomerAggregateProjection> findAll();

}
