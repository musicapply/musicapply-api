package ru.musicapply.customerservice.mapper;

import java.util.Date;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import ru.musicapply.api.customerservice.dto.CustomerResponseDto;
import ru.musicapply.customerservice.projection.CustomerAggregateProjection;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-06-01T11:23:19+0600",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.3 (Amazon.com Inc.)"
)
@Component
public class CustomerMapperImpl implements CustomerMapper {

    @Override
    public CustomerResponseDto toResponse(CustomerAggregateProjection projection) {
        if ( projection == null ) {
            return null;
        }

        String status = null;
        String userId = null;
        String subscriptionId = null;
        Date start = null;
        Date nextPayment = null;
        String productId = null;

        status = projection.getSubscriptionStatus();
        userId = projection.getUserId();
        subscriptionId = projection.getSubscriptionId();
        start = projection.getStart();
        nextPayment = projection.getNextPayment();
        productId = projection.getProductId();

        CustomerResponseDto customerResponseDto = new CustomerResponseDto( userId, subscriptionId, start, nextPayment, productId, status );

        return customerResponseDto;
    }
}
