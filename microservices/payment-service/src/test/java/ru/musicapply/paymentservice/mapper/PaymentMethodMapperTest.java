package ru.musicapply.paymentservice.mapper;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import ru.musicapply.api.paymentservice.dto.PaymentMethodResponseDto;
import ru.musicapply.paymentservice.projections.PaymentMethodProjection;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentMethodMapperTest {

    private final PaymentMethodMapper mapper = Mappers.getMapper(PaymentMethodMapper.class);


    @Test
    public void contextLoad() {
        assertThat(mapper).isNotNull();
    }

    @Test
    public void testMapProjectionToResponse() {
        Integer id = 5;
        String paymentMethodId = UUID.randomUUID().toString();
        String stripeId = "pm_jdw29JwdW29io";
        String cardNumber = "6310";
        String cardBrand = "visa";

        PaymentMethodProjection paymentMethod = new PaymentMethodProjection();
        paymentMethod.setId(id);
        paymentMethod.setPaymentMethodId(paymentMethodId);
        paymentMethod.setStripeId(stripeId);
        paymentMethod.setCardNumber(cardNumber);
        paymentMethod.setCardBrand(cardBrand);

        PaymentMethodResponseDto dto = mapper.toResponse(paymentMethod);

        assertAll(
                () -> assertEquals(paymentMethodId, dto.getPaymentMethodId()),
                () -> assertEquals(cardNumber, dto.getCardNumber()),
                () -> assertEquals(cardBrand, dto.getCardBrand())
        );
    }

}
