package ru.musicapply.paymentservice.domainevents;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PaymentMethodCreatedEvent {

    String userId;
    String paymentMethodId;
    String stripeId;
    String cardNumber;
    String brand;

}
