package ru.musicapply.paymentservice.commands;

import lombok.Value;

@Value
public class CreatePaymentProfileCommand {

    String userId;
    String stripeId;

}
