package ru.musicapply.paymentservice.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.musicapply.paymentservice.projections.PaymentProfileAggregateProjection;

@Repository
public interface PaymentProfileR2dbcDAO extends R2dbcRepository<PaymentProfileAggregateProjection, Integer> {

    Mono<PaymentProfileAggregateProjection> findByUserId(String userId);
    Mono<PaymentProfileAggregateProjection> findByStripeId(String stripeId);

}
