package ru.musicapply.paymentservice.aggregates;

import ru.musicapply.sharedmodel.PaymentMethodId;

class PaymentMethod {

    PaymentMethodId paymentMethodId;
    String stripeId;
    String cardNumber;
    String brand;

    PaymentMethod(
        PaymentMethodId paymentMethodId,
        String stripeId,
        String cardNumber,
        String brand
    ) {
        this.paymentMethodId = paymentMethodId;
        this.stripeId = stripeId;
        this.cardNumber = cardNumber;
        this.brand = brand;
    }

}
