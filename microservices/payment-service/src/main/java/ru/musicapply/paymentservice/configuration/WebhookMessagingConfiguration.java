package ru.musicapply.paymentservice.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import ru.musicapply.api.event.CancelSubscriptionEvent;
import ru.musicapply.api.event.CreateCustomerSubscriptionEvent;
import ru.musicapply.api.event.NewTransactionEvent;

import java.util.function.Supplier;

@Configuration
@RequiredArgsConstructor
public class WebhookMessagingConfiguration {

    @Bean
    public Sinks.Many<CreateCustomerSubscriptionEvent> createSubscriptionSink() {
        return Sinks.many().unicast().onBackpressureBuffer();
    }

    @Bean
    public Supplier<Flux<CreateCustomerSubscriptionEvent>> createSubscriptionSupplier(Sinks.Many<CreateCustomerSubscriptionEvent> sink){
        return sink::asFlux;
    }

    @Bean
    public Sinks.Many<NewTransactionEvent> newTransactionSink() {
        return Sinks.many().unicast().onBackpressureBuffer();
    }

    @Bean
    public Supplier<Flux<NewTransactionEvent>> newTransactionSupplier(Sinks.Many<NewTransactionEvent> sink){
        return sink::asFlux;
    }

    @Bean
    public Sinks.Many<CancelSubscriptionEvent> cancelSubscriptionSink() {
        return Sinks.many().unicast().onBackpressureBuffer();
    }

    @Bean
    public Supplier<Flux<CancelSubscriptionEvent>> cancelSubscriptionSupplier(Sinks.Many<CancelSubscriptionEvent> sink){
        return sink::asFlux;
    }

}
