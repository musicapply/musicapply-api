package ru.musicapply.paymentservice.repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.paymentservice.projections.PaymentMethodProjection;
import ru.musicapply.paymentservice.projections.PaymentProfileAggregateProjection;

public interface PaymentProfileRepository {

    Mono<PaymentProfileAggregateProjection> save(PaymentProfileAggregateProjection paymentProfile);
    Mono<PaymentProfileAggregateProjection> update(PaymentProfileAggregateProjection paymentProfile);
    Mono<PaymentProfileAggregateProjection> findById(Integer id);
    Mono<PaymentProfileAggregateProjection> findByIdWithPaymentMethods(Integer id);
    Mono<PaymentProfileAggregateProjection> findByUserId(String userId);
    Mono<PaymentProfileAggregateProjection> findByStripeId(String stripeId);
    Mono<PaymentProfileAggregateProjection> findByStripeIdWithPaymentMethods(String stripeId);
    Mono<PaymentProfileAggregateProjection> findByUserIdWithPaymentMethods(String userId);
    Flux<PaymentProfileAggregateProjection> findAll();

    Flux<PaymentProfileAggregateProjection> findAllWithPaymentMethods();
    Mono<Void> addPaymentMethodToProfile(PaymentMethodProjection paymentMethod, String userId);
    Mono<Void> removePaymentMethodFromProfile(String paymentMethodId, String userId);

    Mono<PaymentMethodProjection> findPaymentMethodByPaymentMethodId(String paymentMethodId);

    Flux<PaymentMethodProjection> findPaymentMethodsByUserId(String userId);

}
