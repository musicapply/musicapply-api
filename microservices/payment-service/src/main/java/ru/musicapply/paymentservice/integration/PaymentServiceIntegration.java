package ru.musicapply.paymentservice.integration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import ru.musicapply.api.customerservice.internal.api.InternalCustomerApi;
import ru.musicapply.api.customerservice.internal.dto.InternalCustomerResponseDto;
import ru.musicapply.api.productservice.internal.api.InternalProductServiceApi;
import ru.musicapply.api.productservice.internal.dto.InternalProductResponseDto;
import ru.musicapply.paymentservice.configuration.IntegrationProperties;

import java.net.URI;


@Component
@Slf4j
public class PaymentServiceIntegration implements InternalProductServiceApi, InternalCustomerApi {

    private final String PRODUCT_SERVICE_URL;
    private final String CUSTOMER_SERVICE_URL;


    private final WebClient webClient;

    @Autowired
    public PaymentServiceIntegration(
            WebClient.Builder webClientBuilder,
            IntegrationProperties integrationProperties
    ) {
        this.webClient = webClientBuilder.build();
        this.PRODUCT_SERVICE_URL = "http://" + integrationProperties.getProductService();
        this.CUSTOMER_SERVICE_URL = "http://" + integrationProperties.getCustomerService();
    }

    @Override
    public Mono<InternalProductResponseDto> getProductById(String productId) {

        URI url = UriComponentsBuilder.fromUriString(PRODUCT_SERVICE_URL
                + "/internal/product/{productId}")
                                      .build(productId);

        log.debug("Will call the getProductById API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(InternalProductResponseDto.class);
    }

    @Override
    public Mono<InternalProductResponseDto> getProductByStripeId(String stripeId) {
        URI url = UriComponentsBuilder.fromUriString(PRODUCT_SERVICE_URL
                + "/internal/product/getByStripeId/{productId}")
                                      .build(stripeId);

        log.debug("Will call the getProductByStripeId API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(InternalProductResponseDto.class);
    }

    @Override
    public Mono<InternalCustomerResponseDto> internalGetCustomerByUserId(String userId) {
        URI url = UriComponentsBuilder.fromUriString(CUSTOMER_SERVICE_URL
                + "/internal/customer/{userId}")
                                      .build(userId);

        log.debug("Will call the internalGetCustomerByUserId API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(InternalCustomerResponseDto.class);
    }
}
