package ru.musicapply.paymentservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.paymentservice.api.PaymentReadApi;
import ru.musicapply.api.paymentservice.dto.PaymentMethodResponseDto;
import ru.musicapply.api.paymentservice.dto.PaymentProfileResponseDto;
import ru.musicapply.paymentservice.security.UserPrincipal;
import ru.musicapply.paymentservice.service.PaymentProfileService;


@RestController
@RequestMapping("/payment_profile")
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "app", name = "profile", havingValue = "read", matchIfMissing = true)
public class PaymentProfileReadController implements PaymentReadApi {

    private final PaymentProfileService paymentProfileService;


    @Override
    public Flux<PaymentMethodResponseDto> getUserPaymentMethods() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMapMany(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return paymentProfileService.findPaymentMethodsByUserId(userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<PaymentProfileResponseDto> getPaymentProfile() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return paymentProfileService.findPaymentProfileByUserId(userPrincipal.getUserId());
                                            });
    }

}
