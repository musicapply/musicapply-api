package ru.musicapply.paymentservice.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.paymentservice.projections.PaymentMethodProjection;
import ru.musicapply.paymentservice.projections.PaymentProfileAggregateProjection;
import ru.musicapply.paymentservice.projections.ProfileMethod;


@Component
@RequiredArgsConstructor
public class PaymentProfileRepositoryR2dbc implements PaymentProfileRepository {

    private final PaymentProfileR2dbcDAO paymentProfileDAO;
    private final PaymentMethodR2dbcDAO paymentMethodDAO;
    private final ProfileMethodR2dbcDAO profileMethodDAO;

    @Override
    public Mono<PaymentProfileAggregateProjection> save(PaymentProfileAggregateProjection paymentProfile) {
        return paymentProfileDAO.save(paymentProfile);
    }

    @Override
    public Mono<PaymentProfileAggregateProjection> update(PaymentProfileAggregateProjection paymentProfile) {
        return paymentProfileDAO.findById(paymentProfile.getId())
                                .map(pr -> {
                                    pr.setUserId(paymentProfile.getUserId());
                                    pr.setStripeId(paymentProfile.getStripeId());
                                    pr.setDefaultPaymentMethodId(paymentProfile.getDefaultPaymentMethodId());

                                    return pr;
                                })
                                .flatMap(paymentProfileDAO::save);
    }

    @Override
    public Mono<PaymentProfileAggregateProjection> findById(Integer id) {
        return paymentProfileDAO.findById(id);
    }

    @Override
    public Mono<PaymentProfileAggregateProjection> findByIdWithPaymentMethods(Integer id) {
        return paymentProfileDAO.findById(id)
                                .flatMap(aggregateProjection -> profileMethodDAO.findByPaymentProfileId(aggregateProjection.getId())
                                                                                .flatMap(profileMethod -> paymentMethodDAO.findById(profileMethod.getPaymentMethodId()))
                                                                                .doOnNext(aggregateProjection::addPaymentMethodProjection).then(Mono.just(aggregateProjection))
                                ); // refactor
    }

    @Override
    public Mono<PaymentProfileAggregateProjection> findByUserId(String userId) {
        return paymentProfileDAO.findByUserId(userId);
    }

    @Override
    public Mono<PaymentProfileAggregateProjection> findByStripeId(String stripeId) {
        return paymentProfileDAO.findByStripeId(stripeId);
    }

    @Override
    public Mono<PaymentProfileAggregateProjection> findByStripeIdWithPaymentMethods(String stripeId) {
        return paymentProfileDAO.findByStripeId(stripeId)
                                .flatMap(projection -> paymentMethodDAO.findPaymentMethodsByUserId(projection.getUserId())
                                                                       .map(pm -> {
                                                                           projection.addPaymentMethodProjection(pm);
                                                                           return pm;
                                                                       })
                                                                       .then(Mono.just(projection))
                                );
    }

    @Override
    public Mono<PaymentProfileAggregateProjection> findByUserIdWithPaymentMethods(String userId) {
        return paymentProfileDAO.findByUserId(userId)
                                .flatMap(projection -> paymentMethodDAO.findPaymentMethodsByUserId(projection.getUserId())
                                                                       .map(pm -> {
                                                                           projection.addPaymentMethodProjection(pm);
                                                                           return pm;
                                                                       })
                                                                       .then(Mono.just(projection))
                                );
    }

    @Override
    public Flux<PaymentProfileAggregateProjection> findAll() {
        return paymentProfileDAO.findAll();
    }

    @Override
    public Flux<PaymentProfileAggregateProjection> findAllWithPaymentMethods() {
        return paymentProfileDAO.findAll()
                                .flatMap(aggregateProjection -> profileMethodDAO.findByPaymentProfileId(aggregateProjection.getId())
                                                                                .flatMap(profileMethod -> paymentMethodDAO.findById(profileMethod.getPaymentMethodId()))
                                                                                .doOnNext(aggregateProjection::addPaymentMethodProjection)
                                                                                .then(Mono.just(aggregateProjection))
                                );
    }

    @Override
    public Mono<Void> addPaymentMethodToProfile(PaymentMethodProjection paymentMethod, String userId) {
        return paymentProfileDAO.findByUserId(userId)
                                .flatMap(profile ->
                                        paymentMethodDAO.save(paymentMethod)
                                                        .flatMap(method ->
                                                                profileMethodDAO.save(ProfileMethod.builder()
                                                                                                   .paymentProfileId(profile.getId())
                                                                                                   .paymentMethodId(method.getId())
                                                                                                   .build()
                                                                )
                                                        )
                                )
                                .then(Mono.empty());
    }

    @Override
    public Mono<Void> removePaymentMethodFromProfile(String paymentMethodId, String userId) {
        return paymentMethodDAO.findByPaymentMethodId(paymentMethodId)
                               .flatMap(paymentMethod -> profileMethodDAO.deleteProfileMethodByPaymentMethodId(paymentMethod.getId())
                                                                         .then(paymentMethodDAO.deleteById(paymentMethod.getId()))
                               )
                               .then(Mono.empty());
    }

    @Override
    public Mono<PaymentMethodProjection> findPaymentMethodByPaymentMethodId(String paymentMethodId) {
        return paymentMethodDAO.findByPaymentMethodId(paymentMethodId);
    }

    @Override
    public Flux<PaymentMethodProjection> findPaymentMethodsByUserId(String userId) {
        return paymentMethodDAO.findPaymentMethodsByUserId(userId);
    }

}
