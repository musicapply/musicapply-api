package ru.musicapply.paymentservice;

import com.stripe.Stripe;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import reactor.core.publisher.Hooks;
import ru.musicapply.paymentservice.configuration.IntegrationProperties;
import ru.musicapply.paymentservice.configuration.StripeProperties;


@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		StripeProperties.class,
		IntegrationProperties.class
})
@RequiredArgsConstructor
public class PaymentServiceApplication {

	private final StripeProperties stripeProperties;

	public static void main(String[] args) {
		SpringApplication.run(PaymentServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner() {
		return args -> {
			Stripe.apiKey = stripeProperties.getPrivateKey();
		};
	}

}
