package ru.musicapply.paymentservice.commands;

import lombok.Value;

@Value
public class CreatePaymentMethodCommand {

    String userId;
    String paymentMethodId;
    String stripeId;
    String cardNumber;
    String brand;

}
