package ru.musicapply.paymentservice.configuration;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "stripe")
@Data
public class StripeProperties {

    private String publicKey;
    private String privateKey;
    private String webhookEndpointSecret;

}
