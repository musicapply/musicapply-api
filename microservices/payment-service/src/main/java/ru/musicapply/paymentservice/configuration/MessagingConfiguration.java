package ru.musicapply.paymentservice.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;
import ru.musicapply.api.event.CreatePaymentProfileEvent;
import ru.musicapply.api.event.EventType;
import ru.musicapply.paymentservice.commands.CreatePaymentProfileCommand;
import ru.musicapply.paymentservice.service.PaymentProfileService;
import ru.musicapply.paymentservice.utils.StripeUtils;

import static reactor.function.TupleUtils.function;

import java.util.function.Consumer;


@Configuration
@RequiredArgsConstructor
public class MessagingConfiguration {

    private final PaymentProfileService paymentProfileService;

    @Bean
    public Consumer<Flux<CreatePaymentProfileEvent>> createPaymentProfileEventConsumer() {
        return flux -> flux.filter(e -> e.getType() == EventType.CREATE_PAYMENT_PROFILE)
                           .flatMap(
                                e -> Mono.fromCallable(() ->
                                        Tuples.of(StripeUtils.createCustomer("User (" + e.getUserId() + ")"), e)
                                )
                           )
                           .flatMap(function((stripeCustomer, createPaymentProfileEvent) ->
                                   this.paymentProfileService.createPaymentProfile(
                                           new CreatePaymentProfileCommand(
                                                   createPaymentProfileEvent.getUserId(),
                                                   stripeCustomer.getId()
                                           )
                                   )
                           ))
                           .subscribe();
    }

}
