package ru.musicapply.paymentservice.projections;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("payment_method")
public class PaymentMethodProjection {

    @Id
    private Integer id;

    @Column("payment_method_id")
    private String paymentMethodId;

    @Column("stripe_id")
    private String stripeId;

    @Column("card_number")
    private String cardNumber;

    @Column("card_brand")
    private String cardBrand;

}
