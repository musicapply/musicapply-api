package ru.musicapply.paymentservice.commands;

import lombok.Value;

@Value
public class DeletePaymentMethodCommand {

    String userId;
    String paymentMethodId;

}
