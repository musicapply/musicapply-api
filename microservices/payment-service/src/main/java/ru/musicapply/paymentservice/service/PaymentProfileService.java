package ru.musicapply.paymentservice.service;


import com.stripe.exception.SignatureVerificationException;
import com.stripe.model.Event;
import com.stripe.model.EventDataObjectDeserializer;
import com.stripe.model.StripeObject;
import com.stripe.model.Subscription;
import com.stripe.model.Invoice;
import com.stripe.net.Webhook;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.util.function.Tuples;
import ru.musicapply.api.event.CancelSubscriptionEvent;
import ru.musicapply.api.event.CreateCustomerSubscriptionEvent;
import ru.musicapply.api.event.NewTransactionEvent;
import ru.musicapply.api.paymentservice.dto.CreatePaymentMethodRequest;
import ru.musicapply.api.paymentservice.dto.PaymentMethodResponseDto;
import ru.musicapply.api.paymentservice.dto.PaymentProfileResponseDto;
import ru.musicapply.paymentservice.commands.CreatePaymentMethodCommand;
import ru.musicapply.paymentservice.commands.CreatePaymentProfileCommand;
import ru.musicapply.paymentservice.commands.DeletePaymentMethodCommand;
import ru.musicapply.paymentservice.commands.SetDefaultPaymentMethodCommand;
import ru.musicapply.paymentservice.configuration.StripeProperties;
import ru.musicapply.paymentservice.integration.PaymentServiceIntegration;
import ru.musicapply.paymentservice.mapper.PaymentMethodMapper;
import ru.musicapply.paymentservice.mapper.PaymentProfileMapper;
import ru.musicapply.paymentservice.projections.PaymentProfileAggregateProjection;
import ru.musicapply.paymentservice.repository.PaymentProfileRepository;
import ru.musicapply.paymentservice.utils.StripeUtils;
import ru.musicapply.util.exception.ResourceNotFoundException;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;

import static reactor.function.TupleUtils.function;


@Service
@RequiredArgsConstructor
public class PaymentProfileService {

    private final PaymentProfileAggregateService paymentProfileAggregateService;
    private final PaymentProfileAggregateProjector paymentProfileAggregateProjector;
    private final PaymentProfileRepository paymentProfileRepository;

    private final PaymentMethodMapper paymentMethodMapper;
    private final PaymentProfileMapper paymentProfileMapper;

    private final TransactionalOperator reactiveMongoTransactionalOperator;
    private final TransactionalOperator r2dbcTransactionalOperator;

    private final StripeProperties stripeProperties;

    private final PaymentServiceIntegration integration;
    private final Sinks.Many<CreateCustomerSubscriptionEvent> createSubscriptionSink;
    private final Sinks.Many<NewTransactionEvent> newTransactionSink;
    private final Sinks.Many<CancelSubscriptionEvent> cancelSubscriptionSink;


    public Mono<Void> createPaymentProfile(CreatePaymentProfileCommand command) {
        return paymentProfileAggregateService.handleCreatePaymentProfileCommand(command)
                                             .flatMap(paymentProfileAggregateProjector::projectPaymentProfileCreatedEvent)
                                             .as(reactiveMongoTransactionalOperator::transactional)
                                             .as(r2dbcTransactionalOperator::transactional)
                                             .then(Mono.empty());
    }

    public Mono<Void> createPaymentMethod(CreatePaymentMethodRequest request, String userId) {
        return findByUserId(userId)
                .flatMap(pr -> Mono.fromCallable(
                        () -> Tuples.of(
                                StripeUtils.createPaymentMethod(
                                        request.getCardNumber(),
                                        request.getExpMonth(),
                                        request.getExpYear(),
                                        request.getCvc()
                                ),
                                pr,
                                userId
                        )
                ))
                .flatMap(function( (paymentMethod, projection, uId) -> Mono.fromCallable(
                        () -> Tuples.of(
                                StripeUtils.attachPaymentMethodToCustomer(
                                    paymentMethod.getId(), projection.getStripeId()
                                ),
                                projection,
                                uId
                        ))
                ))
                .flatMap(function( (paymentMethod, projection, uId) -> paymentProfileAggregateService.handleCreatePaymentMethodCommand(
                        new CreatePaymentMethodCommand(
                                uId,
                                UUID.randomUUID().toString(),
                                paymentMethod.getId(),
                                paymentMethod.getCard().getLast4(),
                                paymentMethod.getCard().getBrand()
                        )
                )))
                .flatMap(paymentProfileAggregateProjector::projectPaymentMethodCreatedEvent)
                .as(reactiveMongoTransactionalOperator::transactional)
                .as(r2dbcTransactionalOperator::transactional)
                .then(Mono.empty());
    }

    public Mono<PaymentProfileAggregateProjection> findByStripeId(String stripeId) {
        return paymentProfileRepository.findByStripeId(stripeId)
                                       .switchIfEmpty(Mono.error(new ResourceNotFoundException("PaymentProfile", "stripeId", stripeId)));
    }

    public Mono<PaymentProfileAggregateProjection> findByStripeIdWithPaymentMethods(String stripeId) {
        return paymentProfileRepository.findByStripeIdWithPaymentMethods(stripeId)
                                       .switchIfEmpty(Mono.error(new ResourceNotFoundException("PaymentProfile", "stripeId", stripeId)));
    }

    public Mono<PaymentProfileAggregateProjection> findByUserId(String userId) {
        return paymentProfileRepository.findByUserId(userId)
                                       .switchIfEmpty(Mono.error(new ResourceNotFoundException("PaymentProfile", "userId", userId)));
    }

    public Mono<PaymentProfileAggregateProjection> findByUserIdWithPaymentMethods(String userId) {
        return paymentProfileRepository.findByUserIdWithPaymentMethods(userId)
                                       .switchIfEmpty(Mono.error(new ResourceNotFoundException("PaymentProfile", "userId", userId)));
    }

    public Mono<Void> deletePaymentMethod(String paymentMethodId, String userId) {
        return findByUserId(userId)
                .flatMap(profile -> paymentProfileRepository.findPaymentMethodByPaymentMethodId(paymentMethodId).map(method -> Tuples.of(profile, method)))
                .flatMap(function((profile, method) -> Mono.fromCallable(
                        () -> Tuples.of(
                                StripeUtils.detachPaymentMethodFromCustomer(
                                        method.getStripeId(),
                                        profile.getStripeId()
                                ),
                                profile,
                                method,
                                userId
                        )
                )))
                .flatMap(function( (paymentMethod, profile, method, uId) -> paymentProfileAggregateService.handleDeletePaymentMethodCommand(
                        new DeletePaymentMethodCommand(
                                uId,
                                method.getPaymentMethodId()
                        )
                )))
                .flatMap(paymentProfileAggregateProjector::projectPaymentMethodDeletedEvent)
                .as(reactiveMongoTransactionalOperator::transactional)
                .as(r2dbcTransactionalOperator::transactional)
                .then(Mono.empty());
    }

    public Flux<PaymentMethodResponseDto> findPaymentMethodsByUserId(String userId) {
        return paymentProfileRepository.findPaymentMethodsByUserId(userId)
                                       .map(paymentMethodMapper::toResponse);
    }

    public Mono<Void> setDefaultPaymentMethod(String paymentMethodId, String userId) {
        return findByUserId(userId)
                .flatMap(profile -> paymentProfileRepository.findPaymentMethodByPaymentMethodId(paymentMethodId).map(method -> Tuples.of(profile, method)))
                .flatMap(function((profile, method) -> Mono.fromCallable(
                        () -> Tuples.of(
                                StripeUtils.setDefaultPaymentMethod(
                                        method.getStripeId(),
                                        profile.getStripeId()
                                ),
                                profile,
                                method,
                                userId
                        )
                )))
                .flatMap(function((stripeCustomer, paymentProfileProjection, paymentMethodProjection, uId) -> paymentProfileAggregateService.handleSetDefaultPaymentMethod(
                        new SetDefaultPaymentMethodCommand(
                                uId,
                                paymentMethodProjection.getPaymentMethodId()
                        )
                )))
                .flatMap(paymentProfileAggregateProjector::projectDefaultPaymentMethodSelectedEvent)
                .as(reactiveMongoTransactionalOperator::transactional)
                .as(r2dbcTransactionalOperator::transactional)
                .then(Mono.empty());
    }

    public Mono<Void> createSubscription(String productId, String userId) {
        return findByUserId(userId)
                .flatMap(profile -> integration.getProductById(productId)
                                               .map(productDto -> Tuples.of(profile, productDto))
                )
                .flatMap(function((profile, productDto) -> Mono.fromCallable(
                        () -> Tuples.of(
                                StripeUtils.createSubscription(productDto.getDefaultPriceId(), profile.getStripeId()),
                                profile,
                                productDto
                        )
                )))
                .flatMap(function((stripeSubscription, profile, productDto) -> {
                    return Mono.empty();
                }));
    }

    public Mono<Void> handleStripeEvents(ServerHttpRequest request) {
        String sigHeader = request.getHeaders().get("Stripe-Signature").get(0);
        return DataBufferUtils.join(request.getBody())
                              .map(DataBuffer::asInputStream)
                              .flatMap(is -> Mono.fromCallable(() -> new String(is.readAllBytes(), StandardCharsets.UTF_8)))
                              .map(body -> {

                                  String endpointSecret = stripeProperties.getWebhookEndpointSecret();
                                  Event event = null;

                                  try {
                                      event = Webhook.constructEvent(
                                              body, sigHeader, endpointSecret
                                      );

                                      EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
                                      StripeObject stripeObject = null;
                                      if (dataObjectDeserializer.getObject().isPresent()) {
                                          stripeObject = dataObjectDeserializer.getObject().get();
                                      }


                                      switch (event.getType()) {
                                          case "customer.subscription.created" -> {
                                              Subscription subscription = (Subscription) stripeObject;

                                              return handleCustomerSubscriptionCreatedStripeEvent(subscription)
                                                      .subscribe();

                                          }
                                          case "invoice.payment_succeeded" -> {
                                              Invoice invoice = (Invoice) stripeObject;

                                              return handleInvoicePaymentSucceededStripeEvent(invoice)
                                                      .subscribe();
                                          }
                                          default -> System.out.println("Unhandled event type: " + event.getType());
                                      }

                                  } catch (SignatureVerificationException e) {
                                      System.err.println("Signature failed verification");
                                      return Mono.error(e);
                                  }

                                  return body;
                              })
                              .then(Mono.empty());
    }

    private Mono<Void> handleCustomerSubscriptionCreatedStripeEvent(Subscription subscription) {
        String stripeSubscriptionId = subscription.getId();
        LocalDateTime startSubscriptionDate = Instant.ofEpochMilli(subscription.getStartDate() * 1000).atZone(ZoneId.of("UTC")).toLocalDateTime();
        LocalDateTime nextPaymentDate = Instant.ofEpochMilli(subscription.getCurrentPeriodEnd() * 1000).atZone(ZoneId.of("UTC")).toLocalDateTime();

        String stripeCustomerId = subscription.getCustomer();
        String stripeProductId = subscription.getItems().getData().get(0).getPrice().getProduct();

        return integration.getProductByStripeId(stripeProductId)
                          .flatMap(product -> findByStripeId(stripeCustomerId).flatMap(paymentProfile -> {
                              CreateCustomerSubscriptionEvent event = new CreateCustomerSubscriptionEvent();
                              event.setUserId(paymentProfile.getUserId());
                              event.setProductId(product.getId());
                              event.setStripeSubscriptionId(stripeSubscriptionId);
                              event.setStartSubscriptionDate(startSubscriptionDate);
                              event.setNextPaymentDate(nextPaymentDate);

                              this.createSubscriptionSink.tryEmitNext(event);

                              return Mono.just(paymentProfile);
                          }))
                          .then(Mono.empty());
    }

    private Mono<Void> handleInvoicePaymentSucceededStripeEvent(Invoice invoice) {
        Long amountPaid = invoice.getAmountPaid();
        String stripeCustomerId = invoice.getCustomer();
        String currency = invoice.getCurrency();
        String stripeSubscriptionId = invoice.getSubscription();
        LocalDateTime paymentDate = Instant.ofEpochMilli(invoice.getPeriodEnd() * 1000).atZone(ZoneId.of("UTC")).toLocalDateTime();
        String stripeProductId = invoice.getLines().getData().get(0).getPlan().getProduct();


        return integration.getProductByStripeId(stripeProductId)
                          .flatMap(product -> findByStripeIdWithPaymentMethods(stripeCustomerId)
                                  .map(paymentProfile -> Tuples.of(product, paymentProfile)
                          ))
                          .flatMap(function((product, paymentProfile) ->
                                  Mono.fromCallable(
                                          () -> Tuples.of(
                                                  product,
                                                  paymentProfile,
                                                  StripeUtils.retrieveSubscription(stripeSubscriptionId)
                                          )
                                  )
                          ))
                          .flatMap(function((product, paymentProfile, stripeSubscription) -> {

                              NewTransactionEvent event = new NewTransactionEvent();
                              event.setUserId(paymentProfile.getUserId());
                              event.setAmountPaid(amountPaid);
                              event.setCurrency(currency);
                              event.setPaymentDate(paymentDate);
                              event.setProductId(product.getId());
                              event.setPaymentMethodId(paymentProfile.getDefaultPaymentMethod().getPaymentMethodId());
                              event.setNextPayment(Instant.ofEpochMilli(stripeSubscription.getCurrentPeriodEnd() * 1000).atZone(ZoneId.of("UTC")).toLocalDateTime());

                              this.newTransactionSink.tryEmitNext(event);

                              return Mono.just(event);
                          }))
                          .then(Mono.empty());
    }

    public Mono<Void> cancelSubscription(String userId) {
        return integration.internalGetCustomerByUserId(userId)
                          .flatMap(internalCustomerDto ->
                                  Mono.fromCallable(
                                          () -> Tuples.of(
                                                  internalCustomerDto,
                                                  StripeUtils.cancelSubscription(internalCustomerDto.getStripeSubscriptionId())
                                          )
                                  )
                          )
                          .flatMap(function((internalCustomer, stripeSubscription) -> {
                              CancelSubscriptionEvent event = new CancelSubscriptionEvent();
                              event.setUserId(internalCustomer.getUserId());

                              this.cancelSubscriptionSink.tryEmitNext(event);
                              return Mono.empty();
                          }))
                          .then(Mono.empty());
    }

    public Mono<PaymentProfileResponseDto> findPaymentProfileByUserId(String userId) {
        return findByUserIdWithPaymentMethods(userId).map(paymentProfileMapper::toResponse);
    }
}
