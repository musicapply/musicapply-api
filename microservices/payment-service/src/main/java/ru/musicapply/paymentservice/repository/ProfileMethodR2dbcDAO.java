package ru.musicapply.paymentservice.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.musicapply.paymentservice.projections.ProfileMethod;

@Repository
public interface ProfileMethodR2dbcDAO extends R2dbcRepository<ProfileMethod, Integer> {

    Mono<Void> deleteProfileMethodByPaymentMethodId(Integer paymentMethodId);
    Mono<ProfileMethod> findByPaymentProfileId(Integer paymentProfileId);

}
