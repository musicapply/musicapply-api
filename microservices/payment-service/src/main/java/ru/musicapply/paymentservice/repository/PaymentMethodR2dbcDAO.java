package ru.musicapply.paymentservice.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.paymentservice.projections.PaymentMethodProjection;

@Repository
public interface PaymentMethodR2dbcDAO extends R2dbcRepository<PaymentMethodProjection, Integer> {

    Mono<PaymentMethodProjection> findByPaymentMethodId(String paymentMethodId);

    @Query("select\n" +
            "\tpm.id,\n" +
            "\tpm.payment_method_id,\n" +
            "\tpm.stripe_id,\n" +
            "\tpm.card_number,\n" +
            "\tpm.card_brand\n" +
            "from payment_method pm\n" +
            "where pm.id in (\n" +
            "\tselect link.payment_method_id\n" +
            "\tfrom payment_profile__payment_method link\n" +
            "\twhere link.payment_profile_id = (\n" +
            "\t\tselect p.id\n" +
            "\t\tfrom payment_profile p\n" +
            "\t\twhere p.user_id = :userId\n" +
            "\t)\n" +
            ")")
    Flux<PaymentMethodProjection> findPaymentMethodsByUserId(@Param("userId") String userId);

}
