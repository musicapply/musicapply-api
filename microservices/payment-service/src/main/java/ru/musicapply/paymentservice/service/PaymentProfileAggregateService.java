package ru.musicapply.paymentservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import ru.musicapply.paymentservice.aggregates.PaymentProfileAggregate;
import ru.musicapply.paymentservice.commands.CreatePaymentMethodCommand;
import ru.musicapply.paymentservice.commands.CreatePaymentProfileCommand;
import ru.musicapply.paymentservice.commands.DeletePaymentMethodCommand;
import ru.musicapply.paymentservice.commands.SetDefaultPaymentMethodCommand;
import ru.musicapply.paymentservice.domainevents.DefaultPaymentMethodSelectedEvent;
import ru.musicapply.paymentservice.domainevents.PaymentMethodCreatedEvent;
import ru.musicapply.paymentservice.domainevents.PaymentMethodDeletedEvent;
import ru.musicapply.paymentservice.domainevents.PaymentProfileCreatedEvent;
import ru.musicapply.paymentservice.repository.EventEntity;
import ru.musicapply.paymentservice.repository.EventStore;
import ru.musicapply.util.exception.ResourceNotFoundException;


@Component
@RequiredArgsConstructor
public class PaymentProfileAggregateService {

    private final EventStore writeRepository;

    public Mono<PaymentProfileCreatedEvent> handleCreatePaymentProfileCommand(CreatePaymentProfileCommand command) {
        return Mono.just(PaymentProfileAggregate.builder().build())
                   .flatMap(aggregate -> {
                       PaymentProfileCreatedEvent event = aggregate.handleCreatePaymentProfileCommand(command);
                       Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                                    .streamId(command.getUserId())
                                                                                    .data(event)
                                                                                    .build()
                       );

                       return Mono.zip(mono, Mono.just(event));
                   })
                   .map(Tuple2::getT2);
    }

    public Mono<PaymentMethodCreatedEvent> handleCreatePaymentMethodCommand(CreatePaymentMethodCommand command) {
        return recreatePaymentProfileState(command.getUserId()).flatMap(aggregate -> {
            PaymentMethodCreatedEvent event = aggregate.handleCreatePaymentMethodCommand(command);
            Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                         .streamId(command.getUserId())
                                                                         .data(event)
                                                                         .build()
            );

            return Mono.zip(mono, Mono.just(event));
        })
                .map(Tuple2::getT2);
    }

    public Mono<PaymentMethodDeletedEvent> handleDeletePaymentMethodCommand(DeletePaymentMethodCommand command) {
        return recreatePaymentProfileState(command.getUserId()).flatMap(aggregate -> {
            PaymentMethodDeletedEvent event = aggregate.handleDeletePaymentMethodCommand(command);
            Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                         .streamId(command.getUserId())
                                                                         .data(event)
                                                                         .build()
            );

            return Mono.zip(mono, Mono.just(event));
        })
                .map(Tuple2::getT2);
    }

    public Mono<DefaultPaymentMethodSelectedEvent> handleSetDefaultPaymentMethod(SetDefaultPaymentMethodCommand command) {
        return recreatePaymentProfileState(command.getUserId()).flatMap(aggregate -> {
            DefaultPaymentMethodSelectedEvent event = aggregate.handleSetDefaultPaymentMethodCommand(command);
            Mono<EventEntity> mono = writeRepository.addEvent(EventEntity.builder()
                                                                         .streamId(command.getUserId())
                                                                         .data(event)
                                                                         .build()
            );

            return Mono.zip(mono, Mono.just(event));
        })
                .map(Tuple2::getT2);

    }

    private Mono<PaymentProfileAggregate> recreatePaymentProfileState(String userId) {
        PaymentProfileAggregate aggregate = PaymentProfileAggregate.builder().build();
        return writeRepository.getEvents(userId)
                              .map(event -> {
                                  Object sourceEvent = event.getData();
                                  if (sourceEvent instanceof PaymentProfileCreatedEvent) {
                                      aggregate.apply((PaymentProfileCreatedEvent) sourceEvent);
                                  }
                                  if (sourceEvent instanceof PaymentMethodCreatedEvent) {
                                      aggregate.apply((PaymentMethodCreatedEvent) sourceEvent);
                                  }
                                  if (sourceEvent instanceof PaymentMethodDeletedEvent) {
                                      aggregate.apply((PaymentMethodDeletedEvent) sourceEvent);
                                  }
                                  return event;
                              })
                              .switchIfEmpty(Mono.error(new ResourceNotFoundException("PaymentProfile", "userId", userId)))
                              .then(Mono.just(aggregate));
    }


}
