package ru.musicapply.paymentservice.commands;

import lombok.Value;

@Value
public class SetDefaultPaymentMethodCommand {

    String userId;
    String paymentMethodId;

}
