package ru.musicapply.paymentservice.projections;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("payment_profile__payment_method")
public class ProfileMethod {

    @Id
    private Integer id;

    @Column("payment_profile_id")
    private Integer paymentProfileId;

    @Column("payment_method_id")
    private Integer paymentMethodId;

}
