package ru.musicapply.paymentservice.domainevents;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PaymentProfileCreatedEvent {

    String userId;
    String stripeId;

}
