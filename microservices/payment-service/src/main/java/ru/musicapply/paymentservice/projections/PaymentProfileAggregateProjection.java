package ru.musicapply.paymentservice.projections;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("payment_profile")
public class PaymentProfileAggregateProjection {

    @Id
    private Integer id;

    @Column("user_id")
    private String userId;

    @Column("stripe_id")
    private String stripeId;

    @Column("default_payment_method_id")
    private Integer defaultPaymentMethodId;

    @Transient
    private PaymentMethodProjection defaultPaymentMethod;

    @Transient
    private List<PaymentMethodProjection> paymentMethods = new ArrayList<>();

    public void addPaymentMethodProjection(PaymentMethodProjection paymentMethodProjection) {
        this.paymentMethods.add(paymentMethodProjection);
        if (paymentMethodProjection.getId().equals(defaultPaymentMethodId)) {
            this.defaultPaymentMethod = paymentMethodProjection;
        }
    }

}
