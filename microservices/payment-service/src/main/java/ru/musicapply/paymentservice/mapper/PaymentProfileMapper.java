package ru.musicapply.paymentservice.mapper;

import org.mapstruct.Mapper;
import ru.musicapply.api.paymentservice.dto.PaymentProfileResponseDto;
import ru.musicapply.paymentservice.projections.PaymentProfileAggregateProjection;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        uses = {
                PaymentMethodMapper.class
        }
)
public interface PaymentProfileMapper {

    PaymentProfileResponseDto toResponse(PaymentProfileAggregateProjection projection);
}
