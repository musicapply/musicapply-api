package ru.musicapply.paymentservice.domainevents;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PaymentMethodDeletedEvent {

    String userId;
    String paymentMethodId;

}
