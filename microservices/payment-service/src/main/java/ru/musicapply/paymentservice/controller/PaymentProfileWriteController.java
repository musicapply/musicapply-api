package ru.musicapply.paymentservice.controller;

import com.stripe.exception.SignatureVerificationException;
import com.stripe.model.*;
import com.stripe.net.Webhook;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Mono;
import ru.musicapply.api.paymentservice.api.PaymentWriteApi;
import ru.musicapply.api.paymentservice.dto.CreatePaymentMethodRequest;
import ru.musicapply.paymentservice.security.UserPrincipal;
import ru.musicapply.paymentservice.service.PaymentProfileService;


import java.nio.charset.StandardCharsets;
import java.util.Date;


@RestController
@RequestMapping("/payment_profile")
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "app", name = "profile", havingValue = "write", matchIfMissing = true)
public class PaymentProfileWriteController implements PaymentWriteApi {

    private final PaymentProfileService paymentProfileService;


    @Override
    public Mono<Void> addPaymentMethod(CreatePaymentMethodRequest request) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return paymentProfileService.createPaymentMethod(request, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> setDefaultPaymentMethod(String paymentMethodId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return paymentProfileService.setDefaultPaymentMethod(paymentMethodId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> removePaymentMethod(String paymentMethodId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return paymentProfileService.deletePaymentMethod(paymentMethodId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> createSubscription(String productId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return paymentProfileService.createSubscription(productId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> cancelSubscription() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return paymentProfileService.cancelSubscription(userPrincipal.getUserId());
                                            });
    }

    @PostMapping("/stripe/events")
    public Mono<Void> stripeWebhook(ServerHttpRequest request) {
        return paymentProfileService.handleStripeEvents(request);
    }

}
