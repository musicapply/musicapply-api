package ru.musicapply.paymentservice.domainevents;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class DefaultPaymentMethodSelectedEvent {

    String userId;
    String paymentMethodId;

}
