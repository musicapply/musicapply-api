package ru.musicapply.paymentservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.musicapply.paymentservice.domainevents.DefaultPaymentMethodSelectedEvent;
import ru.musicapply.paymentservice.domainevents.PaymentMethodCreatedEvent;
import ru.musicapply.paymentservice.domainevents.PaymentMethodDeletedEvent;
import ru.musicapply.paymentservice.domainevents.PaymentProfileCreatedEvent;
import ru.musicapply.paymentservice.projections.PaymentMethodProjection;
import ru.musicapply.paymentservice.projections.PaymentProfileAggregateProjection;
import ru.musicapply.paymentservice.repository.PaymentProfileRepository;


@Component
@RequiredArgsConstructor
public class PaymentProfileAggregateProjector {

    private final PaymentProfileRepository paymentProfileRepository;

    public Mono<Void> projectPaymentProfileCreatedEvent(PaymentProfileCreatedEvent event) {
        return Mono.just(event)
                   .map(e -> PaymentProfileAggregateProjection.builder()
                                                              .userId(event.getUserId())
                                                              .stripeId(event.getStripeId())
                                                              .build()
                   )
                  .flatMap(paymentProfileRepository::save)
                  .then(Mono.empty());
    }

    public Mono<Void> projectPaymentMethodCreatedEvent(PaymentMethodCreatedEvent event) {
        return Mono.just(event)
                   .map(e -> PaymentMethodProjection.builder()
                                                    .paymentMethodId(event.getPaymentMethodId())
                                                    .stripeId(event.getStripeId())
                                                    .cardNumber(event.getCardNumber())
                                                    .cardBrand(event.getBrand())
                                                    .build())
                   .flatMap(pr -> paymentProfileRepository.addPaymentMethodToProfile(pr, event.getUserId()))
                   .then(Mono.empty());
    }

    public Mono<Void> projectPaymentMethodDeletedEvent(PaymentMethodDeletedEvent event) {
        return Mono.just(event)
                   .flatMap(e -> paymentProfileRepository.findByUserId(event.getUserId())
                                                         .flatMap(profile -> paymentProfileRepository.findPaymentMethodByPaymentMethodId(event.getPaymentMethodId())
                                                                                                     .flatMap(method -> {
                                                                                                         if (profile.getDefaultPaymentMethodId().equals(method.getId())) {
                                                                                                             profile.setDefaultPaymentMethodId(null);
                                                                                                             return paymentProfileRepository.update(profile)
                                                                                                                                            .then(paymentProfileRepository.removePaymentMethodFromProfile(event.getPaymentMethodId(), event.getUserId()));
                                                                                                         } else {
                                                                                                             return paymentProfileRepository.removePaymentMethodFromProfile(event.getPaymentMethodId(), event.getUserId());
                                                                                                         }
                                                                                                     })
                                                         )
                   )
                   //.flatMap(e -> paymentProfileRepository.removePaymentMethodFromProfile(event.getPaymentMethodId(), event.getUserId()))
                   .then(Mono.empty());
    }

    public Mono<Void> projectDefaultPaymentMethodSelectedEvent(DefaultPaymentMethodSelectedEvent event) {
        return Mono.just(event)
                   .flatMap(e -> paymentProfileRepository.findByUserId(event.getUserId())
                                                         .flatMap(profile -> paymentProfileRepository.findPaymentMethodByPaymentMethodId(event.getPaymentMethodId())
                                                                                                     .flatMap(method -> {
                                                                                                         profile.setDefaultPaymentMethodId(method.getId());
                                                                                                         return paymentProfileRepository.update(profile);
                                                                                                     }
                                                         )
                   ))
                                                         .then(Mono.empty());
    }

}
