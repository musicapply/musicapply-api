package ru.musicapply.paymentservice.utils;

import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.PaymentMethod;
import com.stripe.model.Subscription;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StripeUtils {

    public static Customer createCustomer(String description) throws StripeException {
        Map<String, Object> params = new HashMap<>();
        params.put("description", description);

        return Customer.create(params);
    }

    public static PaymentMethod createPaymentMethod(String cardNumber, Integer expMonth, Integer expYear, String cvc) throws StripeException {
        Map<String, Object> card = new HashMap<>();
        card.put("number", cardNumber);
        card.put("exp_month", expMonth);
        card.put("exp_year", expYear);
        card.put("cvc", cvc);

        Map<String, Object> params = new HashMap<>();
        params.put("type", "card");
        params.put("card", card);

        return PaymentMethod.create(params);
    }

    public static PaymentMethod attachPaymentMethodToCustomer(String paymentMethodId, String customerId) throws StripeException {
        PaymentMethod paymentMethod = PaymentMethod.retrieve(paymentMethodId);

        Map<String, Object> params = new HashMap<>();
        params.put("customer", customerId);


        return paymentMethod.attach(params);
    }

    public static PaymentMethod detachPaymentMethodFromCustomer(String paymentMethodId, String customerId) throws StripeException {
        PaymentMethod paymentMethod = PaymentMethod.retrieve(paymentMethodId);
        return paymentMethod.detach();
    }

    public static Customer setDefaultPaymentMethod(String paymentMethodId, String customerId) throws StripeException {
        Customer customer = Customer.retrieve(customerId);

        Map<String, Object> invoiceSettings = new HashMap<>();
        invoiceSettings.put("default_payment_method", paymentMethodId);

        Map<String, Object> params = new HashMap<>();
        params.put("invoice_settings", invoiceSettings);

        return customer.update(params);

    }

    public static Subscription createSubscription(String priceId, String customerId) throws StripeException {
        List<Object> items = new ArrayList<>();

        Map<String, Object> item1 = new HashMap<>();
        item1.put("price", priceId);

        items.add(item1);

        Map<String, Object> params = new HashMap<>();
        params.put("customer", customerId);
        params.put("items", items);

        return Subscription.create(params);
    }

    public static Subscription retrieveSubscription(String subscriptionId) throws StripeException {
        return Subscription.retrieve(subscriptionId);
    }

    public static Subscription cancelSubscription(String stripeSubscriptionId) throws StripeException {
        Subscription subscription = Subscription.retrieve(stripeSubscriptionId);
        return subscription.cancel();
    }
}
