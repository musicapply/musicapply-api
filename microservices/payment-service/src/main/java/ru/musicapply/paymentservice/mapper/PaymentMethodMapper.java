package ru.musicapply.paymentservice.mapper;

import org.mapstruct.Mapper;
import ru.musicapply.api.paymentservice.dto.PaymentMethodResponseDto;
import ru.musicapply.paymentservice.projections.PaymentMethodProjection;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE
)
public interface PaymentMethodMapper {

    PaymentMethodResponseDto toResponse(PaymentMethodProjection projection);
    List<PaymentMethodResponseDto> toListResponses(List<PaymentMethodProjection> projections);

}
