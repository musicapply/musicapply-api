package ru.musicapply.paymentservice.aggregates;

import ru.musicapply.microarchitecture.Aggregate;
import ru.musicapply.microarchitecture.AggregateBuilder;
import ru.musicapply.paymentservice.commands.CreatePaymentMethodCommand;
import ru.musicapply.paymentservice.commands.CreatePaymentProfileCommand;
import ru.musicapply.paymentservice.commands.DeletePaymentMethodCommand;
import ru.musicapply.paymentservice.commands.SetDefaultPaymentMethodCommand;
import ru.musicapply.paymentservice.domainevents.DefaultPaymentMethodSelectedEvent;
import ru.musicapply.paymentservice.domainevents.PaymentMethodCreatedEvent;
import ru.musicapply.paymentservice.domainevents.PaymentMethodDeletedEvent;
import ru.musicapply.paymentservice.domainevents.PaymentProfileCreatedEvent;
import ru.musicapply.sharedmodel.PaymentMethodId;
import ru.musicapply.sharedmodel.UserId;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Aggregate
public class PaymentProfileAggregate {

    private UserId userId;
    private String stripeId;
    private PaymentMethod defaultPaymentMethod;
    private List<PaymentMethod> paymentMethods = new ArrayList<>();

    private PaymentProfileAggregate(PaymentProfileAggregateBuilder builder) {
        this.userId = new UserId(builder.userId);
        this.stripeId = builder.stripeId;
        this.defaultPaymentMethod = builder.defaultPaymentMethod;
        this.paymentMethods = builder.paymentMethods;
    }

    public PaymentProfileCreatedEvent handleCreatePaymentProfileCommand(CreatePaymentProfileCommand command) {
        return PaymentProfileCreatedEvent.builder()
                                         .userId(command.getUserId())
                                         .stripeId(command.getStripeId())
                                         .build();
    }

    public PaymentMethodCreatedEvent handleCreatePaymentMethodCommand(CreatePaymentMethodCommand command) {
        return PaymentMethodCreatedEvent.builder()
                                        .userId(command.getUserId())
                                        .paymentMethodId(command.getPaymentMethodId())
                                        .stripeId(command.getStripeId())
                                        .cardNumber(command.getCardNumber())
                                        .brand(command.getBrand())
                                        .build();
    }

    public PaymentMethodDeletedEvent handleDeletePaymentMethodCommand(DeletePaymentMethodCommand command) {
        return PaymentMethodDeletedEvent.builder()
                                        .userId(command.getUserId())
                                        .paymentMethodId(command.getPaymentMethodId())
                                        .build();
    }

    public DefaultPaymentMethodSelectedEvent handleSetDefaultPaymentMethodCommand(SetDefaultPaymentMethodCommand command) {
        return DefaultPaymentMethodSelectedEvent.builder()
                                                .userId(command.getUserId())
                                                .paymentMethodId(command.getPaymentMethodId())
                                                .build();
    }

    public void apply(PaymentProfileCreatedEvent event) {
        this.userId = new UserId(event.getUserId());
        this.stripeId = event.getStripeId();
    }

    public void apply(PaymentMethodCreatedEvent event) {
        PaymentMethod paymentMethod = new PaymentMethod(
                new PaymentMethodId(event.getPaymentMethodId()),
                event.getStripeId(),
                event.getCardNumber(),
                event.getBrand()
        );
    }


    public void apply(PaymentMethodDeletedEvent event) {
        String paymentMethodId = event.getPaymentMethodId();
        Optional<PaymentMethod> paymentMethodOptional = paymentMethods.stream()
                                                                      .filter(pm -> paymentMethodId.equals(
                                                                              pm.paymentMethodId.getPaymentMethodId()
                                                                      ))
                                                                      .findFirst();
        if (paymentMethodOptional.isPresent()) {
            PaymentMethod paymentMethod = paymentMethodOptional.get();

            paymentMethods.remove(paymentMethod);
            if (paymentMethod.paymentMethodId.getPaymentMethodId().equals(defaultPaymentMethod.paymentMethodId.getPaymentMethodId())) {
                this.defaultPaymentMethod = null;
            }
        }
    }

    public void apply(DefaultPaymentMethodSelectedEvent event) {
        String paymentMethodId = event.getPaymentMethodId();
        Optional<PaymentMethod> paymentMethodOptional = paymentMethods.stream()
                                                                      .filter(pm -> paymentMethodId.equals(
                                                                              pm.paymentMethodId.getPaymentMethodId()
                                                                      ))
                                                                      .findFirst();
        paymentMethodOptional.ifPresent(paymentMethod -> this.defaultPaymentMethod = paymentMethod);
    }

    public static PaymentProfileAggregate.PaymentProfileAggregateBuilder builder() {
        return new PaymentProfileAggregateBuilder();
    }

    @AggregateBuilder
    public static class PaymentProfileAggregateBuilder {

        private String userId;
        private String stripeId;
        private PaymentMethod defaultPaymentMethod;
        private List<PaymentMethod> paymentMethods;

        private PaymentProfileAggregateBuilder() {
            paymentMethods = new ArrayList<>();
        }

        public PaymentProfileAggregateBuilder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public PaymentProfileAggregateBuilder stripeId(String stripeId) {
            this.stripeId = stripeId;
            return this;
        }

        public PaymentProfileAggregateBuilder defaultPaymentMethod(String paymentMethodId, String stripeId, String cardNumber, String cardBrand) {
            this.defaultPaymentMethod = new PaymentMethod(new PaymentMethodId(paymentMethodId), stripeId, cardNumber, cardBrand);
            return this;
        }

        public PaymentProfileAggregateBuilder cardNumber(String paymentMethodId, String stripeId, String cardNumber, String cardBrand) {
            this.paymentMethods.add(new PaymentMethod(new PaymentMethodId(paymentMethodId), stripeId, cardNumber, cardBrand));
            return this;
        }

        public PaymentProfileAggregate build() {
            return new PaymentProfileAggregate(this);
        }

    }

}
