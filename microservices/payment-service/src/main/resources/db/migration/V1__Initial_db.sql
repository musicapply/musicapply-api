CREATE TABLE payment_profile(
    id                          SERIAL          NOT NULL,
    user_id                     VARCHAR(50)     NOT NULL,
    stripe_id                   VARCHAR(50)     NOT NULL,
    default_payment_method_id   INTEGER
);


CREATE TABLE payment_method(
    id                      SERIAL          NOT NULL,
    payment_method_id       VARCHAR(50),
    stripe_id               VARCHAR(50),
    card_number             VARCHAR(25),
    card_brand              VARCHAR(25)
);

CREATE TABLE payment_profile__payment_method(
    id                      SERIAL      NOT NULL,
    payment_profile_id      INTEGER     NOT NULL,
    payment_method_id       INTEGER     NOT NULL
);

ALTER TABLE IF EXISTS payment_profile
    ADD CONSTRAINT PK_payment_profile__id
        PRIMARY KEY (id);

ALTER TABLE IF EXISTS payment_profile
    ADD CONSTRAINT U_payment_profile__user_id
        UNIQUE (user_id);

ALTER TABLE IF EXISTS payment_profile
    ADD CONSTRAINT U_payment_profile__id_user_id
        UNIQUE (id, user_id);

ALTER TABLE IF EXISTS payment_method
    ADD CONSTRAINT PK_payment_method__id
        PRIMARY KEY (id);

ALTER TABLE IF EXISTS payment_profile__payment_method
    ADD CONSTRAINT PK_payment_profile__payment_method__id
        PRIMARY KEY (id);

ALTER TABLE IF EXISTS payment_profile__payment_method
    ADD CONSTRAINT FK_payment_profile__payment_method__payment_profile_id
        FOREIGN KEY (payment_profile_id) REFERENCES payment_profile(id);

ALTER TABLE IF EXISTS payment_profile__payment_method
    ADD CONSTRAINT  FK_payment_profile__payment_method__payment_method_id
        FOREIGN KEY (payment_method_id) REFERENCES payment_method(id);