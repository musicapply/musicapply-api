package ru.musicapply.collectionfavoriteservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.favorite.dto.TrackRecordDto;
import ru.musicapply.collectionfavoriteservice.domain.TrackRecord;
import ru.musicapply.collectionfavoriteservice.integration.CollectionFavoriteServiceIntegration;
import ru.musicapply.collectionfavoriteservice.repository.TrackRecordRepository;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CollectionFavoriteService {

    private final TrackRecordRepository trackRecordRepository;
    private final CollectionFavoriteServiceIntegration integration;

    public Mono<Void> addTrackInFavorite(String trackId, String userId) {
        return trackRecordRepository.findByTrackIdAndUserId(trackId, userId)
                                    .switchIfEmpty(integration.getTrackByIdFull(trackId)
                                                              .flatMap(track -> {
                                                                  TrackRecord record = TrackRecord.builder()
                                                                                                  .trackId(trackId)
                                                                                                  .userId(userId)
                                                                                                  .creationDate(LocalDateTime.now())
                                                                                                  .build();

                                                                  return trackRecordRepository.save(record);
                                                              })
                                    )
                                    .then();
    }

    public Mono<Void> removeTrackFromFavorite(String trackId, String userId) {
        return trackRecordRepository.findByTrackIdAndUserId(trackId, userId)
                                    .flatMap(trackRecordRepository::delete);
    }

    public Flux<TrackRecordDto> getAllTracksByUserId(String userId) {
        return trackRecordRepository.findByUserIdOrderByCreationDateDesc(userId)
                                    .flatMapSequential(trackRecord -> integration.getTrackByIdFull(trackRecord.getTrackId())
                                                                       .map(track -> {
                                                                           TrackRecordDto dto = TrackRecordDto.builder()
                                                                                                              .id(trackRecord.getId().toString())
                                                                                                              .creationDate(trackRecord.getCreationDate())
                                                                                                              .track(track)
                                                                                                              .build();

                                                                           return dto;
                                                                       })
                                    );
    }
}
