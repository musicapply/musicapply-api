package ru.musicapply.collectionfavoriteservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.favorite.api.CollectionFavoriteServiceApi;
import ru.musicapply.api.collection.favorite.dto.TrackRecordDto;
import ru.musicapply.collectionfavoriteservice.security.UserPrincipal;
import ru.musicapply.collectionfavoriteservice.service.CollectionFavoriteService;

@RestController
@RequestMapping("/collection/favorite")
@RequiredArgsConstructor
public class FavoriteController implements CollectionFavoriteServiceApi {

    private final CollectionFavoriteService service;


    @Override
    public Mono<Void> addTrackInFavorite(String trackId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.addTrackInFavorite(trackId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> removeTrackFromFavorite(String trackId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.removeTrackFromFavorite(trackId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Flux<TrackRecordDto> getAllFavoriteTracks() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMapMany(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.getAllTracksByUserId(userPrincipal.getUserId());
                                            });
    }

}
