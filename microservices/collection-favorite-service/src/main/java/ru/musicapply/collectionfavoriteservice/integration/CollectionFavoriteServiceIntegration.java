package ru.musicapply.collectionfavoriteservice.integration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.trackservice.api.TrackServiceApi;
import ru.musicapply.api.trackservice.dto.TrackResponseDtoShort;
import ru.musicapply.api.trackservice.dto.TrackResponseFullDto;
import ru.musicapply.collectionfavoriteservice.configuration.IntegrationProperties;

import java.net.URI;

@Component
@Slf4j
public class CollectionFavoriteServiceIntegration implements TrackServiceApi {

    private final String TRACK_SERVICE_URL;

    private final WebClient webClient;


    @Autowired
    public CollectionFavoriteServiceIntegration(
            WebClient.Builder webClientBuilder,
            IntegrationProperties integrationProperties
    ) {
        this.webClient = WebClient.builder().build();
        this.TRACK_SERVICE_URL = "http://" + integrationProperties.getTrackService();
    }


    @Override
    public Mono<TrackResponseDtoShort> getTrackByIdShort(String trackId) {
        throw new NotImplementedException("getTrackByIdShort not allowed here");
    }

    @Override
    public Mono<TrackResponseFullDto> getTrackByIdFull(String trackId) {

        URI url = UriComponentsBuilder.fromUriString(TRACK_SERVICE_URL
                + "/track/{trackId}").build(trackId);

        log.debug("Will call the getTrackByIdFull API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(TrackResponseFullDto.class);
    }

    @Override
    public Flux<TrackResponseFullDto> getTracksByAlbumId(String albumId) {
        throw new NotImplementedException("getTracksByAlbumId not allowed here");
    }

    @Override
    public Flux<TrackResponseFullDto> searchTracksLikeName(String name) {
        throw new NotImplementedException("searchTracksLikeName not allowed here");
    }
}
