package ru.musicapply.collectionfavoriteservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.collectionfavoriteservice.domain.TrackRecord;


@Repository
public interface TrackRecordRepository extends ReactiveMongoRepository<TrackRecord, ObjectId> {

    Mono<TrackRecord> findByTrackIdAndUserId(String trackId, String userId);
    Flux<TrackRecord> findByUserIdOrderByCreationDateDesc(String userId);

}
