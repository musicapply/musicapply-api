package ru.musicapply.collectionfavoriteservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import ru.musicapply.collectionfavoriteservice.configuration.IntegrationProperties;


@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		IntegrationProperties.class
})
@EnableReactiveMongoRepositories
public class CollectionFavoriteServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollectionFavoriteServiceApplication.class, args);
	}

}
