package ru.musicapply.album.integration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.album.configuration.IntegrationProperties;
import ru.musicapply.api.albumservice.api.AlbumServiceApi;
import ru.musicapply.api.albumservice.dto.AlbumResponseFullDto;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;
import ru.musicapply.api.artistservice.api.ArtistServiceApi;
import ru.musicapply.api.artistservice.dto.ArtistResponseDto;
import ru.musicapply.api.artistservice.dto.ArtistResponseShortDto;
import ru.musicapply.api.trackservice.api.TrackServiceApi;
import ru.musicapply.api.trackservice.dto.TrackResponseDtoShort;
import ru.musicapply.api.trackservice.dto.TrackResponseFullDto;

import java.net.URI;

@Component
@Slf4j
public class AlbumServiceIntegration implements TrackServiceApi, ArtistServiceApi {

    private final String TRACK_SERVICE_URL;
    private final String ARTIST_SERVICE_URL;

    private final WebClient webClient;

    @Autowired
    public AlbumServiceIntegration(
            WebClient.Builder webClientBuilder,
            IntegrationProperties integrationProperties
    ) {
        this.webClient = WebClient.builder().build();
        this.TRACK_SERVICE_URL = "http://" + integrationProperties.getTrackService();
        this.ARTIST_SERVICE_URL = "http://" + integrationProperties.getArtistService();
    }

    @Override
    public Mono<TrackResponseDtoShort> getTrackByIdShort(String trackId) {
        URI url = UriComponentsBuilder.fromUriString(TRACK_SERVICE_URL
                + "/track/short/{trackId}").build(trackId);

        log.debug("Will call the getTrackByIdShort API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(TrackResponseDtoShort.class);
    }

    @Override
    public Mono<TrackResponseFullDto> getTrackByIdFull(String trackId) {
        URI url = UriComponentsBuilder.fromUriString(TRACK_SERVICE_URL
                + "/track/{trackId}").build(trackId);

        log.debug("Will call the getTrackByIdFull API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(TrackResponseFullDto.class);
    }

    @Override
    public Flux<TrackResponseFullDto> getTracksByAlbumId(String albumId) {

        URI url = UriComponentsBuilder.fromUriString(TRACK_SERVICE_URL
                + "/track/findByAlbumId/{albumId}").build(albumId);

        log.debug("Will call the getTracksByAlbumId API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToFlux(TrackResponseFullDto.class);
    }

    @Override
    public Flux<TrackResponseFullDto> searchTracksLikeName(String name) {
        throw new NotImplementedException("searchTracksLikeName not allowed here");
    }

    @Override
    public Mono<ArtistResponseShortDto> getArtistByArtistIdShort(String artistId) {
        URI url = UriComponentsBuilder.fromUriString(ARTIST_SERVICE_URL
                + "/artist/short/{artistId}").build(artistId);

        log.debug("Will call the getArtistByArtistIdShort API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(ArtistResponseShortDto.class);
    }

    @Override
    public Mono<ArtistResponseDto> getArtistByArtistIdFull(String artistId) {
        throw new NotImplementedException("getArtistByArtistIdFull not allowed here");
    }

    @Override
    public Flux<ArtistResponseDto> searchArtistsLikeName(String name) {
        throw new NotImplementedException("searchArtistsLikeName not allowed here");
    }
}
