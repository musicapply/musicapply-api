package ru.musicapply.album.mapper;

import org.mapstruct.Mapper;
import ru.musicapply.album.domain.Album;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE
)
public interface AlbumMapper {

    AlbumResponseShortDto toShortDto(Album album);

}
