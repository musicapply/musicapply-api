package ru.musicapply.album.configuration;

import io.minio.MinioClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MinioConfiguration {

    @Bean
    public MinioClient minioClient(MinioProperties minioProperties) {
        return MinioClient.builder()
                           .endpoint(minioProperties.getServerHost(), minioProperties.getServerPort(), false)
                           .credentials(minioProperties.getUsername(), minioProperties.getPassword())
                           .build();
    }

}
