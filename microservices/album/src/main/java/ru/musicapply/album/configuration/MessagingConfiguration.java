package ru.musicapply.album.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.musicapply.album.service.AlbumService;
import ru.musicapply.api.event.EventType;
import ru.musicapply.api.event.album.CreateAlbumEvent;

import java.util.function.Consumer;


@Configuration
@RequiredArgsConstructor
public class MessagingConfiguration {

    private final AlbumService albumService;

    @Bean
    public Consumer<Flux<CreateAlbumEvent>> createAlbumConsumer() {
        return flux -> flux.filter(e -> e.getType() == EventType.CREATE_ALBUM)
                           .publishOn(Schedulers.boundedElastic())
                           .flatMap(this.albumService::createAlbum)
                           .subscribeOn(Schedulers.single())
                           .subscribe();
    }

}
