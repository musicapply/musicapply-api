package ru.musicapply.album.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "albums")
public class Album {

    @Id
    private ObjectId id;
    private String albumId;
    private String artistId;
    private String name;
    private String description;
    private LocalDateTime releaseDate;
    private String imageUrl;

}
