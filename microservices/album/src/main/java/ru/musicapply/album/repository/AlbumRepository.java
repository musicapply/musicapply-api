package ru.musicapply.album.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.album.domain.Album;

@Repository
public interface AlbumRepository extends ReactiveMongoRepository<Album, ObjectId> {

    Mono<Album> findByAlbumId(String albumId);
    Flux<Album> findByArtistId(String artistId);

    @Query(value = "{'name': {$regex : ?0, $options: 'i'}}")
    Flux<Album> findByNameLike(String name);

}
