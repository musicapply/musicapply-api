package ru.musicapply.album.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.album.service.AlbumService;
import ru.musicapply.api.albumservice.api.AlbumServiceApi;
import ru.musicapply.api.albumservice.dto.AlbumResponseFullDto;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;


@RestController
@RequestMapping("/album")
@RequiredArgsConstructor
public class AlbumController implements AlbumServiceApi {

    private final AlbumService albumService;

    @Override
    public Mono<AlbumResponseShortDto> getAlbumByIdShort(String albumId) {
        return albumService.getShortAlbumByAlbumId(albumId);
    }

    @Override
    public Mono<AlbumResponseFullDto> getAlbumByAlbumId(String albumId) {
        return albumService.getAlbumByAlbumId(albumId);
    }

    @Override
    public Flux<AlbumResponseShortDto> getAlbumsByArtistIdShort(String artistId) {
        return albumService.getShortAlbumsByArtistId(artistId);
    }

    @Override
    public Flux<AlbumResponseFullDto> searchAlbumsLikeName(String name) {
        return albumService.findFullAlbumsLikeName(name);
    }
}
