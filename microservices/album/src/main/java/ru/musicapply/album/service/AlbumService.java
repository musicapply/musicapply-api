package ru.musicapply.album.service;


import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import ru.musicapply.album.configuration.MinioProperties;
import ru.musicapply.album.domain.Album;
import ru.musicapply.album.integration.AlbumServiceIntegration;
import ru.musicapply.album.mapper.AlbumMapper;
import ru.musicapply.album.repository.AlbumRepository;
import ru.musicapply.api.albumservice.dto.AlbumResponseFullDto;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;
import ru.musicapply.api.event.album.CreateAlbumEvent;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class AlbumService {

    private final Path basePath = Paths.get("/tmp/upload/");
    private final AlbumServiceIntegration integration;

    private final MinioClient minioClient;
    private final MinioProperties minioProperties;
    private final AlbumRepository repository;

    private final AlbumMapper mapper;


    public Mono<Void> createAlbum(CreateAlbumEvent event) {
        Album album = Album.builder()
                           .artistId(event.getArtistId())
                           .albumId(UUID.randomUUID().toString())
                           .name(event.getName())
                           .description(event.getDescription())
                           .releaseDate(event.getReleaseDate())
                           .build();

        String imageId = UUID.randomUUID().toString();
        String fileName = imageId + ".png";

        return Mono.fromCallable(() -> Files.write(basePath.resolve(fileName), event.getImage()))
                   .publishOn(Schedulers.boundedElastic())
                   .flatMap(p -> Mono.fromCallable(() -> minioClient.uploadObject(UploadObjectArgs.builder()
                                                                                                  .bucket(minioProperties.getAlbumBucketName())
                                                                                                  .object(fileName)
                                                                                                  .filename("/tmp/upload/" + fileName)
                                                                                                  .build())
                   ))
                   .flatMap(owr -> {
                       album.setImageUrl("https://api.musicapply.ru/" + minioProperties.getAlbumBucketName() + "/" + fileName);
                       return repository.save(album);
                   })
                   .then();
    }

    public Mono<AlbumResponseShortDto> getShortAlbumByAlbumId(String albumId) {
        return repository.findByAlbumId(albumId)
                         .map(mapper::toShortDto);
    }

    public Mono<AlbumResponseFullDto> getAlbumByAlbumId(String albumId) {
        return repository.findByAlbumId(albumId)
                         .flatMap(album -> integration.getArtistByArtistIdShort(album.getArtistId())
                                                      .map(artist -> {
                                                          return AlbumResponseFullDto.builder()
                                                                                     .albumId(album.getAlbumId())
                                                                                     .name(album.getName())
                                                                                     .artist(artist)
                                                                                     .description(album.getDescription())
                                                                                     .releaseDate(album.getReleaseDate())
                                                                                     .imageUrl(album.getImageUrl())
                                                                                     .build();
                                                      })
                         ).flatMap(dto -> integration.getTracksByAlbumId(dto.getAlbumId())
                                                     .doOnNext(track -> dto.getTracks()
                                                                           .add(track)
                                                     )
                                                     .then(Mono.just(dto))
                        );
    }

    public Flux<AlbumResponseShortDto> getShortAlbumsByArtistId(String artistId) {
        return repository.findByArtistId(artistId)
                         .map(mapper::toShortDto);
    }

    public Flux<AlbumResponseFullDto> findFullAlbumsLikeName(String name) {
        return repository.findByNameLike(name)
                         .flatMap(album -> integration.getArtistByArtistIdShort(album.getArtistId())
                                                      .map(artist -> {
                                                          return AlbumResponseFullDto.builder()
                                                                                     .albumId(album.getAlbumId())
                                                                                     .name(album.getName())
                                                                                     .artist(artist)
                                                                                     .description(album.getDescription())
                                                                                     .releaseDate(album.getReleaseDate())
                                                                                     .imageUrl(album.getImageUrl())
                                                                                     .build();
                                                      })
                         ).flatMap(dto -> integration.getTracksByAlbumId(dto.getAlbumId())
                                                     .doOnNext(track -> dto.getTracks()
                                                                           .add(track)
                                                     )
                                                     .then(Mono.just(dto))
                );
    }
}
