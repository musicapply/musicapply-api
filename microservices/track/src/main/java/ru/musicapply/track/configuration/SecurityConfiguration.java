package ru.musicapply.track.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter;
import reactor.core.publisher.Mono;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final ReactiveAuthenticationManager jwtAuthenticationManager;
    private final ServerAuthenticationConverter jwtAuthenticationConverter;


    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        AuthenticationWebFilter authenticationWebFilter = new AuthenticationWebFilter(jwtAuthenticationManager);
        authenticationWebFilter.setServerAuthenticationConverter(jwtAuthenticationConverter);


        return http.authorizeExchange(authorizeRequests ->
                authorizeRequests
                        .anyExchange().permitAll()
        )
                   .formLogin(ServerHttpSecurity.FormLoginSpec::disable)
                   .csrf(ServerHttpSecurity.CsrfSpec::disable)
                   .cors(ServerHttpSecurity.CorsSpec::disable)
                   .logout(ServerHttpSecurity.LogoutSpec::disable)
                   .addFilterAt(authenticationWebFilter, SecurityWebFiltersOrder.AUTHENTICATION)
                   .exceptionHandling(exceptionHandlingSpec ->
                           exceptionHandlingSpec.authenticationEntryPoint(((swe, e) ->
                                   Mono.fromRunnable(() -> swe.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED)))))
                   .build();
    }

}
