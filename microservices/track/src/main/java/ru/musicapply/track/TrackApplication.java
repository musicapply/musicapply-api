package ru.musicapply.track;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.musicapply.track.configuration.IntegrationProperties;
import ru.musicapply.track.configuration.MinioProperties;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		IntegrationProperties.class,
		MinioProperties.class
})
@RequiredArgsConstructor
public class TrackApplication {

	private final Path uploadPath = Paths.get("/tmp/upload/");
	private final MinioClient minioClient;
	private final MinioProperties minioProperties;

	public static void main(String[] args) {
		SpringApplication.run(TrackApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner() {
		return args -> {
			BucketExistsArgs bucketExistsArgs = BucketExistsArgs.builder().bucket(minioProperties.getTrackBucketName()).build();
			boolean isExists = minioClient.bucketExists(bucketExistsArgs);

			if (!isExists) {
				MakeBucketArgs makeBucketArgs = MakeBucketArgs.builder().bucket(minioProperties.getTrackBucketName()).build();
				minioClient.makeBucket(makeBucketArgs);
			}

			File uploadDirectory = new File(uploadPath.toString());
			if (!uploadDirectory.exists()) {
				uploadDirectory.mkdirs();
			}

		};
	}



}
