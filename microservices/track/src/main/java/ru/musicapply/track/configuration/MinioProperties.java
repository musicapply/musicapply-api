package ru.musicapply.track.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "minio")
@Data
public class MinioProperties {

    private String username;
    private String password;
    private String serverHost;
    private Integer serverPort;
    private String artistBucketName;
    private String playlistBucketName;
    private String trackBucketName;
    private String userBucketName;

}
