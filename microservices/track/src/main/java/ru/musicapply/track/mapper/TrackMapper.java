package ru.musicapply.track.mapper;

import org.mapstruct.Mapper;
import ru.musicapply.api.trackservice.dto.TrackResponseDtoShort;
import ru.musicapply.track.domain.Track;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE
)
public interface TrackMapper {

    TrackResponseDtoShort toShortDto(Track track);

}
