package ru.musicapply.track.service;

import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import ru.musicapply.api.event.track.CreateTrackEvent;
import ru.musicapply.api.trackservice.dto.TrackResponseDtoShort;
import ru.musicapply.api.trackservice.dto.TrackResponseFullDto;
import ru.musicapply.track.configuration.MinioProperties;
import ru.musicapply.track.domain.Track;
import ru.musicapply.track.integraion.TrackServiceIntegration;
import ru.musicapply.track.mapper.TrackMapper;
import ru.musicapply.track.repository.TrackRepository;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class TrackService {

    private final Path basePath = Paths.get("/tmp/upload/");
    private final TrackServiceIntegration integration;

    private final MinioClient minioClient;
    private final MinioProperties minioProperties;
    private final TrackRepository repository;

    private final TrackMapper mapper;


    public Mono<Void> createTrack(CreateTrackEvent event) {
        Track track = Track.builder()
                           .albumId(event.getAlbumId())
                           .trackId(UUID.randomUUID().toString())
                           .name(event.getName())
                           .build();

        String trackId = UUID.randomUUID().toString();
        String fileName = trackId + ".mp3";

        return Mono.fromCallable(() -> Files.write(basePath.resolve(fileName), event.getTrack()))
                   .publishOn(Schedulers.boundedElastic())
                   .flatMap(p -> Mono.fromCallable(() -> minioClient.uploadObject(UploadObjectArgs.builder()
                                                                                                  .bucket(minioProperties.getTrackBucketName())
                                                                                                  .object(fileName)
                                                                                                  .filename("/tmp/upload/" + fileName)
                                                                                                  .build())
                   ))
                   .flatMap(owr -> {
                       track.setFileUrl("https://api.musicapply.ru/" + minioProperties.getTrackBucketName() + "/" + fileName);
                       return repository.save(track);
                   })
                   .then();
    }

    public Mono<TrackResponseDtoShort> getShortTrackByTrackId(String trackId) {
        return repository.getTrackByTrackId(trackId)
                         .map(mapper::toShortDto);
    }

    public Mono<TrackResponseFullDto> getFullTrackByTrackId(String trackId) {
        return repository.getTrackByTrackId(trackId)
                         .flatMap(track -> integration.getAlbumByIdShort(track.getAlbumId())
                                                      .flatMap(album -> integration.getArtistByArtistIdShort(album.getArtistId())
                                                                                   .map(artist -> TrackResponseFullDto.builder()
                                                                                                                  .album(album)
                                                                                                                  .artist(artist)
                                                                                                                  .fileUrl(track.getFileUrl())
                                                                                                                  .trackId(track.getTrackId())
                                                                                                                  .name(track.getName())
                                                                                                                  .build())
                                                      )
                         );
    }

    public Flux<TrackResponseFullDto> getFullTracksByAlbumId(String albumId) {
        return repository.getTracksByAlbumId(albumId)
                         .flatMap(track -> integration.getAlbumByIdShort(track.getAlbumId())
                                                      .flatMap(album -> integration.getArtistByArtistIdShort(album.getArtistId())
                                                                                   .map(artist -> TrackResponseFullDto.builder()
                                                                                                                      .album(album)
                                                                                                                      .artist(artist)
                                                                                                                      .fileUrl(track.getFileUrl())
                                                                                                                      .trackId(track.getTrackId())
                                                                                                                      .name(track.getName())
                                                                                                                      .build())
                                                                                  )
        );
    }

    public Flux<TrackResponseFullDto> findFullTracksLikeName(String name) {
        return repository.findTracksByNameLike(name)
                         .flatMap(track -> integration.getAlbumByIdShort(track.getAlbumId())
                                                      .flatMap(album -> integration.getArtistByArtistIdShort(album.getArtistId())
                                                                                   .map(artist -> TrackResponseFullDto.builder()
                                                                                                                      .album(album)
                                                                                                                      .artist(artist)
                                                                                                                      .fileUrl(track.getFileUrl())
                                                                                                                      .trackId(track.getTrackId())
                                                                                                                      .name(track.getName())
                                                                                                                      .build())
                                                      )
                         );
    }
}
