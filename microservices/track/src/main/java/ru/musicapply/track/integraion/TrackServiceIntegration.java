package ru.musicapply.track.integraion;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.albumservice.api.AlbumServiceApi;
import ru.musicapply.api.albumservice.dto.AlbumResponseFullDto;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;
import ru.musicapply.api.artistservice.api.ArtistServiceApi;
import ru.musicapply.api.artistservice.dto.ArtistResponseDto;
import ru.musicapply.api.artistservice.dto.ArtistResponseShortDto;
import ru.musicapply.track.configuration.IntegrationProperties;

import java.net.URI;

@Component
@Slf4j
public class TrackServiceIntegration implements ArtistServiceApi, AlbumServiceApi {

    private final String ARTIST_SERVICE_URL;
    private final String ALBUM_SERVICE_URL;

    private final WebClient webClient;

    @Autowired
    public TrackServiceIntegration(
            WebClient.Builder webClientBuilder,
            IntegrationProperties integrationProperties
    ) {
        this.webClient = WebClient.builder().build();
        this.ARTIST_SERVICE_URL = "http://" + integrationProperties.getArtistService();
        this.ALBUM_SERVICE_URL = "http://" + integrationProperties.getAlbumService();
    }


    @Override
    public Mono<AlbumResponseShortDto> getAlbumByIdShort(String albumId) {

        URI url = UriComponentsBuilder.fromUriString(ALBUM_SERVICE_URL
                + "/album/short/{albumId}").build(albumId);

        log.debug("Will call the getAlbumByIdShort API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(AlbumResponseShortDto.class);
    }

    @Override
    public Mono<AlbumResponseFullDto> getAlbumByAlbumId(String albumId) {
        throw new NotImplementedException("getAlbumByAlbumId is not allowed here");
    }

    @Override
    public Flux<AlbumResponseShortDto> getAlbumsByArtistIdShort(String artistId) {
        throw new NotImplementedException("getAlbumsByArtistIdShort is not allowed here");
    }

    @Override
    public Flux<AlbumResponseFullDto> searchAlbumsLikeName(String name) {
        throw new NotImplementedException("searchAlbumsLikeName is not allowed here");
    }

    @Override
    public Mono<ArtistResponseShortDto> getArtistByArtistIdShort(String artistId) {

        URI url = UriComponentsBuilder.fromUriString(ARTIST_SERVICE_URL
                + "/artist/short/{artistId}").build(artistId);

        log.debug("Will call the getArtistByArtistIdShort API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(ArtistResponseShortDto.class);
    }

    @Override
    public Mono<ArtistResponseDto> getArtistByArtistIdFull(String artistId) {
        throw new NotImplementedException("getAlbumByAlbumId is not allowed here");
    }

    @Override
    public Flux<ArtistResponseDto> searchArtistsLikeName(String name) {
        throw new NotImplementedException("searchArtistsLikeName is not allowed here");
    }
}
