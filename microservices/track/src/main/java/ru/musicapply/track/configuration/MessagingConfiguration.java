package ru.musicapply.track.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.musicapply.api.event.EventType;
import ru.musicapply.api.event.track.CreateTrackEvent;
import ru.musicapply.track.service.TrackService;

import java.util.function.Consumer;


@Configuration
@RequiredArgsConstructor
public class MessagingConfiguration {

    private final TrackService trackService;

    @Bean
    public Consumer<Flux<CreateTrackEvent>> createTrackConsumer() {
        return flux -> flux.filter(e -> e.getType() == EventType.CREATE_TRACK)
                           .publishOn(Schedulers.boundedElastic())
                           .flatMap(this.trackService::createTrack)
                           .subscribeOn(Schedulers.single())
                           .subscribe();
    }

}
