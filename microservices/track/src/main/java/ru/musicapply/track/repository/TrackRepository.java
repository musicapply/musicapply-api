package ru.musicapply.track.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.track.domain.Track;


@Repository
public interface TrackRepository extends ReactiveMongoRepository<Track, ObjectId> {

    Mono<Track> getTrackByTrackId(String trackId);
    Flux<Track> getTracksByAlbumId(String albumId);

    @Query(value = "{'name': {$regex : ?0, $options: 'i'}}")
    Flux<Track> findTracksByNameLike(String name);

}
