package ru.musicapply.track.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "tracks")
public class Track {

    @Id
    private ObjectId id;
    private String trackId;
    private String name;
    private String albumId;
    private String fileUrl;

}
