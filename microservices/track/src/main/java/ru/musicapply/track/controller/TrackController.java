package ru.musicapply.track.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.trackservice.api.TrackServiceApi;
import ru.musicapply.api.trackservice.dto.TrackResponseDtoShort;
import ru.musicapply.api.trackservice.dto.TrackResponseFullDto;
import ru.musicapply.track.service.TrackService;


@RestController
@RequestMapping("/track")
@RequiredArgsConstructor
public class TrackController implements TrackServiceApi {

    private final TrackService trackService;

    @Override
    public Mono<TrackResponseDtoShort> getTrackByIdShort(String trackId) {
        return trackService.getShortTrackByTrackId(trackId);
    }

    @Override
    public Mono<TrackResponseFullDto> getTrackByIdFull(String trackId) {
        return trackService.getFullTrackByTrackId(trackId);
    }

    @Override
    public Flux<TrackResponseFullDto> getTracksByAlbumId(String albumId) {
        return trackService.getFullTracksByAlbumId(albumId);
    }

    @Override
    public Flux<TrackResponseFullDto> searchTracksLikeName(String name) {
        return trackService.findFullTracksLikeName(name);
    }

}
