package ru.musicapply.productservice.mapper;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import ru.musicapply.api.productservice.dto.CreateProductRequest;
import ru.musicapply.api.productservice.dto.ProductResponseDto;
import ru.musicapply.api.productservice.internal.dto.InternalProductResponseDto;
import ru.musicapply.productservice.aggregates.ProductAggregate;
import ru.musicapply.productservice.projection.ProductInfoRootEntityProjection;


import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;


class AggregateMapperTest {

    private final AggregateMapper mapper = Mappers.getMapper(AggregateMapper.class);


    @Test
    public void contextLoad() {
        assertThat(mapper).isNotNull();
    }

    @Test
    public void testMapCreateProductRequestToAggregate() {
        String name = "Premium";
        String description = "Description";
        Integer unitAmount = 16900;
        String currency = "rub";

        CreateProductRequest request = new CreateProductRequest(
                name,
                description,
                unitAmount,
                currency
        );

        ProductAggregate aggregate = mapper.toAggregate(request);

        assertAll(
                () -> assertEquals(name, aggregate.getName()),
                () -> assertEquals(description, aggregate.getDescription()),
                () -> assertEquals(unitAmount, aggregate.getPrice().getUnitAmount()),
                () -> assertEquals(currency, aggregate.getPrice().getCurrency()),
                () -> assertThat(aggregate.getStripeId()).isNull()
        );
    }

    @Test
    public void testMapProductInfoRootEntityProjectionToProductAggregate() {
        Integer id = 1;
        String productId = UUID.randomUUID().toString();
        String stripeId = "prod_LmaLBzumPpXOuJ";
        String name = "Premium subscription";
        String imageUrl = "http://localhost/static/img.webp";
        String description = "Some description";
        Integer unitAmount = 16900;
        String currency = "rub";

        ProductInfoRootEntityProjection projection = new ProductInfoRootEntityProjection();
        projection.setId(id);
        projection.setProductId(productId);
        projection.setStripeId(stripeId);
        projection.setName(name);
        projection.setImageUrl(imageUrl);
        projection.setDescription(description);
        projection.setUnitAmount(unitAmount);
        projection.setCurrency(currency);

        ProductAggregate aggregate = mapper.toAggregate(projection);

        assertAll(
                () -> assertEquals(productId, aggregate.getProductId().getProductId()),
                () -> assertEquals(stripeId, aggregate.getStripeId()),
                () -> assertEquals(name, aggregate.getName()),
                () -> assertEquals(imageUrl, aggregate.getImageUrl()),
                () -> assertEquals(description, aggregate.getDescription()),
                () -> assertEquals(unitAmount, aggregate.getPrice().getUnitAmount()),
                () -> assertEquals(currency, aggregate.getPrice().getCurrency())
        );
    }

    @Test
    public void testMapProductAggregateToProductInfoRootEntityProjection() {
        String productId = UUID.randomUUID().toString();
        String stripeId = "prod_LmaLBzumPpXOuJ";
        String name = "Premium subscription";
        String imageUrl = "http://localhost/static/img.webp";
        String description = "Some description";
        Integer unitAmount = 16900;
        String currency = "rub";

        ProductAggregate aggregate = ProductAggregate.builder()
                                                     .productId(productId)
                                                     .stripeId(stripeId)
                                                     .name(name)
                                                     .imageUrl(imageUrl)
                                                     .description(description)
                                                     .unitAmount(unitAmount)
                                                     .currency(currency)
                                                     .build();

        ProductInfoRootEntityProjection projection = mapper.toProjection(aggregate);

        assertAll(
                () -> assertEquals(productId, projection.getProductId()),
                () -> assertEquals(stripeId, projection.getStripeId()),
                () -> assertEquals(name, projection.getName()),
                () -> assertEquals(imageUrl, projection.getImageUrl()),
                () -> assertEquals(description, projection.getDescription()),
                () -> assertEquals(unitAmount, projection.getUnitAmount()),
                () -> assertEquals(currency, projection.getCurrency())
        );
    }

    @Test
    public void testMapProductAggregateToResponse() {
        String productId = UUID.randomUUID().toString();
        String stripeId = "prod_LmaLBzumPpXOuJ";
        String name = "Premium subscription";
        String imageUrl = "http://localhost/static/img.webp";
        String description = "Some description";
        Integer unitAmount = 16900;
        String currency = "rub";

        ProductAggregate aggregate = ProductAggregate.builder()
                                                     .productId(productId)
                                                     .stripeId(stripeId)
                                                     .name(name)
                                                     .imageUrl(imageUrl)
                                                     .description(description)
                                                     .unitAmount(unitAmount)
                                                     .currency(currency)
                                                     .build();

        ProductResponseDto dto = mapper.toResponse(aggregate);

        assertAll(
                () -> assertEquals(productId, dto.getId()),
                () -> assertEquals(name, dto.getName()),
                () -> assertEquals(imageUrl, dto.getImageUrl()),
                () -> assertEquals(description, dto.getDescription()),
                () -> assertEquals(unitAmount, dto.getUnitAmount()),
                () -> assertEquals(currency, dto.getCurrency())
        );
    }

    @Test
    public void testMapProductAggregateTointernalResponse() {
        String productId = UUID.randomUUID().toString();
        String stripeId = "prod_LmaLBzumPpXOuJ";
        String name = "Premium subscription";
        String imageUrl = "http://localhost/static/img.webp";
        String description = "Some description";
        Integer unitAmount = 16900;
        String currency = "rub";

        ProductAggregate aggregate = ProductAggregate.builder()
                                                     .productId(productId)
                                                     .stripeId(stripeId)
                                                     .name(name)
                                                     .imageUrl(imageUrl)
                                                     .description(description)
                                                     .unitAmount(unitAmount)
                                                     .currency(currency)
                                                     .build();

        InternalProductResponseDto dto = mapper.toInternalResponse(aggregate);

        assertAll(
                () -> assertEquals(productId, dto.getId()),
                () -> assertEquals(stripeId, dto.getStripeId()),
                () -> assertEquals(name, dto.getName()),
                () -> assertEquals(imageUrl, dto.getImageUrl()),
                () -> assertEquals(description, dto.getDescription()),
                () -> assertEquals(unitAmount, dto.getUnitAmount()),
                () -> assertEquals(currency, dto.getCurrency()),
                () -> assertThat(dto.getDefaultPriceId()).isNullOrEmpty()
        );
    }



}
