CREATE TABLE product(
    id              SERIAL          NOT NULL,
    product_id      VARCHAR(50),
    stripe_id       VARCHAR(50),
    name            VARCHAR(250)    NOT NULL,
    image_url       TEXT,
    description     TEXT            NOT NULL,
    unit_amount     Integer         NOT NULL,
    currency        VARCHAR(10)     NOT NULL
);

ALTER TABLE IF EXISTS product
    ADD CONSTRAINT PK_product__id
        PRIMARY KEY (id);

ALTER TABLE IF EXISTS product
    ADD CONSTRAINT Q_product__product_id
        UNIQUE (product_id);

ALTER TABLE IF EXISTS product
    ADD CONSTRAINT Q_product__name
        UNIQUE (name);

ALTER TABLE IF EXISTS product
    ADD CONSTRAINT Q_product__stripe_id
        UNIQUE (stripe_id);

ALTER TABLE IF EXISTS product
    ADD CONSTRAINT Q_product__id_product_id_stripe_id_name
        UNIQUE (id, product_id, stripe_id, name);