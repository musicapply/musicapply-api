package ru.musicapply.productservice.mapper;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import ru.musicapply.api.productservice.dto.CreateProductRequest;
import ru.musicapply.api.productservice.dto.ProductResponseDto;
import ru.musicapply.productservice.aggregates.ProductAggregate;
import ru.musicapply.productservice.aggregates.ProductAggregate.ProductAggregateBuilder;
import ru.musicapply.productservice.projection.ProductInfoRootEntityProjection;
import ru.musicapply.productservice.projection.ProductInfoRootEntityProjection.ProductInfoRootEntityProjectionBuilder;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-06-01T10:40:37+0600",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.3 (Amazon.com Inc.)"
)
@Component
public class AggregateMapperImpl implements AggregateMapper {

    @Override
    public ProductAggregate toAggregate(CreateProductRequest request) {
        if ( request == null ) {
            return null;
        }

        ProductAggregateBuilder productAggregate = ProductAggregate.builder();

        productAggregate.name( request.getName() );
        productAggregate.description( request.getDescription() );
        productAggregate.unitAmount( request.getUnitAmount() );
        productAggregate.currency( request.getCurrency() );

        return productAggregate.build();
    }

    @Override
    public ProductAggregate toAggregate(ProductInfoRootEntityProjection projection) {
        if ( projection == null ) {
            return null;
        }

        ProductAggregateBuilder productAggregate = ProductAggregate.builder();

        productAggregate.productId( projection.getProductId() );
        productAggregate.stripeId( projection.getStripeId() );
        productAggregate.name( projection.getName() );
        productAggregate.imageUrl( projection.getImageUrl() );
        productAggregate.description( projection.getDescription() );
        productAggregate.unitAmount( projection.getUnitAmount() );
        productAggregate.currency( projection.getCurrency() );

        return productAggregate.build();
    }

    @Override
    public ProductResponseDto toResponse(ProductAggregate aggregate) {
        if ( aggregate == null ) {
            return null;
        }

        String name = null;
        String description = null;
        String imageUrl = null;

        name = aggregate.getName();
        description = aggregate.getDescription();
        imageUrl = aggregate.getImageUrl();

        String id = aggregate.getProductId().getProductId();
        Integer unitAmount = aggregate.getPrice().getUnitAmount();
        String currency = aggregate.getPrice().getCurrency();

        ProductResponseDto productResponseDto = new ProductResponseDto( id, name, description, imageUrl, unitAmount, currency );

        return productResponseDto;
    }

    @Override
    public ProductInfoRootEntityProjection toProjection(ProductAggregate aggregate) {
        if ( aggregate == null ) {
            return null;
        }

        ProductInfoRootEntityProjectionBuilder productInfoRootEntityProjection = ProductInfoRootEntityProjection.builder();

        productInfoRootEntityProjection.stripeId( aggregate.getStripeId() );
        productInfoRootEntityProjection.name( aggregate.getName() );
        productInfoRootEntityProjection.imageUrl( aggregate.getImageUrl() );
        productInfoRootEntityProjection.description( aggregate.getDescription() );

        productInfoRootEntityProjection.productId( aggregate.getProductId().getProductId() );
        productInfoRootEntityProjection.unitAmount( aggregate.getPrice().getUnitAmount() );
        productInfoRootEntityProjection.currency( aggregate.getPrice().getCurrency() );

        return productInfoRootEntityProjection.build();
    }
}
