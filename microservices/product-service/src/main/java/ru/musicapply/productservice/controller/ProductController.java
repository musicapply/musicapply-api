package ru.musicapply.productservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.productservice.api.ProductServiceApi;
import ru.musicapply.api.productservice.dto.CreateProductRequest;
import ru.musicapply.api.productservice.dto.CreateProductResponse;
import ru.musicapply.api.productservice.dto.ProductResponseDto;
import ru.musicapply.productservice.service.ProductService;


@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController implements ProductServiceApi {

    private final ProductService productService;

    @Override
    public Mono<CreateProductResponse> createProduct(CreateProductRequest request) {
        return productService.createProduct(request);
    }

    @Override
    public Mono<ProductResponseDto> getProductById(String productId) {
        return productService.findProductById(productId);
    }

    @Override
    public Flux<ProductResponseDto> getAllProducts() {
        return productService.findAllProducts();
    }


}
