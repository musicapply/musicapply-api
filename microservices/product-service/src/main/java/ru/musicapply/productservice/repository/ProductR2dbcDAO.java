package ru.musicapply.productservice.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.musicapply.productservice.projection.ProductInfoRootEntityProjection;

@Repository
public interface ProductR2dbcDAO extends R2dbcRepository<ProductInfoRootEntityProjection, Integer> {

    Mono<ProductInfoRootEntityProjection> findByProductId(String productId);
    Mono<ProductInfoRootEntityProjection> findByName(String name);
    Mono<ProductInfoRootEntityProjection> findByStripeId(String stripeId);

}
