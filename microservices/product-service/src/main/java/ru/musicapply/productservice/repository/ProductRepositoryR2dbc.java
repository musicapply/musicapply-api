package ru.musicapply.productservice.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.productservice.aggregates.ProductAggregate;
import ru.musicapply.productservice.mapper.AggregateMapper;
import ru.musicapply.sharedmodel.ProductId;


@Component
@RequiredArgsConstructor
public class ProductRepositoryR2dbc implements ProductRepository {

    private final ProductR2dbcDAO productDAO;
    private final AggregateMapper mapper;

    @Override
    public Mono<ProductAggregate> save(ProductAggregate product) {
        return Mono.just(product)
                   .map(mapper::toProjection)
                   .flatMap(productDAO::save)
                   .then(Mono.just(product));
    }

    @Override
    public Mono<ProductAggregate> findById(Integer id) {
        return productDAO.findById(id).map(mapper::toAggregate);
    }

    @Override
    public Mono<ProductAggregate> findByProductId(ProductId productId) {
        return productDAO.findByProductId(productId.getProductId())
                         .map(mapper::toAggregate);
    }

    @Override
    public Mono<ProductAggregate> findByStripeId(String stripeId) {
        return productDAO.findByStripeId(stripeId)
                         .map(mapper::toAggregate);
    }

    @Override
    public Mono<ProductAggregate> findByName(String name) {
        return productDAO.findByName(name)
                         .map(mapper::toAggregate);
    }

    @Override
    public Flux<ProductAggregate> findAll() {
        return productDAO.findAll()
                         .map(mapper::toAggregate);
    }

    @Override
    public Mono<ProductAggregate> updateProduct(ProductId id, ProductAggregate aggregate) {
        return productDAO.findByProductId(id.getProductId())
                .map(aggregateProjection -> {
                    aggregateProjection.setName(aggregate.getName());
                    aggregateProjection.setDescription((aggregate.getDescription()));
                    aggregateProjection.setStripeId(aggregate.getStripeId());
                    aggregateProjection.setImageUrl(aggregate.getImageUrl());
                    aggregateProjection.setUnitAmount(aggregate.getPrice().getUnitAmount());
                    aggregateProjection.setCurrency(aggregate.getPrice().getCurrency());

                    return aggregateProjection;
                })
                .flatMap(productDAO::save)
                .map(mapper::toAggregate);
    }
}
