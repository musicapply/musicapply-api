package ru.musicapply.productservice;

import com.stripe.Stripe;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.musicapply.productservice.configuration.IntegrationProperties;
import ru.musicapply.productservice.configuration.StripeProperties;


@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		StripeProperties.class,
		IntegrationProperties.class
})
@RequiredArgsConstructor
public class ProductServiceApplication {

	private final StripeProperties stripeProperties;

	public static void main(String[] args) {
		SpringApplication.run(ProductServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner() {
		return args -> {
			Stripe.apiKey = stripeProperties.getPrivateKey();
		};
	}

}
