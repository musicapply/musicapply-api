package ru.musicapply.productservice.aggregates;

import ru.musicapply.sharedmodel.Price;
import ru.musicapply.sharedmodel.ProductId;

class ProductInfoRootEntity {

    ProductId id;
    String stripeId;
    String name;
    String imageUrl;
    String description;
    Price price;

    ProductInfoRootEntity(
            ProductId productId,
            String stripeId,
            String name,
            String imageUrl,
            String description,
            Price price
    ) {
        this.id = productId;
        this.stripeId = stripeId;
        this.name = name;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
    }

}
