package ru.musicapply.productservice.repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.productservice.aggregates.ProductAggregate;
import ru.musicapply.sharedmodel.ProductId;


public interface ProductRepository {

    Mono<ProductAggregate> save(ProductAggregate product);
    Mono<ProductAggregate> findById(Integer id);
    Mono<ProductAggregate> findByProductId(ProductId productId);
    Mono<ProductAggregate> findByStripeId(String stripeId);
    Mono<ProductAggregate> findByName(String name);
    Flux<ProductAggregate> findAll();
    Mono<ProductAggregate> updateProduct(ProductId id, ProductAggregate aggregate);

}
