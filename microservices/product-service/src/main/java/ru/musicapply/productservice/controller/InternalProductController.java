package ru.musicapply.productservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.musicapply.api.productservice.internal.api.InternalProductServiceApi;
import ru.musicapply.api.productservice.internal.dto.InternalProductResponseDto;
import ru.musicapply.productservice.service.ProductService;

@RestController
@RequestMapping("/internal/product")
@RequiredArgsConstructor
@Slf4j
public class InternalProductController implements InternalProductServiceApi {

    private final ProductService productService;


    @Override
    public Mono<InternalProductResponseDto> getProductById(String productId) {
        return productService.internalFindProductById(productId);
    }

    @Override
    public Mono<InternalProductResponseDto> getProductByStripeId(String stripeId) {
        return productService.internalFindProductByStripeId(stripeId);
    }
}
