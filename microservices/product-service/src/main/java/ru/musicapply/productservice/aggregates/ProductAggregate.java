package ru.musicapply.productservice.aggregates;

import ru.musicapply.microarchitecture.Aggregate;
import ru.musicapply.microarchitecture.AggregateBuilder;
import ru.musicapply.sharedmodel.Price;
import ru.musicapply.sharedmodel.ProductId;

@Aggregate
public class ProductAggregate {

    private ProductInfoRootEntity productInfo;

    private ProductAggregate(ProductAggregateBuilder builder) {
        this.productInfo = new ProductInfoRootEntity(
                builder.productId,
                builder.stripeId,
                builder.name,
                builder.imageUrl,
                builder.description,
                new Price(builder.unitAmount, builder.currency)
        );
    }

    public void setProductId(ProductId id) {
        this.productInfo.id = id;
    }

    public ProductId getProductId() {
        return productInfo.id;
    }

    public String getStripeId() {
        return productInfo.stripeId;
    }

    public String getName() {
        return productInfo.name;
    }

    public String getImageUrl() {
        return productInfo.imageUrl;
    }

    public String getDescription() {
        return productInfo.description;
    }

    public Price getPrice() {
        return productInfo.price;
    }

    public void setStripeId(String stripeId) {
        this.productInfo.stripeId = stripeId;
    }

    public static ProductAggregate.ProductAggregateBuilder builder() {
        return new ProductAggregate.ProductAggregateBuilder();
    }

    @AggregateBuilder
    public static class ProductAggregateBuilder {

        private ProductId productId;
        private String stripeId;
        private String name;
        private String imageUrl;
        private String description;
        private Integer unitAmount;
        private String currency;

        private ProductAggregateBuilder() {

        }

        public ProductAggregateBuilder productId(String productId) {
            this.productId = new ProductId(productId);
            return this;
        }

        public ProductAggregateBuilder stripeId(String stripeId) {
            this.stripeId = stripeId;
            return this;
        }

        public ProductAggregateBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ProductAggregateBuilder imageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public ProductAggregateBuilder description(String description) {
            this.description = description;
            return this;
        }

        public ProductAggregateBuilder unitAmount(Integer unitAmount) {
            this.unitAmount = unitAmount;
            return this;
        }

        public ProductAggregateBuilder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public ProductAggregate build() {
            return new ProductAggregate(this);
        }

    }

}
