package ru.musicapply.productservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;
import ru.musicapply.api.productservice.dto.CreateProductRequest;
import ru.musicapply.api.productservice.dto.CreateProductResponse;
import ru.musicapply.api.productservice.dto.ProductResponseDto;
import ru.musicapply.api.productservice.internal.dto.InternalProductResponseDto;
import ru.musicapply.productservice.aggregates.ProductAggregate;
import ru.musicapply.productservice.mapper.AggregateMapper;
import ru.musicapply.productservice.repository.ProductRepository;
import ru.musicapply.productservice.utils.StripeUtils;
import ru.musicapply.sharedmodel.ProductId;

import java.util.UUID;

import static reactor.function.TupleUtils.function;
import static ru.musicapply.productservice.utils.StripeUtils.createStripePrice;
import static ru.musicapply.productservice.utils.StripeUtils.createStripeProduct;
import static ru.musicapply.productservice.utils.StripeUtils.setDefaultPriceToStripeProduct;


@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final AggregateMapper mapper;

    public Mono<CreateProductResponse> createProduct(CreateProductRequest productDto) {
        String productId = UUID.randomUUID().toString();
        return productRepository.findByName(productDto.getName())
                                .flatMap(p -> Mono.error(new IllegalStateException("Same product already exists")))
                                .switchIfEmpty(Mono.just(productDto)
                                                   .map(dto -> {
                                                       ProductAggregate aggregate = mapper.toAggregate(dto);

                                                       aggregate.setProductId(new ProductId(productId));
                                                       return aggregate;
                                                   })
                                                   .flatMap(productRepository::save)
                                                   .flatMap(productAggregate -> Mono.fromCallable(
                                                           () -> Tuples.of(
                                                                   productAggregate,
                                                                   createStripeProduct(
                                                                           productAggregate.getName(),
                                                                           productAggregate.getDescription()
                                                                   )
                                                   )))
                                                   .flatMap(function((pAggregate, p) -> Mono.fromCallable(
                                                           () -> Tuples.of(
                                                                   pAggregate,
                                                                   p,
                                                                   createStripePrice(
                                                                           pAggregate.getPrice().getUnitAmount(),
                                                                           pAggregate.getPrice().getCurrency(),
                                                                           p.getId()
                                                                   )
                                                           )
                                                   )))
                                                   .flatMap(function((pAggregate, product, price) -> Mono.fromCallable(
                                                           () -> Tuples.of(
                                                                   pAggregate,
                                                                   setDefaultPriceToStripeProduct(
                                                                           price.getId(),
                                                                           product.getId()
                                                                   ),
                                                                   price
                                                           )
                                                   )))
                                                   .flatMap(function((pAggregate, product, price) -> {
                                                       pAggregate.setStripeId(product.getId());
                                                       return productRepository.updateProduct(pAggregate.getProductId(), pAggregate);
                                                   }))
                                )
                                .then(Mono.just(new CreateProductResponse(productId)));
    }

    public Mono<ProductResponseDto> findProductById(String productId) {
        return productRepository.findByProductId(new ProductId(productId))
                                .map(mapper::toResponse);
    }

    public Mono<InternalProductResponseDto> internalFindProductById(String productId) {
        return productRepository.findByProductId(new ProductId(productId))
                                .map(mapper::toInternalResponse)
                                .flatMap(dto -> Mono.fromCallable(() ->
                                    Tuples.of(
                                            dto,
                                            StripeUtils.retrieveProduct(dto.getStripeId())
                                    )
                                ))
                                .flatMap(function((dto, stripeProduct) -> {
                                    dto.setDefaultPriceId(stripeProduct.getDefaultPrice());

                                    return Mono.just(dto);
                                }));
    }

    public Flux<ProductResponseDto> findAllProducts() {
        return productRepository.findAll()
                                .map(mapper::toResponse);
    }

    public Mono<InternalProductResponseDto> internalFindProductByStripeId(String stripeId) {
        return productRepository.findByStripeId(stripeId)
                                .map(mapper::toInternalResponse)
                                .flatMap(dto -> Mono.fromCallable(() ->
                                        Tuples.of(
                                                dto,
                                                StripeUtils.retrieveProduct(dto.getStripeId())
                                        )
                                ))
                                .flatMap(function((dto, stripeProduct) -> {
                                    dto.setDefaultPriceId(stripeProduct.getDefaultPrice());

                                    return Mono.just(dto);
                                }));
    }
}
