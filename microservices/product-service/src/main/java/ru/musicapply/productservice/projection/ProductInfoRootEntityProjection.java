package ru.musicapply.productservice.projection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("product")
public class ProductInfoRootEntityProjection {

    @Id
    private Integer id;

    @Column("product_id")
    private String productId;

    @Column("stripe_id")
    private String stripeId;

    @Column("name")
    private String name;

    @Column("image_url")
    private String imageUrl;

    @Column("description")
    private String description;

    @Column("unit_amount")
    private Integer unitAmount;

    @Column("currency")
    private String currency;

}
