package ru.musicapply.productservice.utils;

import com.stripe.exception.StripeException;
import com.stripe.model.Price;
import com.stripe.model.Product;

import java.util.HashMap;
import java.util.Map;

public class StripeUtils {

    public static Product createStripeProduct(String name, String description) throws StripeException {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);
        params.put("description", description);

        return Product.create(params);
    }

    public static Price createStripePrice(Integer unitAmount, String currency, String productId) throws StripeException {
        Map<String, Object> recurring = new HashMap<>();
        recurring.put("interval", "month");

        Map<String, Object> params = new HashMap<>();
        params.put("unit_amount", unitAmount);
        params.put("currency", currency);
        params.put("recurring", recurring);
        params.put("product", productId);

        return Price.create(params);
    }

    public static Product setDefaultPriceToStripeProduct(String priceId, String productId) throws StripeException {
        Product product = Product.retrieve(productId);

        Map<String, Object> params = new HashMap<>();
        params.put(
                "default_price",
                priceId
        );

        return product.update(params);
    }

    public static Product retrieveProduct(String productId) throws StripeException {
        return Product.retrieve(productId);
    }

}
