package ru.musicapply.productservice.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "stripe")
@Data
public class StripeProperties {

    private String publicKey;
    private String privateKey;

}
