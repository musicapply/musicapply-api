package ru.musicapply.productservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.musicapply.api.productservice.dto.CreateProductRequest;
import ru.musicapply.api.productservice.dto.ProductResponseDto;
import ru.musicapply.api.productservice.internal.dto.InternalProductResponseDto;
import ru.musicapply.productservice.aggregates.ProductAggregate;
import ru.musicapply.productservice.projection.ProductInfoRootEntityProjection;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE
)
public interface AggregateMapper {

    ProductAggregate toAggregate(CreateProductRequest request);
    ProductAggregate toAggregate(ProductInfoRootEntityProjection projection);

    @Mapping(target = "id", expression = "java(aggregate.getProductId().getProductId())")
    @Mapping(target = "unitAmount", expression = "java(aggregate.getPrice().getUnitAmount())")
    @Mapping(target = "currency", expression = "java(aggregate.getPrice().getCurrency())")
    ProductResponseDto toResponse(ProductAggregate aggregate);

    @Mapping(target = "id", expression = "java(aggregate.getProductId().getProductId())")
    @Mapping(target = "unitAmount", expression = "java(aggregate.getPrice().getUnitAmount())")
    @Mapping(target = "currency", expression = "java(aggregate.getPrice().getCurrency())")
    InternalProductResponseDto toInternalResponse(ProductAggregate aggregate);

    @Mapping(target = "productId", expression = "java(aggregate.getProductId().getProductId())")
    @Mapping(target = "unitAmount", expression = "java(aggregate.getPrice().getUnitAmount())")
    @Mapping(target = "currency", expression = "java(aggregate.getPrice().getCurrency())")
    ProductInfoRootEntityProjection toProjection(ProductAggregate aggregate);

}
