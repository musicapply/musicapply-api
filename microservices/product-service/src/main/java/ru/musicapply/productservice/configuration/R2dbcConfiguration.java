package ru.musicapply.productservice.configuration;

import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;

import static io.r2dbc.spi.ConnectionFactoryOptions.*;
import static io.r2dbc.spi.ConnectionFactoryOptions.DATABASE;

@Configuration
public class R2dbcConfiguration extends AbstractR2dbcConfiguration {

    @Autowired
    private Environment environment;

    @Override
    @Bean
    public ConnectionFactory connectionFactory() {
        return ConnectionFactories.get(
                ConnectionFactoryOptions.builder()
                                        .option(DRIVER, "postgresql")
                                        .option(HOST, environment.getProperty("POSTGRES_DB_HOST"))
                                        .option(USER, environment.getProperty("POSTGRES_DB_USERNAME"))
                                        .option(PORT, Integer.parseInt(environment.getProperty("POSTGRES_DB_PORT")))
                                        .option(PASSWORD, environment.getProperty("POSTGRES_DB_PASSWORD"))
                                        .option(DATABASE, environment.getProperty("POSTGRES_DB_DATABASE_NAME"))
                                        .build()
        );
    }

}
