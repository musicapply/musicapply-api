package ru.musicapply.productservice.security;

import io.jsonwebtoken.*;
import org.springframework.stereotype.Component;

@Component
public class JwtParser {

    public Jwt<Header, Claims> parseJwt(String jwt) {
        String[] split = jwt.split("\\.");
        String untrustedJwt = split[0] + "." + split[1] + ".";

        return Jwts.parser()
                   .parseClaimsJwt(untrustedJwt);
    }

}
