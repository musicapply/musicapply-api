#Product microservice

### Environment variables  
  * POSTGRES_DB_HOST (postgres database host address)
  * POSTGRES_DB_PORT (postgres database port)
  * POSTGRES_DB_DATABASE_NAME (database name)
  * POSTGRES_DB_USERNAME (postgres database username)
  * POSTGRES_DB_PASSWORD (postgres database password)
  * STRIPE_PUBLIC_KEY (stripe public key)
  * STRIPE_PRIVATE_KEY (stripe private key)  

### Run in IDE

1. Add Environment variables in `Run Configuration`
2. Start database

### Run in `docker-compose`

1. Build  
  `./gradlew :microservices:product-service:build  `  
2. Build image  
  `docker build -f src/main/docker/Dockerfile -t musicapply/product-service:1 .`  
3. Run  
  `docker-compose up -d product-service`