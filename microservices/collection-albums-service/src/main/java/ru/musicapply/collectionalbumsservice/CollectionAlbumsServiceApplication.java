package ru.musicapply.collectionalbumsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import ru.musicapply.collectionalbumsservice.configuration.IntegrationProperties;


@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		IntegrationProperties.class
})
public class CollectionAlbumsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollectionAlbumsServiceApplication.class, args);
	}

}
