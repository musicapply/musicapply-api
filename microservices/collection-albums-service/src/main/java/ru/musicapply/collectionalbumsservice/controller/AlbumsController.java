package ru.musicapply.collectionalbumsservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.albums.api.CollectionAlbumsServiceApi;
import ru.musicapply.api.collection.albums.dto.AlbumRecordDto;
import ru.musicapply.collectionalbumsservice.security.UserPrincipal;
import ru.musicapply.collectionalbumsservice.service.CollectionAlbumsService;


@RestController
@RequestMapping("/collection/albums")
@RequiredArgsConstructor
public class AlbumsController implements CollectionAlbumsServiceApi {

    private final CollectionAlbumsService service;

    @Override
    public Mono<Void> addAlbumInLibrary(String albumId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.addAlbumInLibrary(albumId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> removeAlbumFromFavorite(String albumId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.removeAlbumFromFavorite(albumId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Flux<AlbumRecordDto> getAllAlbumsInLibrary() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMapMany(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.getAllAlbumByUserId(userPrincipal.getUserId());
                                            });
    }
}
