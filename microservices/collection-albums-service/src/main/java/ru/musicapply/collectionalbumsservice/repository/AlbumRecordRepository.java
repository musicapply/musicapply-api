package ru.musicapply.collectionalbumsservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.collectionalbumsservice.domain.AlbumRecord;

@Repository
public interface AlbumRecordRepository extends ReactiveMongoRepository<AlbumRecord, ObjectId> {

    Mono<AlbumRecord> findByUserIdAndAlbumId(String userId, String albumId);
    Flux<AlbumRecord> findByUserIdOrderByCreationDateDesc(String userId);

}
