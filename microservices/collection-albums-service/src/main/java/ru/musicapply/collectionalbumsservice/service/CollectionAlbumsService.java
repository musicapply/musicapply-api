package ru.musicapply.collectionalbumsservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.albums.dto.AlbumRecordDto;
import ru.musicapply.collectionalbumsservice.domain.AlbumRecord;
import ru.musicapply.collectionalbumsservice.integration.CollectionAlbumsServiceIntegration;
import ru.musicapply.collectionalbumsservice.repository.AlbumRecordRepository;

import java.time.LocalDateTime;


@Service
@RequiredArgsConstructor
public class CollectionAlbumsService {

    private final AlbumRecordRepository albumRecordRepository;
    private final CollectionAlbumsServiceIntegration integration;


    public Mono<Void> addAlbumInLibrary(String albumId, String userId) {
        return albumRecordRepository.findByUserIdAndAlbumId(userId, albumId)
                                    .switchIfEmpty(integration.getAlbumByIdShort(albumId)
                                                              .flatMap(album -> {
                                                                  AlbumRecord record = AlbumRecord.builder()
                                                                                                  .albumId(album.getAlbumId())
                                                                                                  .userId(userId)
                                                                                                  .creationDate(LocalDateTime.now())
                                                                                                  .build();

                                                                  return albumRecordRepository.save(record);
                                                              })
                                    )
                                    .then();
    }

    public Mono<Void> removeAlbumFromFavorite(String albumId, String userId) {
        return albumRecordRepository.findByUserIdAndAlbumId(userId, albumId)
                                    .flatMap(albumRecordRepository::delete);
    }

    public Flux<AlbumRecordDto> getAllAlbumByUserId(String userId) {
        return albumRecordRepository.findByUserIdOrderByCreationDateDesc(userId)
                                    .flatMapSequential(albumRecord -> integration.getAlbumByAlbumId(albumRecord.getAlbumId())
                                                                                 .map(albumDto -> {
                                                                                     AlbumRecordDto dto = AlbumRecordDto.builder()
                                                                                                                        .id(albumRecord.getId().toString())
                                                                                                                        .creationDate(albumRecord.getCreationDate())
                                                                                                                        .album(albumDto)
                                                                                                                        .build();
                                                                                     return dto;

                                    }));
    }

}
