package ru.musicapply.collectionalbumsservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "albums")
public class AlbumRecord {

    @Id
    private ObjectId id;
    private String albumId;
    private String userId;
    private LocalDateTime creationDate;

}
