package ru.musicapply.collectionalbumsservice.integration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.albumservice.api.AlbumServiceApi;
import ru.musicapply.api.albumservice.dto.AlbumResponseFullDto;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;
import ru.musicapply.collectionalbumsservice.configuration.IntegrationProperties;

import java.net.URI;

@Component
@Slf4j
public class CollectionAlbumsServiceIntegration implements AlbumServiceApi {

    private final String ALBUM_SERVICE_URL;

    private final WebClient webClient;


    @Autowired
    public CollectionAlbumsServiceIntegration(
            WebClient.Builder webClientBuilder,
            IntegrationProperties integrationProperties
    ) {
        this.webClient = WebClient.builder().build();
        this.ALBUM_SERVICE_URL = "http://" + integrationProperties.getAlbumService();
    }


    @Override
    public Mono<AlbumResponseShortDto> getAlbumByIdShort(String albumId) {
        URI url = UriComponentsBuilder.fromUriString(ALBUM_SERVICE_URL
                + "/album/short/{albumId}").build(albumId);

        log.debug("Will call the getAlbumByIdShort API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(AlbumResponseShortDto.class);
    }

    @Override
    public Mono<AlbumResponseFullDto> getAlbumByAlbumId(String albumId) {
        URI url = UriComponentsBuilder.fromUriString(ALBUM_SERVICE_URL
                + "/album/{albumId}").build(albumId);

        log.debug("Will call the getAlbumByAlbumId API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(AlbumResponseFullDto.class);
    }

    @Override
    public Flux<AlbumResponseShortDto> getAlbumsByArtistIdShort(String artistId) {
        throw new NotImplementedException("getAlbumsByArtistIdShort not allowed here");
    }

    @Override
    public Flux<AlbumResponseFullDto> searchAlbumsLikeName(String name) {
        throw new NotImplementedException("searchAlbumsLikeName not allowed here");
    }
}
