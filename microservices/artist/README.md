#Artist microservice

### Environment variables  
  * DB_HOST (mongodb host address)
  * DB_PORT (mongodb port)
  * DB_DATABASE (database name)
  * DB_USERNAME (mongodb username)
  * DB_PASSWORD (mongodb password)
  * MINIO_USERNAME (minio admin username)
  * MINIO_PASSWORD (minio admin password)
  * MINIO_SERVER_HOST (minio server host address)
  * MINIO_SERVER_PORT (minio server port)
  * KAFKA_BROKER_HOST (apache kafka broker host address)
  * KAFKA_BROKER_PORT (apache kafka broker port)  

### Run in IDE

1. Add Environment variables in `Run Configuration`
2. Start MongoDB database, Minio and Kafka broker

### Run in `docker-compose`

1. Build  
  `./gradlew :microservices:artist-service:build  `  
2. Build image  
  `docker build -f src/main/docker/Dockerfile -t musicapply/artist-service:1 .`  
3. Run  
  `docker-compose up -d artist-service`