package ru.musicapply.artist.security;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
@RequiredArgsConstructor
public class JwtAuthenticationManager implements ReactiveAuthenticationManager {

    private final JwtParser jwtParser;

    @Override
    @SuppressWarnings("unchecked")
    public Mono<Authentication> authenticate(Authentication authentication) {
        return Mono.just(authentication)
                   .map(a -> jwtParser.parseJwt(a.getCredentials().toString()))
                   .onErrorResume(Mono::error)
                   .map(jws -> {
                       Claims claims = jws.getBody();
                       String username = claims.get("preferred_username", String.class);
                       String id = claims.get("sub", String.class);
                       Map<String, List<String>> realm_access = claims.get("realm_access", Map.class);
                       List<String> roles = realm_access.get("roles")
                                                        .stream()
                                                        .map(String::toUpperCase)
                                                        .map(r -> "ROLE_" + r)
                                                        .collect(Collectors.toList());



                       UserPrincipal principal = UserPrincipal.builder()
                                                              .id(id)
                                                              .username(username)
                                                              .authorities(roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()))
                                                              .build();

                       return new UsernamePasswordAuthenticationToken(
                               principal,
                               null,
                               roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList())
                       );
                   });
    }

}
