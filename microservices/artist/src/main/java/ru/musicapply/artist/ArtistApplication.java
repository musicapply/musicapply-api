package ru.musicapply.artist;

import io.minio.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.musicapply.artist.configuration.IntegrationProperties;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;


@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		IntegrationProperties.class,
		ru.musicapply.artist.configuration.MinioProperties.class
})
@RequiredArgsConstructor
public class ArtistApplication {

	private final Path uploadPath = Paths.get("/tmp/upload/");
	private final MinioClient minioClient;
	private final ru.musicapply.artist.configuration.MinioProperties minioProperties;

	public static void main(String[] args) {
		SpringApplication.run(ArtistApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner() {
		return args -> {
			BucketExistsArgs bucketExistsArgs = BucketExistsArgs.builder().bucket(minioProperties.getArtistBucketName()).build();
			boolean isExists = minioClient.bucketExists(bucketExistsArgs);

			if (!isExists) {
				MakeBucketArgs makeBucketArgs = MakeBucketArgs.builder().bucket(minioProperties.getArtistBucketName()).build();
				minioClient.makeBucket(makeBucketArgs);
			}

			File uploadDirectory = new File(uploadPath.toString());
			if (!uploadDirectory.exists()) {
				uploadDirectory.mkdirs();
			}

		};
	}

}
