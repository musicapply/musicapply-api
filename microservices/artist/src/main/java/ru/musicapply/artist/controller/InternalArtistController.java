package ru.musicapply.artist.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.musicapply.api.artistservice.internal.api.InternalArtistApi;
import ru.musicapply.api.artistservice.internal.dto.InternalArtistResponseDto;
import ru.musicapply.artist.service.ArtistService;


@RestController
@RequestMapping("/internal/artist")
@RequiredArgsConstructor
public class InternalArtistController implements InternalArtistApi {

    private final ArtistService service;

    @Override
    public Mono<InternalArtistResponseDto> findByUserId(String userId) {
        return service.findArtistByUserId(userId);
    }
}
