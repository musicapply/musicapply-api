package ru.musicapply.artist.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "artists")
public class Artist {

    @Id
    private ObjectId id;

    private String artistId;
    private String userId;
    private String imageUrl;
    private String name;
    private String bio;

}
