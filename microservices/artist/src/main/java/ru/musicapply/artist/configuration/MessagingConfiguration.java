package ru.musicapply.artist.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.musicapply.api.event.EventType;
import ru.musicapply.api.event.artist.CreateArtistEvent;
import ru.musicapply.artist.service.ArtistService;

import java.util.function.Consumer;


@Configuration
@RequiredArgsConstructor
public class MessagingConfiguration {

    private final ArtistService artistService;

    @Bean
    public Consumer<Flux<CreateArtistEvent>> createArtistConsumer() {
        return flux -> flux.filter(e -> e.getType() == EventType.CREATE_ARTIST)
                           .publishOn(Schedulers.boundedElastic())
                           .flatMap(this.artistService::createArtist)
                           .subscribeOn(Schedulers.single())
                           .subscribe();
    }

}
