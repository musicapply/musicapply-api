package ru.musicapply.artist.integration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.albumservice.api.AlbumServiceApi;
import ru.musicapply.api.albumservice.dto.AlbumResponseFullDto;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;
import ru.musicapply.artist.configuration.IntegrationProperties;

import java.net.URI;

@Component
@Slf4j
public class ArtistServiceIntegration implements AlbumServiceApi {

    private final String ALBUM_SERVICE_URL;

    private final WebClient webClient;


    @Autowired
    public ArtistServiceIntegration(
            WebClient.Builder webClientBuilder,
            IntegrationProperties integrationProperties
    ) {
        this.webClient = WebClient.builder().build();
        this.ALBUM_SERVICE_URL = "http://" + integrationProperties.getAlbumService();
    }


    @Override
    public Mono<AlbumResponseShortDto> getAlbumByIdShort(String albumId) {
        throw new NotImplementedException("getAlbumByAlbumId not allowed here");
    }

    @Override
    public Mono<AlbumResponseFullDto> getAlbumByAlbumId(String albumId) {
        throw new NotImplementedException("getAlbumByAlbumId not allowed here");
    }

    @Override
    public Flux<AlbumResponseShortDto> getAlbumsByArtistIdShort(String artistId) {

        URI url = UriComponentsBuilder.fromUriString(ALBUM_SERVICE_URL
                + "/album/findByArtistId/{artistId}").build(artistId);

        log.debug("Will call the getAlbumsByArtistIdShort API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToFlux(AlbumResponseShortDto.class);
    }

    @Override
    public Flux<AlbumResponseFullDto> searchAlbumsLikeName(String name) {
        throw new NotImplementedException("searchAlbumsLikeName not allowed here");
    }
}
