package ru.musicapply.artist.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.artist.domain.Artist;

@Repository
public interface ArtistRepository extends ReactiveMongoRepository<Artist, ObjectId> {

    Mono<Artist> findByUserId(String userId);
    Mono<Artist> findByArtistId(String artistId);

    @Query(value = "{'name': {$regex : ?0, $options: 'i'}}")
    Flux<Artist> findByNameLike(String name);

}
