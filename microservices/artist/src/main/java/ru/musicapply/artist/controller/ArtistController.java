package ru.musicapply.artist.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.artistservice.api.ArtistServiceApi;
import ru.musicapply.api.artistservice.dto.ArtistResponseDto;
import ru.musicapply.api.artistservice.dto.ArtistResponseShortDto;
import ru.musicapply.artist.service.ArtistService;

@RestController
@RequestMapping("/artist")
@RequiredArgsConstructor
public class ArtistController implements ArtistServiceApi {

    private final ArtistService artistService;

    @Override
    public Mono<ArtistResponseShortDto> getArtistByArtistIdShort(String artistId) {
        return artistService.getShortArtistByArtistId(artistId);
    }

    @Override
    public Mono<ArtistResponseDto> getArtistByArtistIdFull(String artistId) {
        return artistService.getFullArtistByArtistId(artistId);
    }

    @Override
    public Flux<ArtistResponseDto> searchArtistsLikeName(String name) {
        return artistService.findFullArtistsLikeName(name);
    }
}
