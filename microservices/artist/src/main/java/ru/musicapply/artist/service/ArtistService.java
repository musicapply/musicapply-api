package ru.musicapply.artist.service;

import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import ru.musicapply.api.artistservice.dto.ArtistResponseDto;
import ru.musicapply.api.artistservice.dto.ArtistResponseShortDto;
import ru.musicapply.api.artistservice.internal.dto.InternalArtistResponseDto;
import ru.musicapply.api.event.artist.CreateArtistEvent;
import ru.musicapply.artist.configuration.MinioProperties;
import ru.musicapply.artist.domain.Artist;
import ru.musicapply.artist.integration.ArtistServiceIntegration;
import ru.musicapply.artist.mapper.ArtistMapper;
import ru.musicapply.artist.repository.ArtistRepository;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class ArtistService {

    private final Path basePath = Paths.get("/tmp/upload/");

    private final MinioClient minioClient;
    private final MinioProperties minioProperties;
    private final ArtistRepository artistRepository;
    private final ArtistServiceIntegration integration;

    private final ArtistMapper mapper;


    public Mono<Void> createArtist(CreateArtistEvent event) {
        Artist artist = Artist.builder()
                              .name(event.getName())
                              .bio(event.getBio())
                              .userId(event.getUserId())
                              .artistId(UUID.randomUUID().toString())
                              .build();

        String imageId = UUID.randomUUID().toString();
        String fileName = imageId + ".png";


        return Mono.fromCallable(() -> Files.write(basePath.resolve(fileName), event.getImage()))
                   .publishOn(Schedulers.boundedElastic())
                   .flatMap(p -> Mono.fromCallable(() -> minioClient.uploadObject(UploadObjectArgs.builder()
                                                                                              .bucket(minioProperties.getArtistBucketName())
                                                                                              .object(fileName)
                                                                                              .filename("/tmp/upload/" + fileName)
                                                                                              .build())
               ))
                   .flatMap(owr -> {
                       artist.setImageUrl("https://api.musicapply.ru/" + minioProperties.getArtistBucketName() + "/" + fileName);
                       return artistRepository.save(artist);
                   })
                   .then();
    }

    public Mono<InternalArtistResponseDto> findArtistByUserId(String userId) {
        return artistRepository.findByUserId(userId)
                               .map(mapper::toInternalResponse);
    }

    public Mono<ArtistResponseShortDto> getShortArtistByArtistId(String artistId) {
        return artistRepository.findByArtistId(artistId).map(mapper::toShortDto);
    }

    public Mono<ArtistResponseDto> getFullArtistByArtistId(String artistId) {
        return artistRepository.findByArtistId(artistId)
                               .map(mapper::toDto)
                               .flatMap(artist -> integration.getAlbumsByArtistIdShort(artistId)
                                                             .doOnNext(album -> artist.getAlbums().add(album))
                                                             .then(Mono.just(artist))
                               );
    }

    public Flux<ArtistResponseDto> findFullArtistsLikeName(String artistName) {
        return artistRepository.findByNameLike(artistName)
                               .map(mapper::toDto)
                               .flatMap(artist -> integration.getAlbumsByArtistIdShort(artist.getArtistId())
                                                             .doOnNext(album -> artist.getAlbums().add(album))
                                                             .then(Mono.just(artist))
                               );
    }
}
