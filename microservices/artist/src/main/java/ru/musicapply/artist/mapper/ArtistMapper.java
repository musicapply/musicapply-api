package ru.musicapply.artist.mapper;

import org.mapstruct.Mapper;
import ru.musicapply.api.artistservice.dto.ArtistResponseDto;
import ru.musicapply.api.artistservice.dto.ArtistResponseShortDto;
import ru.musicapply.api.artistservice.internal.dto.InternalArtistResponseDto;
import ru.musicapply.artist.domain.Artist;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE
)
public interface ArtistMapper {

    InternalArtistResponseDto toInternalResponse(Artist artist);
    ArtistResponseShortDto toShortDto(Artist artist);
    ArtistResponseDto toDto(Artist artist);

}
