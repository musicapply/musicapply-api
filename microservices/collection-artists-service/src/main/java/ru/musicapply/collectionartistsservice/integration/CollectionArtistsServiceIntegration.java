package ru.musicapply.collectionartistsservice.integration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.artistservice.api.ArtistServiceApi;
import ru.musicapply.api.artistservice.dto.ArtistResponseDto;
import ru.musicapply.api.artistservice.dto.ArtistResponseShortDto;
import ru.musicapply.collectionartistsservice.configuration.IntegrationProperties;

import java.net.URI;


@Component
@Slf4j
public class CollectionArtistsServiceIntegration implements ArtistServiceApi {

    private final String ARTIST_SERVICE_URL;

    private final WebClient webClient;


    @Autowired
    public CollectionArtistsServiceIntegration(
            WebClient.Builder webClientBuilder,
            IntegrationProperties integrationProperties
    ) {
        this.webClient = WebClient.builder().build();
        this.ARTIST_SERVICE_URL = "http://" + integrationProperties.getArtistService();
    }



    @Override
    public Mono<ArtistResponseShortDto> getArtistByArtistIdShort(String artistId) {
        URI url = UriComponentsBuilder.fromUriString(ARTIST_SERVICE_URL
                + "/artist/short/{artistId}").build(artistId);

        log.debug("Will call the getArtistByArtistIdShort API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(ArtistResponseShortDto.class);
    }

    @Override
    public Mono<ArtistResponseDto> getArtistByArtistIdFull(String artistId) {
        URI url = UriComponentsBuilder.fromUriString(ARTIST_SERVICE_URL
                + "/artist/{artistId}").build(artistId);

        log.debug("Will call the getArtistByArtistIdFull API on URL: {}", url);

        return webClient.get()
                        .uri(url)
                        .retrieve()
                        .bodyToMono(ArtistResponseDto.class);
    }

    @Override
    public Flux<ArtistResponseDto> searchArtistsLikeName(String name) {
        throw new NotImplementedException("searchArtistsLikeName not allowed here");
    }
}
