package ru.musicapply.collectionartistsservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.collectionartistsservice.domain.ArtistRecord;


@Repository
public interface ArtistRecordRepository extends ReactiveMongoRepository<ArtistRecord, ObjectId> {

    Mono<ArtistRecord> findByUserIdAndArtistId(String userId, String artistId);
    Flux<ArtistRecord> findByUserIdOrderByCreationDateDesc(String userId);

}
