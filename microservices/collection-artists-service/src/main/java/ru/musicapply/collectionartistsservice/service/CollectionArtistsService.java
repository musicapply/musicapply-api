package ru.musicapply.collectionartistsservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.artists.dto.ArtistRecordDto;
import ru.musicapply.collectionartistsservice.domain.ArtistRecord;
import ru.musicapply.collectionartistsservice.integration.CollectionArtistsServiceIntegration;
import ru.musicapply.collectionartistsservice.repository.ArtistRecordRepository;

import java.time.LocalDateTime;


@Service
@RequiredArgsConstructor
public class CollectionArtistsService {

    private final ArtistRecordRepository repository;
    private final CollectionArtistsServiceIntegration integration;

    public Mono<Void> addArtistInLibrary(String artistId, String userId) {
        return repository.findByUserIdAndArtistId(userId, artistId)
                         .switchIfEmpty(integration.getArtistByArtistIdShort(artistId)
                                                   .flatMap(artist -> {
                                                       ArtistRecord record = ArtistRecord.builder()
                                                                                         .artistId(artist.getArtistId())
                                                                                         .creationDate(LocalDateTime.now())
                                                                                         .userId(userId)
                                                                                         .build();

                                                       return repository.save(record);
                                                   })
                         )
                         .then();
    }

    public Mono<Void> removeArtistFromLibrary(String artistId, String userId) {
        return repository.findByUserIdAndArtistId(userId, artistId)
                         .flatMap(repository::delete);
    }

    public Flux<ArtistRecordDto> getAllArtistsByUserId(String userId) {
        return repository.findByUserIdOrderByCreationDateDesc(userId)
                         .flatMapSequential(artistRecord -> integration.getArtistByArtistIdFull(artistRecord.getArtistId())
                                                                       .map(artist -> {
                                                                           ArtistRecordDto dto = ArtistRecordDto.builder()
                                                                                                                .id(artistRecord.getId().toString())
                                                                                                                .creationDate(artistRecord.getCreationDate())
                                                                                                                .artist(artist)
                                                                                                                .build();

                                                                           return dto;
                                                                       })
                         );
    }

}
