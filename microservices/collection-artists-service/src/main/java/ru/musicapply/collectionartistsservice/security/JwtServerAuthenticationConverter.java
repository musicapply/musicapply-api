package ru.musicapply.collectionartistsservice.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


@Component
public class JwtServerAuthenticationConverter implements ServerAuthenticationConverter {

    @Override
    public Mono<Authentication> convert(ServerWebExchange exchange) {
        return Mono.justOrEmpty(exchange)
                   .flatMap(e -> Mono.justOrEmpty(e.getRequest().getHeaders().get("X-Access-Token")))
                   .filter(e -> !e.isEmpty())
                   .map(e -> e.get(0))
                   .map(e -> new UsernamePasswordAuthenticationToken(e, e));
    }

}
