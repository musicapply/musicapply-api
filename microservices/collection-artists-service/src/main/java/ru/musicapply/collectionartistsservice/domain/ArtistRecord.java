package ru.musicapply.collectionartistsservice.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "artists")
public class ArtistRecord {

    @Id
    private ObjectId id;
    private String artistId;
    private String userId;
    private LocalDateTime creationDate;

}
