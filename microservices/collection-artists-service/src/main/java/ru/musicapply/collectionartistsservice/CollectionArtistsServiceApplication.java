package ru.musicapply.collectionartistsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import ru.musicapply.collectionartistsservice.configuration.IntegrationProperties;


@SpringBootApplication
@ComponentScan("ru.musicapply")
@EnableConfigurationProperties({
		IntegrationProperties.class
})
public class CollectionArtistsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollectionArtistsServiceApplication.class, args);
	}

}
