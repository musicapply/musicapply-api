package ru.musicapply.collectionartistsservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.artists.api.CollectionArtistsServiceApi;
import ru.musicapply.api.collection.artists.dto.ArtistRecordDto;
import ru.musicapply.collectionartistsservice.security.UserPrincipal;
import ru.musicapply.collectionartistsservice.service.CollectionArtistsService;


@RestController
@RequestMapping("/collection/artists")
@RequiredArgsConstructor
public class ArtistsController implements CollectionArtistsServiceApi {

    private final CollectionArtistsService service;


    @Override
    public Mono<Void> addArtistInLibrary(String artistId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.addArtistInLibrary(artistId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Mono<Void> removeArtistFromLibrary(String artistId) {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMap(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.removeArtistFromLibrary(artistId, userPrincipal.getUserId());
                                            });
    }

    @Override
    public Flux<ArtistRecordDto> getAllArtistsInLibrary() {
        return ReactiveSecurityContextHolder.getContext()
                                            .map(SecurityContext::getAuthentication)
                                            .map(Authentication::getPrincipal)
                                            .flatMapMany(u -> {
                                                UserPrincipal userPrincipal = (UserPrincipal) u;
                                                return service.getAllArtistsByUserId(userPrincipal.getUserId());
                                            });
    }
}
