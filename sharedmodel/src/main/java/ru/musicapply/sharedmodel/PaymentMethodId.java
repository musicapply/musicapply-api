package ru.musicapply.sharedmodel;

public class PaymentMethodId {

    private String id;

    public PaymentMethodId(String id) {
        this.id = id;
    }

    public String getPaymentMethodId() {
        return id;
    }

    @Override
    public String toString() {
        return id;
    }
}
