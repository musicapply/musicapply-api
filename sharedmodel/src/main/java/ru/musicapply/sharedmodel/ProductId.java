package ru.musicapply.sharedmodel;

public class ProductId {

    private String productId;

    public ProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    @Override
    public String toString() {
        return productId;
    }
}
