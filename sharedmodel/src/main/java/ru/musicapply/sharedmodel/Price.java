package ru.musicapply.sharedmodel;

import java.util.StringJoiner;

public class Price {

    private Integer unitAmount;
    private String currency;

    public Price(Integer unitAmount, String currency) {
        this.unitAmount = unitAmount;
        this.currency = currency;
    }

    public Integer getUnitAmount() {
        return unitAmount;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Price.class.getSimpleName() + "[", "]")
                .add("unitAmount=" + unitAmount)
                .add("currency='" + currency + "'")
                .toString();
    }

}
