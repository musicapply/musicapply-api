package ru.musicapply.sharedmodel;

public class SubscriptionId {

    private String subscriptionId;

    public SubscriptionId(String id) {
        this.subscriptionId = id;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    @Override
    public String toString() {
        return subscriptionId;
    }
}
