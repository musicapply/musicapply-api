package ru.musicapply.sharedmodel;

public class UserId {

    private String userId;

    public UserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return userId;
    }

}
