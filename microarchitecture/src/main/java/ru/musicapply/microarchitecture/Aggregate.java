package ru.musicapply.microarchitecture;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Documented
public @interface Aggregate {
}
