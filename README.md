# MusicApply - streaming music platform🤔

### Live demo: https://open.musicapply.ru


### Project features  
   * :heavy_check_mark: Spring Boot + Project Reactor  
   * :heavy_check_mark: Kafka  
   * :heavy_check_mark: Minio  
   * :heavy_check_mark: API Gateway (Kong)  
   * :heavy_check_mark: Microservices  
   * :heavy_check_mark: Stripe (Payment processing)  
   * :heavy_check_mark: CQRS/ES/DDD (user-service, payment-service, customer-service)  
   * :x: Kubernetes  
   * :x: Istio  
   * :x: Debezium 
   * :x: AWS 
   * :x: CI/CD 
   * :x: ELK 
   * :x: Protobuf 
   * :x: Filtering, Sorting, Pagination 

### Run locally in docker-compose  

1. Generate SSL certificates for API Gateway and IAM server:
    * cd in `docker/ssl` in project folder
    * Generate certification authority:
        ```console
        foo@bar:~$ openssl genrsa -aes256 -out localCertificationAuthority.key 4096
        
        Enter pass phrase for localCertificationAuthority.key: <type password here>
        Verifying - Enter pass phrase for localCertificationAuthority.key: <type password here>
        ```
    * ```console
      foo@bar:~$ openssl req -x509 -new -key localCertificationAuthority.key -days 10000 -out localCertificationAuthority.crt -sha512
      
      Enter pass phrase for localCertificationAuthority.key: <type password here>
      
      You are about to be asked to enter information that will be incorporated into your certificate request.
      What you are about to enter is what is called a Distinguished Name or a DN.
      There are quite a few fields but you can leave some blank
      For some fields there will be a default value,
      If you enter '.', the field will be left blank
      ---
      Country Name (2 letter code) [AU]:RU
      State or Province Name (full name) [Some-State]:Omsk
      Locality Name (eg, city) []:Omsk
      Organization Name (eg, company) [Internet Widgits Pty Ltd]:Musicapply
      Organizational Unit Name (eg, section) []:Musicapply
      Common Name (e.g. server FQDN or YOUR name) []:musicapply.ru
      Email Address []:support@musicapply.ru
      ```
    * Generate key and certificate for keycloak
      ```console
      foo@bar:~$ openssl genrsa -out keycloak.key 4096
      ```
      
      ```
      foo@bar:~$ cp /etc/ssl/openssl.cnf ./
      ```
      ```console
      foo@bar:~$ nano ./openssl.cnf
      ```  
      Uncomment #req_extensions line
      ![uncomment req_extensions](./images/docker-compose/v3_req.png)
      
      Add line `subjectAltName = @alt_names`  
      ![Add line subjectAltName = @alt_names](./images/docker-compose/subjectAltName.png)
      
      Create a new section [ alt_names ] at the bottom of the config file.  
      ![alt_names](./images/docker-compose/alt_names.png)  
      
      Create the certificate request for keycloak
      ```console
      foo@bar:~$ openssl req -new -key keycloak.key -out keycloak.csr -sha512 -config ./openssl.cnf -extensions v3_req  
      
      Country Name (2 letter code) [AU]:RU
      State or Province Name (full name) [Some-State]:Siberia
      Locality Name (eg, city) []:Omsk
      Organization Name (eg, company) [Internet Widgits Pty Ltd]:Musicapply
      Organizational Unit Name (eg, section) []:Musicapply Accounts
      Common Name (e.g. server FQDN or YOUR name) []:accounts.musicapply.ru
      Email Address []:support@musicapply.ru
      
      Please enter the following 'extra' attributes
      to be sent with your certificate request
      A challenge password []:
      An optional company name []:

      ```  
      Generate the certificate for keycloak  
      ```console
      foo@bar:~$ openssl x509 -req -in keycloak.csr -CA localCertificationAuthority.crt -CAkey localCertificationAuthority.key -CAcreateserial -out keycloak.crt -days 365 -sha512 -extfile ./openssl.cnf -extensions v3_req  
      
      Signature ok
      subject=C = RU, ST = Siberia, L = Omsk, O = Musicapply, OU = Musicapply Accounts, CN = accounts.musicapply.ru, emailAddress = support@musicapply.ru
      Getting CA Private Key
      Enter pass phrase for localCertificationAuthority.key: <Enter password here>
      ```
      
      Verify  
      ![verify keycloak](./images/docker-compose/verify_keycloak.png)  
      
      Copy `keycloak.key` and `keycloak.crt` to `docker/keycloak/certs` folder  
      
    * Generate key and certificate for Kong API Gateway  
      ```console
      foo@bar:~$ openssl genrsa -out api_gateway.key
      ```
      ```  
      foo@bar:~$ cp /etc/ssl/openssl.cnf ./
      ```
      ```
      foo@bar:~$ nano ./openssl.cnf
      ```  
      Uncomment #req_extensions line
      ![uncomment req_extensions](./images/docker-compose/v3_req.png)  
            
      Add line `subjectAltName = @alt_names`  
      ![Add line subjectAltName = @alt_names](./images/docker-compose/subjectAltName.png)  
      
      Create a new section [ alt_names ] at the bottom of the config file.  
      ![alt_names](./images/docker-compose/alt_names_api_gateway.png)
          
      Generate the certificate request for API Gateway  
      ```
      foo@bar:~$ openssl req -new -key api_gateway.key -out api_gateway.csr -sha512 -config ./openssl.cnf -extensions v3_req  
      
      Country Name (2 letter code) [AU]:RU
      State or Province Name (full name) [Some-State]:Siberia
      Locality Name (eg, city) []:Omsk
      Organization Name (eg, company) [Internet Widgits Pty Ltd]:Musicapply
      Organizational Unit Name (eg, section) []:Musicapply api
      Common Name (e.g. server FQDN or YOUR name) []:api.musicapply.ru
      Email Address []:support@musicapply.ru
      
      Please enter the following 'extra' attributes
      to be sent with your certificate request
      A challenge password []:
      An optional company name []:
      ```  
      Generate the certificate for API Gateway (you can attach `-trustout` property)
      ```console
      foo@bar:~$ openssl x509 -req -in api_gateway.csr -CA localCertificationAuthority.crt -CAkey localCertificationAuthority.key -CAcreateserial -out api_gateway.crt -days 365 -sha512 -extfile ./openssl.cnf -extensions v3_req  
      
      Signature ok
      subject=C = RU, ST = Siberia, L = Omsk, O = Musicapply, OU = Musicapply api, CN = api.musicapply.ru, emailAddress = support@musicapply.ru
      Getting CA Private Key
      Enter pass phrase for localCertificationAuthority.key: <type password here>
      ```  
      Verify  
      ![verify API Gateway](./images/docker-compose/verify_api_gateway.png)  
      
      Copy `api_gateway.key` and `api_gateway.crt` to `docker/kong/certs` folder
    
    * Install the certification authority in operating system  
      ```console
      foo@bar:~$ sudo cp localCertificationAuthority.crt /usr/local/share/ca-certificates
      foo@bar:~$ sudo update-ca-certificates  
      
      Updating certificates in /etc/ssl/certs...
      1 added, 0 removed; done.
      Running hooks in /etc/ca-certificates/update.d...
      
      Adding debian:localCertificationAuthority.pem
      done.
      done.
      ```  
      
    * Install the certification authority in java truststore
      ```console
      foo@bar:~$ sudo keytool -keystore ${JAVA_HOME}/lib/security/cacerts -list
      
      Enter keystore password: changeit
      Keystore type: JKS
      Keystore provider: SUN
      
      Your keystore contains 224 entries
      ```
      ```console
      foo@bar:~$ sudo keytool -import -alias musicapply_ruCA -keystore ${JAVA_HOME}/lib/security/cacerts -file ./localCertificationAuthority.crt  
      
      Enter keystore password: changeit
      Owner: EMAILADDRESS=support@musicapply.ru, CN=musicapply.ru, OU=Musicapply, O=Musicapply, L=Omsk, ST=Omsk, C=RU
      Issuer: EMAILADDRESS=support@musicapply.ru, CN=musicapply.ru, OU=Musicapply, O=Musicapply, L=Omsk, ST=Omsk, C=RU
      Serial number: 495d9b58d7e613003a942616ff32a21d73ab594e
      Valid from: Sat May 28 20:41:01 OMST 2022 until: Wed Oct 13 20:41:01 OMST 2049
      ...
      
      Trust this certificate? [no]:  yes
      Certificate was added to keystore
      ```
      ```console
      foo@bar:~$ sudo keytool -keystore ${JAVA_HOME}/lib/security/cacerts -list
            
      Enter keystore password: changeit
      Keystore type: JKS
      Keystore provider: SUN
            
      Your keystore contains 225 entries
      ```  
     
    * Add the certification authority in your browser  
        Add    
        ![Add CA to browser](./images/docker-compose/add_CA.png)  
        Verify  
        ![Verify CA browser installation](./images/docker-compose/CA_details.png)  

2. Add to /etc/hosts file several domains:
    * `172.35.0.1    accounts.musicapply.ru`
    * `172.36.0.1    api.musicapply.ru`

    `accounts.musicapply.ru` - Keycloak domain name  
    `api.musicapply.ru` - API Gateway domain name

    This is necessary so that the IAM server and API Gateway can communicate with each other. Two IP addresses allocated for them and specific docker containers attached to them.
    **You can choose any other ip address for IAM and API Gateway.**
    
    ![SSL certificates installed successfully](./images/docker-compose/SSL_ok.png)

3. Prepare environment for run in docker compose
    * Add read/write permission for api_gateway.key, api_gateway.crt, kong.yml
      ```console
      foo@bar:~$ cd docker/kong/certs
      foo@bar:~$ chmod +rw api_gateway.key
      foo@bar:~$ chmod +rw api_gateway.crt
      foo@bar:~$ cd ../config
      foo@bar:~$ chmod +rwx kong.yml
      ```  
      ![Prepare Kong API Gateway files](./images/docker-compose/prepare_kong_files.png)
      
    * Add `replica.key` for mongodb in each folder (mongo_db_service-name_service)
      ```console
      foo@bar:~$ cd docker/mongo_db_customer_service
      foo@bar:~$ cd ./scripts
      foo@bar:~$ openssl rand -base64 741 > replica.key
      foo@bar:~$ chmod 400 replica.key
      foo@bar:~$ sudo chown 999:999 ./replica.key
      ```  
      ![Check permission and owner of replica.key](./images/docker-compose/replica.key.png)
      
4. Generate .key for mongodb
    * `openssl rand -base64 741 > replica.key `  
    * `chmod 600 replica.key `  
    * `chmod 400 replica.key`  
    * `chown 999:999 /data/replica.key`  
    
5. Change permission for all files that passes as volumes in docker containers
6. Build project
    * `./gradlew build`

5. Manually build images
    ```console
    foo@bar:~$ cd microservices/user-service
    foo@bar:~$ docker build -f src/main/docker/Docker -t musicapply/user-sevice:1 .  
    ```
6. Run replica-set-setup
    ```console
        foo@bar:~$ cd docker
        foo@bar:~$ docker-compose up -d mongo_db_user mongo_db_customer mongo_db_payment  
        foo@bar:~$ docker-compose run --rm replica_setup_mongo_user_service
        foo@bar:~$ docker-compose run --rm replica_setup_mongo_customer_service  
        foo@bar:~$ docker-compose run --rm replica_setup_mongo_payment_service  
    ```
7. Run another containers
    ```console
    foo@bar:~$ cd docker
    foo@bar:~$ docker-compose up -d    
    ```
    
