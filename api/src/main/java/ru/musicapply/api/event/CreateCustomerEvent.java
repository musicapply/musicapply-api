package ru.musicapply.api.event;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class CreateCustomerEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.CREATE_CUSTOMER;
    private String userId;

    public CreateCustomerEvent() {

    }

    public CreateCustomerEvent(String userId) {
        this.userId = userId;
    }

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

    public String getUserId() {
        return userId;
    }


}
