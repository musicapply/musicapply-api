package ru.musicapply.api.event;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
public class NewTransactionEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.NEW_TRANSACTION_EVENT;
    private String userId;
    private Long amountPaid;
    private String currency;
    private LocalDateTime paymentDate;
    private String productId;
    private String paymentMethodId;
    private LocalDateTime nextPayment;

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

}
