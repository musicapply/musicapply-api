package ru.musicapply.api.event;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class RevokeUserRoleEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.REVOKE_USER_ROLE;
    private String userId;
    private String role;

    public RevokeUserRoleEvent() {

    }

    public RevokeUserRoleEvent(String userId, String role) {
        this.userId = userId;
        this.role = role;
    }

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

}
