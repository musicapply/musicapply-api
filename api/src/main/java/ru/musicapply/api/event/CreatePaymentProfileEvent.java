package ru.musicapply.api.event;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class CreatePaymentProfileEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.CREATE_PAYMENT_PROFILE;
    private String userId;

    public CreatePaymentProfileEvent() {

    }

    public CreatePaymentProfileEvent(String userId) {
        this.userId = userId;
    }

    @Override
    public UUID getEventId() {
        return eventId;
    }

    @Override
    public Date getDate() {
        return date;
    }

    public String getUserId() {
        return userId;
    }

}
