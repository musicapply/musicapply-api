package ru.musicapply.api.event;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;


@Data
public class CreateCustomerSubscriptionEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.CREATE_CUSTOMER_SUBSCRIPTION;
    private String userId;
    private String productId;
    private String stripeSubscriptionId;
    private LocalDateTime startSubscriptionDate;
    private LocalDateTime nextPaymentDate;

    public CreateCustomerSubscriptionEvent() {

    }

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

}
