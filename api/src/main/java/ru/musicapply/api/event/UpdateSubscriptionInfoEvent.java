package ru.musicapply.api.event;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;


@Data
public class UpdateSubscriptionInfoEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.UPDATE_SUBSCRIPTION_INFO;
    private String userId;
    private LocalDateTime nextPaymentDate;

    public UpdateSubscriptionInfoEvent() {}

    public UpdateSubscriptionInfoEvent(String userId, LocalDateTime nextPaymentDate) {
        this.userId = userId;
        this.nextPaymentDate = nextPaymentDate;
    }

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

}
