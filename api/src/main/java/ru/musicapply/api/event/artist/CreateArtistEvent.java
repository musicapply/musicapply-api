package ru.musicapply.api.event.artist;

import lombok.Data;
import org.springframework.http.codec.multipart.FilePart;
import ru.musicapply.api.event.Event;
import ru.musicapply.api.event.EventType;

import java.util.Date;
import java.util.UUID;

@Data
public class CreateArtistEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.CREATE_ARTIST;
    private String userId;
    private String name;
    private String bio;
    private byte[] image;


    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }
}
