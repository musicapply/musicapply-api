package ru.musicapply.api.event.album;

import lombok.Data;
import ru.musicapply.api.event.Event;
import ru.musicapply.api.event.EventType;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Data
public class CreateAlbumEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.CREATE_ALBUM;

    private String artistId;
    private String name;
    private String description;
    private LocalDateTime releaseDate;
    private byte[] image;


    @Override
    public UUID getEventId() {
        return eventId;
    }

    @Override
    public Date getDate() {
        return date;
    }
}
