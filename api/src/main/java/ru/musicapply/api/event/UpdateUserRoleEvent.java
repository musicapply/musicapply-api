package ru.musicapply.api.event;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class UpdateUserRoleEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.UPDATE_USER_ROLE;
    private String userId;
    private String role;

    public UpdateUserRoleEvent() {}

    public UpdateUserRoleEvent(
            String userId,
            String role
    ) {
        this.userId = userId;
        this.role = role;
    }

    @Override
    public UUID getEventId() {
        return eventId;
    }

    @Override
    public Date getDate() {
        return date;
    }
}
