package ru.musicapply.api.event;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class CancelSubscriptionEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.CANCEL_SUBSCRIPTION;
    private String userId;


    @Override
    public UUID getEventId() {
        return eventId;
    }

    @Override
    public Date getDate() {
        return date;
    }
}
