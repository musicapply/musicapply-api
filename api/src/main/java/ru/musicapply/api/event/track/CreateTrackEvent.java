package ru.musicapply.api.event.track;

import lombok.Data;
import ru.musicapply.api.event.Event;
import ru.musicapply.api.event.EventType;

import java.util.Date;
import java.util.UUID;

@Data
public class CreateTrackEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private final EventType type = EventType.CREATE_TRACK;
    private String albumId;
    private String name;
    private byte[] track;

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }
}
