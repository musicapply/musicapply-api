package ru.musicapply.api.trackservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.trackservice.dto.TrackResponseDtoShort;
import ru.musicapply.api.trackservice.dto.TrackResponseFullDto;


public interface TrackServiceApi {

    @GetMapping("/short/{trackId}")
    Mono<TrackResponseDtoShort> getTrackByIdShort(@PathVariable("trackId") String trackId);

    @GetMapping("/{trackId}")
    Mono<TrackResponseFullDto> getTrackByIdFull(@PathVariable("trackId") String trackId);

    @GetMapping("/findByAlbumId/{albumId}")
    Flux<TrackResponseFullDto> getTracksByAlbumId(@PathVariable("albumId") String albumId);

    @GetMapping("/search/likeName/{name}")
    Flux<TrackResponseFullDto> searchTracksLikeName(@PathVariable("name") String name);

}
