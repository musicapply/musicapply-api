package ru.musicapply.api.trackservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TrackResponseDtoShort {

    private final String trackId;
    private final String albumId;
    private final String name;
    private final String fileUrl;

    @JsonCreator
    public TrackResponseDtoShort(
            @JsonProperty(value = "trackId", required = true) final String trackId,
            @JsonProperty(value = "albumId", required = true) final String albumId,
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "fileUrl", required = true) final String fileUrl
    ) {
        this.trackId = trackId;
        this.albumId = albumId;
        this.name = name;
        this.fileUrl = fileUrl;
    }

}
