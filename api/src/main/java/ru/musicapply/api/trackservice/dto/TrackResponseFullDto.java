package ru.musicapply.api.trackservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;
import ru.musicapply.api.artistservice.dto.ArtistResponseShortDto;

@Data
@Builder
public class TrackResponseFullDto {

    private final String trackId;
    private final String name;
    private final String fileUrl;
    private final AlbumResponseShortDto album;
    private final ArtistResponseShortDto artist;

    @JsonCreator
    public TrackResponseFullDto(
            @JsonProperty(value = "trackId", required = true) final String trackId,
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "fileUrl", required = true) final String fileUrl,
            @JsonProperty(value = "album", required = true) final AlbumResponseShortDto album,
            @JsonProperty(value = "artist", required = true) final ArtistResponseShortDto artist
    ) {
        this.trackId = trackId;
        this.name = name;
        this.fileUrl = fileUrl;
        this.album = album;
        this.artist = artist;
    }

}
