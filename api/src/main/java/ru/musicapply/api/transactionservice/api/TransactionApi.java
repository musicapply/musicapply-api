package ru.musicapply.api.transactionservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import reactor.core.publisher.Flux;
import ru.musicapply.api.transactionservice.dto.TransactionResponseDto;

public interface TransactionApi {

    @GetMapping("/all")
    Flux<TransactionResponseDto> getAllTransactions();

}
