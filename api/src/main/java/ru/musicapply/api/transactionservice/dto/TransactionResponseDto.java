package ru.musicapply.api.transactionservice.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import ru.musicapply.api.productservice.dto.ProductResponseDto;

import java.time.LocalDateTime;


@Data
@Builder
public class TransactionResponseDto {

    private String userId;
    private Long amountPaid;
    private String currency;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDateTime paymentDate;
    private ProductResponseDto product;
    private String productId;
    private String paymentMethodId;

}
