package ru.musicapply.api.collection.playlists.api;

import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.playlists.dto.AddTrackInPlaylistRequest;
import ru.musicapply.api.collection.playlists.dto.PlaylistFullResponseDto;
import ru.musicapply.api.collection.playlists.dto.RemoveTrackRequest;


public interface CollectionPlaylistsServiceApi {

    @PostMapping("/create/{playlistName}")
    Mono<Void> createPlaylist(@PathVariable("playlistName") String playlistName);

    @PostMapping("/addTrack")
    Mono<Void> addTrackInPlaylist(@RequestBody AddTrackInPlaylistRequest request);

    @GetMapping("/getById/{playlistId}")
    Mono<PlaylistFullResponseDto> getPlaylistByPlaylistId(@PathVariable("playlistId") String playlistId);

    @GetMapping("/all")
    Flux<PlaylistFullResponseDto> getAllPlaylistsInLibrary();

    @DeleteMapping("/removeTrack")
    Mono<Void> removeTrack(@RequestBody RemoveTrackRequest request);

    @DeleteMapping("/removePlaylist/{playlistId}")
    Mono<Void> removePlaylist(@PathVariable("playlistId") String playlistId);

}
