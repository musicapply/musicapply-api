package ru.musicapply.api.collection.playlists.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AddTrackInPlaylistRequest {

    private String playlistId;
    private String trackId;


    @JsonCreator
    public AddTrackInPlaylistRequest(
            @JsonProperty(value = "playlistId", required = true) final String playlistId,
            @JsonProperty(value = "trackId", required = true) final String trackId
    ) {
        this.playlistId = playlistId;
        this.trackId = trackId;
    }

}
