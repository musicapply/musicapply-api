package ru.musicapply.api.collection.artists.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Builder;
import lombok.Data;
import ru.musicapply.api.artistservice.dto.ArtistResponseDto;

import java.time.LocalDateTime;


@Data
@Builder
public class ArtistRecordDto {

    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime creationDate;

    private ArtistResponseDto artist;

    @JsonCreator
    public ArtistRecordDto(
            @JsonProperty(value = "id", required = true) final String id,
            @JsonProperty(value = "creationDate", required = true) final LocalDateTime creationDate,
            @JsonProperty(value = "artist", required = true) final ArtistResponseDto artist
    ) {
        this.id = id;
        this.creationDate = creationDate;
        this.artist = artist;
    }

}
