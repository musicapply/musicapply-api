package ru.musicapply.api.collection.playlists.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class PlaylistFullResponseDto {

    private String playlistId;
    private String userId;
    private String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime creationDate;

    @Builder.Default
    private List<TrackRecordDto> tracks = new ArrayList<>();

    @JsonCreator
    public PlaylistFullResponseDto(
            @JsonProperty(value = "playlistId", required = true) final String playlistId,
            @JsonProperty(value = "userId", required = true) final String userId,
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "creationDate", required = true) final LocalDateTime creationDate,
            @JsonProperty(value = "tracks", required = true) final List<TrackRecordDto> tracks
    ) {
        this.playlistId = playlistId;
        this.userId = userId;
        this.name = name;
        this.creationDate = creationDate;
        this.tracks = tracks;
    }

}
