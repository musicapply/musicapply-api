package ru.musicapply.api.collection.albums.api;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.albums.dto.AlbumRecordDto;


public interface CollectionAlbumsServiceApi {

    @PostMapping("/add/{albumId}")
    Mono<Void> addAlbumInLibrary(@PathVariable("albumId") String albumId);

    @DeleteMapping("/remove/{albumId}")
    Mono<Void> removeAlbumFromFavorite(@PathVariable("albumId") String albumId);

    @GetMapping("/all")
    Flux<AlbumRecordDto> getAllAlbumsInLibrary();

}
