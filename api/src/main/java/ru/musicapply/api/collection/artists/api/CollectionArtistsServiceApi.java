package ru.musicapply.api.collection.artists.api;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.artists.dto.ArtistRecordDto;

public interface CollectionArtistsServiceApi {

    @PostMapping("/add/{artistId}")
    Mono<Void> addArtistInLibrary(@PathVariable("artistId") String artistId);

    @DeleteMapping("/remove/{artistId}")
    Mono<Void> removeArtistFromLibrary(@PathVariable("artistId") String artistId);

    @GetMapping("/all")
    Flux<ArtistRecordDto> getAllArtistsInLibrary();

}
