package ru.musicapply.api.collection.favorite.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Builder;
import lombok.Data;
import ru.musicapply.api.trackservice.dto.TrackResponseFullDto;

import java.time.LocalDateTime;


@Data
@Builder
public class TrackRecordDto {

    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime creationDate;
    private TrackResponseFullDto track;

    @JsonCreator
    public TrackRecordDto(
            @JsonProperty(value = "id", required = true) final String id,
            @JsonProperty(value = "creationDate", required = true) final LocalDateTime creationDate,
            @JsonProperty(value = "track", required = true) final TrackResponseFullDto track
    ) {
        this.id = id;
        this.creationDate = creationDate;
        this.track = track;
    }



}
