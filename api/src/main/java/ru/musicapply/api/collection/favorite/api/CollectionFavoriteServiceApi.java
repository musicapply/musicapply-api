package ru.musicapply.api.collection.favorite.api;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.collection.favorite.dto.TrackRecordDto;


public interface CollectionFavoriteServiceApi {

    @PostMapping("/add/{trackId}")
    Mono<Void> addTrackInFavorite(@PathVariable("trackId") String trackId);

    @DeleteMapping("/remove/{trackId}")
    Mono<Void> removeTrackFromFavorite(@PathVariable("trackId") String trackId);

    @GetMapping("/all")
    Flux<TrackRecordDto> getAllFavoriteTracks();

}
