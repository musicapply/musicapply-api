package ru.musicapply.api.collection.playlists.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class RemoveTrackRequest {

    private String playlistId;
    private String trackRecordId;

    @JsonCreator
    public RemoveTrackRequest(
            @JsonProperty(value = "playlistId", required = true) final String playlistId,
            @JsonProperty(value = "trackRecordId", required = true) final String trackRecordId
    ) {
        this.playlistId = playlistId;
        this.trackRecordId = trackRecordId;
    }

}
