package ru.musicapply.api.productservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
public class ProductResponseDto {

    private final String id;
    private final String name;
    private final String description;
    private final String imageUrl;
    private final Integer unitAmount;
    private final String currency;

    @JsonCreator
    public ProductResponseDto(
            @JsonProperty(value = "id", required = true) final String id,
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "description") final String description,
            @JsonProperty(value = "imageUrl", required = false) final String imageUrl,
            @JsonProperty(value = "unitAmount") final Integer unitAmount,
            @JsonProperty(value = "currency", required = true) final String currency
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
        this.unitAmount = unitAmount;
        this.currency = currency;
    }

}
