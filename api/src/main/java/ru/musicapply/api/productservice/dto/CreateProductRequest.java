package ru.musicapply.api.productservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CreateProductRequest {

    private final String name;
    private final String description;
    private final Integer unitAmount;
    private final String currency;

    @JsonCreator
    public CreateProductRequest(
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "description", required = true) final String description,
            @JsonProperty(value = "unitAmount", required = true) final Integer unitAmount,
            @JsonProperty(value = "currency", required = true) final String currency
    ) {
        this.name = name;
        this.description = description;
        this.unitAmount = unitAmount;
        this.currency = currency;
    }

}
