package ru.musicapply.api.productservice.internal.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Mono;
import ru.musicapply.api.productservice.internal.dto.InternalProductResponseDto;


public interface InternalProductServiceApi {

    @GetMapping("/{productId}")
    Mono<InternalProductResponseDto> getProductById(@PathVariable("productId") String productId);

    @GetMapping("/getByStripeId/{stripeId}")
    Mono<InternalProductResponseDto> getProductByStripeId(@PathVariable("stripeId") String stripeId);

}
