package ru.musicapply.api.productservice.internal.dto;

import lombok.Data;

@Data
public class InternalProductResponseDto {

    private String id;
    private String stripeId;
    private String name;
    private String description;
    private String defaultPriceId;
    private String imageUrl;
    private Integer unitAmount;
    private String currency;

}
