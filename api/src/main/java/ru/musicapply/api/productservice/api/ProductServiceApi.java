package ru.musicapply.api.productservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.productservice.dto.CreateProductRequest;
import ru.musicapply.api.productservice.dto.CreateProductResponse;
import ru.musicapply.api.productservice.dto.ProductResponseDto;


public interface ProductServiceApi {

    @PostMapping("/create")
    Mono<CreateProductResponse> createProduct(@RequestBody CreateProductRequest request);

    @GetMapping("/{productId}")
    Mono<ProductResponseDto> getProductById(@PathVariable("productId") String productId);

    @GetMapping("/all")
    Flux<ProductResponseDto> getAllProducts();

}
