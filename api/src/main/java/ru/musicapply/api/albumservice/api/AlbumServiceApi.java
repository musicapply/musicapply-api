package ru.musicapply.api.albumservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.albumservice.dto.AlbumResponseFullDto;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;

public interface AlbumServiceApi {

    @GetMapping("/short/{albumId}")
    Mono<AlbumResponseShortDto> getAlbumByIdShort(@PathVariable("albumId") String albumId);

    @GetMapping("/{albumId}")
    Mono<AlbumResponseFullDto> getAlbumByAlbumId(@PathVariable("albumId") String albumId);

    @GetMapping("/findByArtistId/{artistId}")
    Flux<AlbumResponseShortDto> getAlbumsByArtistIdShort(@PathVariable("artistId") String artistId);

    @GetMapping("/search/likeName/{name}")
    Flux<AlbumResponseFullDto> searchAlbumsLikeName(@PathVariable("name") String name);


}
