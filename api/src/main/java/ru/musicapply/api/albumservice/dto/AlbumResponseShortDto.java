package ru.musicapply.api.albumservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AlbumResponseShortDto {

    private String albumId;
    private String artistId;
    private String name;
    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime releaseDate;
    private String imageUrl;

    @JsonCreator
    public AlbumResponseShortDto(
            @JsonProperty(value = "albumId", required = true) final String albumId,
            @JsonProperty(value = "artistId", required = true) final String artistId,
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "description", required = true) final String description,
            @JsonProperty(value = "releaseDate", required = true) final LocalDateTime releaseDate,
            @JsonProperty(value = "imageUrl", required = true) final String imageUrl
    ) {
        this.albumId = albumId;
        this.artistId = artistId;
        this.name = name;
        this.description =description;
        this.releaseDate = releaseDate;
        this.imageUrl = imageUrl;
    }

}
