package ru.musicapply.api.albumservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Builder;
import lombok.Data;
import ru.musicapply.api.artistservice.dto.ArtistResponseShortDto;
import ru.musicapply.api.trackservice.dto.TrackResponseFullDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Data
@Builder
public class AlbumResponseFullDto {

    private String albumId;
    private ArtistResponseShortDto artist;
    private String name;
    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime releaseDate;
    private String imageUrl;

    @Builder.Default
    private List<TrackResponseFullDto> tracks = new ArrayList<>();


    @JsonCreator
    public AlbumResponseFullDto(
            @JsonProperty(value = "albumId", required = true) final String albumId,
            @JsonProperty(value = "artist", required = true) final ArtistResponseShortDto artist,
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "description", required = true) final String description,
            @JsonProperty(value = "releaseDate", required = true) final LocalDateTime releaseDate,
            @JsonProperty(value = "imageUrl", required = true) final String imageUrl,
            @JsonProperty(value = "tracks", required = true) final List<TrackResponseFullDto> tracks
    ) {
        this.albumId = albumId;
        this.artist = artist;
        this.name = name;
        this.description =description;
        this.releaseDate = releaseDate;
        this.imageUrl = imageUrl;
        this.tracks = tracks;
    }

}
