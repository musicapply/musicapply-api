package ru.musicapply.api.userservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import reactor.core.publisher.Mono;
import ru.musicapply.api.userservice.dto.UserInfoDto;

public interface UserApi {

    @GetMapping("/me")
    Mono<UserInfoDto> getUser();

}
