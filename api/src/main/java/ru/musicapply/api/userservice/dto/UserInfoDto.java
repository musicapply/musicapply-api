package ru.musicapply.api.userservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfoDto {

    private final String id;
    private final String username;
    private final String email;
    private final String avatarUrl;

}
