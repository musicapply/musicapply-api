package ru.musicapply.api.customerservice.internal.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;


@Data
@Builder
public class InternalCustomerResponseDto {

    private String userId;
    private String subscriptionId;
    private String stripeSubscriptionId;
    private LocalDateTime start;
    private LocalDateTime nextPayment;
    private String productId;
    private String subscriptionStatus;

    @JsonCreator
    public InternalCustomerResponseDto(
            @JsonProperty(value = "userId", required = true) final String userId,
            @JsonProperty(value = "subscriptionId", required = true) final String subscriptionId,
            @JsonProperty(value = "stripeSubscriptionId") final String stripeSubscriptionId,
            @JsonProperty(value = "start", required = true) final LocalDateTime start,
            @JsonProperty(value = "nextPayment") final LocalDateTime nextPayment,
            @JsonProperty(value = "productId", required = true) final String productId,
            @JsonProperty(value = "subscriptionStatus", required = true) final String subscriptionStatus
    ) {
        this.userId = userId;
        this.subscriptionId = subscriptionId;
        this.stripeSubscriptionId = stripeSubscriptionId;
        this.start = start;
        this.nextPayment = nextPayment;
        this.productId = productId;
        this.subscriptionStatus = subscriptionStatus;
    }

}
