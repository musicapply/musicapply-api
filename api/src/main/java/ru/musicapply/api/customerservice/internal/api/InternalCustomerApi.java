package ru.musicapply.api.customerservice.internal.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Mono;
import ru.musicapply.api.customerservice.internal.dto.InternalCustomerResponseDto;


public interface InternalCustomerApi {

    @GetMapping("/{userId}")
    Mono<InternalCustomerResponseDto> internalGetCustomerByUserId(@PathVariable("userId") String userId);

}
