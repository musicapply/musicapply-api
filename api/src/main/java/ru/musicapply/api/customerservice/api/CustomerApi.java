package ru.musicapply.api.customerservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Mono;
import ru.musicapply.api.customerservice.dto.CustomerResponseDto;


public interface CustomerApi {

    @GetMapping("/{userId}")
    Mono<CustomerResponseDto> getCustomerByUserId(@PathVariable("userId") String userId);

    @GetMapping("/me")
    Mono<CustomerResponseDto> me();

}
