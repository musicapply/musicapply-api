package ru.musicapply.api.artistservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.artistservice.dto.ArtistResponseDto;
import ru.musicapply.api.artistservice.dto.ArtistResponseShortDto;


public interface ArtistServiceApi {

    @GetMapping("/short/{artistId}")
    Mono<ArtistResponseShortDto> getArtistByArtistIdShort(@PathVariable("artistId") String artistId);

    @GetMapping("/{artistId}")
    Mono<ArtistResponseDto> getArtistByArtistIdFull(@PathVariable("artistId") String artistId);

    @GetMapping("/search/likeName/{name}")
    Flux<ArtistResponseDto> searchArtistsLikeName(@PathVariable("name") String name);

}
