package ru.musicapply.api.artistservice.internal.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Mono;
import ru.musicapply.api.artistservice.internal.dto.InternalArtistResponseDto;

public interface InternalArtistApi {

    @GetMapping("/findByUserId/{userId}")
    Mono<InternalArtistResponseDto> findByUserId(@PathVariable("userId") String userId);

}
