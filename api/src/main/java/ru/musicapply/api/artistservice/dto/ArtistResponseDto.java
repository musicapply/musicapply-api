package ru.musicapply.api.artistservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import ru.musicapply.api.albumservice.dto.AlbumResponseShortDto;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class ArtistResponseDto {

    private String artistId;
    private String imageUrl;
    private String name;
    private String bio;

    @Builder.Default
    private List<AlbumResponseShortDto> albums = new ArrayList<>();

    @JsonCreator
    public ArtistResponseDto(
            @JsonProperty(value = "artistId", required = true) final String artistId,
            @JsonProperty(value = "imageUrl", required = true) final String imageUrl,
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "bio", required = true) final String bio,
            @JsonProperty(value = "albums", required = true) final List<AlbumResponseShortDto> albums
    ) {
        this.artistId = artistId;
        this.imageUrl = imageUrl;
        this.name = name;
        this.bio = bio;
        this.albums = albums;
    }

}
