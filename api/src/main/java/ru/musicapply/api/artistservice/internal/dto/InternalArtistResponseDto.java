package ru.musicapply.api.artistservice.internal.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;

@Data
@Builder
public class InternalArtistResponseDto {

    private ObjectId id;
    private String artistId;
    private String userId;
    private String imageUrl;
    private String name;
    private String bio;

    @JsonCreator
    public InternalArtistResponseDto(
            @JsonProperty(value = "id", required = true) final ObjectId id,
            @JsonProperty(value = "artistId", required = true) final String artistId,
            @JsonProperty(value = "userId", required = true) final String userId,
            @JsonProperty(value = "imageUrl", required = true) final String imageUrl,
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "bio", required = true) final String bio
    ) {
        this.id = id;
        this.artistId = artistId;
        this.userId = userId;
        this.imageUrl = imageUrl;
        this.name = name;
        this.bio = bio;
    }


}
