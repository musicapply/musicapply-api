package ru.musicapply.api.artistservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
public class ArtistResponseShortDto {

    private String artistId;
    private String imageUrl;
    private String name;
    private String bio;

    @JsonCreator
    public ArtistResponseShortDto(
            @JsonProperty(value = "artistId", required = true) final String artistId,
            @JsonProperty(value = "imageUrl", required = true) final String imageUrl,
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "bio", required = true) final String bio
    ) {
        this.artistId = artistId;
        this.imageUrl = imageUrl;
        this.name = name;
        this.bio = bio;
    }

}
