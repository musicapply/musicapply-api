package ru.musicapply.api.artistcompositeservice;

import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import reactor.core.publisher.Mono;
import ru.musicapply.api.artistcompositeservice.dto.CreateAlbumRequest;
import ru.musicapply.api.artistcompositeservice.dto.CreateArtistRequest;
import ru.musicapply.api.artistcompositeservice.dto.CreateTrackRequest;


public interface ArtistCompositeApi {

    @PostMapping("/create")
    Mono<Void> createArtist(
            @RequestPart("data") CreateArtistRequest request,
            @RequestPart("artistImage") Mono<FilePart> filePartMono
    );

    @PostMapping("/createAlbum")
    Mono<Void> createAlbum(
            @RequestPart("data") CreateAlbumRequest request,
            @RequestPart("albumImage") Mono<FilePart> filePartMono
    );

    @PostMapping("/createTrack")
    Mono<Void> createTrack(
            @RequestPart("data") CreateTrackRequest request,
            @RequestPart("trackFile") Mono<FilePart> filePartMono
    );
}
