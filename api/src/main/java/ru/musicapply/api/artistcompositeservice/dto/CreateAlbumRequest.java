package ru.musicapply.api.artistcompositeservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CreateAlbumRequest {

    private String name;
    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime releaseDate;

    @JsonCreator
    public CreateAlbumRequest(
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "description", required = true) final String description,
            @JsonProperty(value = "releaseDate", required = true)
            @JsonDeserialize(using = LocalDateTimeDeserializer.class) final LocalDateTime releaseDate
    ) {
        this.name = name;
        this.description = description;
        this.releaseDate = releaseDate;
    }

}
