package ru.musicapply.api.artistcompositeservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
public class CreateTrackRequest {

    private String name;
    private String albumId;

    @JsonCreator
    public CreateTrackRequest(
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "albumId", required = true) final String albumId
    ) {
        this.name = name;
        this.albumId = albumId;
    }

}
