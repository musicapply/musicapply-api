package ru.musicapply.api.artistcompositeservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
public class CreateArtistRequest {

    private final String name;
    private final String bio;

    @JsonCreator
    public CreateArtistRequest(
            @JsonProperty(value = "name", required = true) final String name,
            @JsonProperty(value = "bio", required = true) final String bio
    ) {
        this.name = name;
        this.bio = bio;
    }


}
