package ru.musicapply.api.paymentservice.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;


@Data
@Builder
public class PaymentProfileResponseDto {

    private String userId;
    private PaymentMethodResponseDto defaultPaymentMethod;
    private List<PaymentMethodResponseDto> paymentMethods;

}
