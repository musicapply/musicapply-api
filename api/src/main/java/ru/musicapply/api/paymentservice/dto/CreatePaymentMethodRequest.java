package ru.musicapply.api.paymentservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CreatePaymentMethodRequest {

    private final String cardNumber;
    private final Integer expMonth;
    private final Integer expYear;
    private final String cvc;


    @JsonCreator
    public CreatePaymentMethodRequest(
            @JsonProperty(value = "cardNumber", required = true) final String cardNumber,
            @JsonProperty(value = "expMonth", required = true) final Integer expMonth,
            @JsonProperty(value = "expYear", required = true) final Integer expYear,
            @JsonProperty(value = "cvc", required = true) final String cvc
    ) {
        this.cardNumber = cardNumber;
        this.expMonth = expMonth;
        this.expYear = expYear;
        this.cvc = cvc;
    }

}
