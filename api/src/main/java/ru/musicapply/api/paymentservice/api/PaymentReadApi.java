package ru.musicapply.api.paymentservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.musicapply.api.paymentservice.dto.PaymentMethodResponseDto;
import ru.musicapply.api.paymentservice.dto.PaymentProfileResponseDto;


public interface PaymentReadApi {

    @GetMapping("/payment_method/all")
    Flux<PaymentMethodResponseDto> getUserPaymentMethods();

    @GetMapping("/me")
    Mono<PaymentProfileResponseDto> getPaymentProfile();

}
