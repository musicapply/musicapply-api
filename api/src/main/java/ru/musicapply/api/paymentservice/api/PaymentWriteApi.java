package ru.musicapply.api.paymentservice.api;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;
import ru.musicapply.api.paymentservice.dto.CreatePaymentMethodRequest;


public interface PaymentWriteApi {

    @PostMapping("/payment_method/new")
    Mono<Void> addPaymentMethod(@RequestBody CreatePaymentMethodRequest request);

    @PostMapping("/payment_method/default/{paymentMethodId}")
    Mono<Void> setDefaultPaymentMethod(@PathVariable("paymentMethodId") String paymentMethodId);

    @DeleteMapping("/payment_method/{paymentMethodId}")
    Mono<Void> removePaymentMethod(@PathVariable("paymentMethodId") String paymentMethodId);

    @PostMapping("/subscription/new/{productId}")
    Mono<Void> createSubscription(@PathVariable("productId") String productId);

    @DeleteMapping("/subscription/cancel")
    Mono<Void> cancelSubscription();


}
