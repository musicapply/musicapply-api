package ru.musicapply.api.paymentservice.dto;

import lombok.Data;


@Data
public class PaymentMethodResponseDto {

    private String paymentMethodId;
    private String cardNumber;
    private String cardBrand;

}
